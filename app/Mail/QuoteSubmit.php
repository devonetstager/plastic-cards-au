<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuoteSubmit extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $content ;
	
    public function __construct($data)
    {
        $this->content = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $bcc = ['mans446@hotmail.com','qacheck17@gmail.com'];
        //  $bcc = ['qacheck17@gmail.com'];
         
        //  return $this->view('emails.quotesubmit')->subject('Quote - Plastic Cards AU')->with('content',$this->content);    
        return $this->view('emails.quotesubmit')->cc('stephanie@plastic-cards.com.au')->replyTo($this->content['c_email'])->subject('Quote - Plastic Cards AU')->with('content',$this->content);    
    }
}
