<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use Illuminate\Support\Facades\Input; 
use Auth; 
use Redirect;
use Illuminate\Support\Facades\Crypt;
use Session;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Hash;use Artisan;


class UserController extends Controller
{



    public function change_password(){
		
		


        return View::make('cp-admin.user.user_changePassword');
                    

    }

    public function updating_Password(Request $request){

        $usr_id= Input::get('usr_id');
        $pwd = Input::get('password');
 $pwd =Hash::make($pwd);
        $data_update = DB::table('users')
                        ->where('id', $usr_id)
                        ->update(['password' => $pwd]);
		if($data_update)
            $request->session()->flash('success', 'Password updated successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }   
}




