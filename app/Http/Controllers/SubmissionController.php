<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use View;
use Image;
use Redirect;
use Crypt;
use Response;

class SubmissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
	public function contact_submissions(Request $request)
	{
	    $date_from = $request->dateFrom;
	    $date_to = $request->dateTo;
	    $data_temp = DB::table('contact_submissions');
		if(!empty($date_from) || !empty($date_to)){
		    $date_from = date('Y-m-d' , strtotime($date_from));
		    $date_to = date('Y-m-d' , strtotime($date_to));
		    $data_temp->when($date_from, function ($query) use ($date_from , $date_to ) {
                                return $query->whereBetween('con_date', [$date_from , $date_to]);
                            });
		}
	
		$data = 		$data_temp->orderBy('con_id','desc')  
				->paginate(10);
		return View::make('cp-admin.submission.contact_list')->with([
			'data'=>$data
		])->render();       
    }
	
	public function contact_submissions_delete($id,Request $request)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('contact_submissions')
				->where('con_id',$id)       
				->delete();
        if($data)
            $request->session()->flash('success', 'Submission deleted successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }
	
	
	public function quote_submissions(Request $request)
	{
		$date_from = $request->dateFrom;
	    $date_to = $request->dateTo;
	    $data_temp = DB::table('quotation_submission');
	    if(!empty($date_from) || !empty($date_to)){
	    	$date_from = date('Y-m-d' , strtotime($date_from));
		    $date_to = date('Y-m-d' , strtotime($date_to));
		    $data_temp->when($date_from, function ($query) use ($date_from , $date_to ) {
                                return $query->whereBetween('created_at', [$date_from , $date_to]);
                            });   
	    }
	    $data = $data_temp->orderBy('qt_id','desc')->paginate(10);
		return View::make('cp-admin.submission.quotation_list')->with([
			'data'=>$data
		])->render();        
    }
	
	public function quote_submissions_delete($id,Request $request)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('quotation_submission')
				->where('qt_id',$id)       
				->delete();
        if($data)
            $request->session()->flash('success', 'Submission deleted successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }
	public function order_submissions(Request $request)
	{
	    $date_from = $request->dateFrom;
	    $date_to = $request->dateTo;
	    $data_temp = DB::table('submit_order_submissions');
		if(!empty($date_from) || !empty($date_to)){
		    $date_from = date('Y-m-d' , strtotime($date_from));
		    $date_to = date('Y-m-d' , strtotime($date_to));
		    $data_temp->when($date_from, function ($query) use ($date_from , $date_to ) {
                                return $query->whereBetween('con_date', [$date_from , $date_to]);
                            });
		}
	
	
		$data = $data_temp->orderBy('sbo_id','desc')->paginate(10);
		
// 		$data = DB::table('submit_order_submissions')
// 				->orderBy('sbo_id','desc')  
// 				->paginate(10);
		return View::make('cp-admin.submission.order_list')->with([
			'data'=>$data
		])->render();       
    }
	
	public function order_submissions_delete($id,Request $request)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('submit_order_submissions')
				->where('sbo_id',$id)       
				->delete();
        if($data)
            $request->session()->flash('success', 'Submission deleted successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }
	public function order_sample_submissions(Request $request)
	{
		
	    $date_from = $request->dateFrom;
	    $date_to = $request->dateTo;
	    $data_temp = DB::table('ordersamples');
		if(!empty($date_from) || !empty($date_to)){
		    $date_from = date('Y-m-d' , strtotime($date_from));
		    $date_to = date('Y-m-d' , strtotime($date_to));
		    $data_temp->when($date_from, function ($query) use ($date_from , $date_to ) {
                                return $query->whereBetween('ord_created_at', [$date_from , $date_to]);
                            });
		}
	
	
		$data = $data_temp->orderBy('ord_id','desc')->paginate(10);		

		return View::make('cp-admin.submission.order_sample_list')->with([
			'data'=>$data
		])->render();         
    }
	public function order_sample_submissions_delete($id,Request $request)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('ordersamples')
				->where('ord_id',$id)       
				->delete();
        if($data)
            $request->session()->flash('success', 'Submission deleted successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }
    public function export_contact(Request $request)
	{

        
		$headers = array(
			"Content-type" => "text/csv",
			"Content-Disposition" => "attachment; filename=file.csv",
			"Pragma" => "no-cache",
			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
			"Expires" => "0"
		);

		// $reviews = Reviews::getReviewExport($this->hw->healthwatchID)->get();

        $date_from = $request->dateFrom;
	    $date_to = $request->dateTo;
	    $data_temp = DB::table('contact_submissions');
		if(!empty($date_from) || !empty($date_to)){
		    $date_from = date('Y-m-d' , strtotime($date_from));
		    $date_to = date('Y-m-d' , strtotime($date_to));
		    $data_temp->when($date_from, function ($query) use ($date_from , $date_to ) {
                                return $query->whereBetween('con_date', [$date_from , $date_to]);
                            });
		}
	
	
		$datas = $data_temp->orderBy('con_id','desc')->get();
		
		if(count($datas) < 1){
		    $request->session()->flash('info', "No result to export");
            return Redirect::back();
		}
		    

        if(!empty($request -> filters)){
           $columns=  array_keys($request -> filters);
           array_unshift($columns, 'ID' );
        }else{
            $columns = array('ID', 'First Name' ,'Last Name', 'Email', 'Inquiry Type', 'Inquiry','Enquired On');
        }
		

		

		$callback = function() use ($datas, $columns)
		{

			$file = fopen('php://output', 'w');
			fputcsv($file, $columns);
		    $count=1;
		   
			foreach($datas as $data) {
                 $data_temp = array($count++);
                if(in_array('First Name' , $columns)){
                    array_push($data_temp,$data->con_firstname);
                }
                if(in_array('Last Name' , $columns)){
                    array_push($data_temp,$data->con_lastname);
                }
                if(in_array('Email' , $columns)){
                    array_push($data_temp,$data->con_email);
                }
                if(in_array('Inquiry Type' , $columns)){
                    array_push($data_temp,$data->con_inquiry_type);
                }
                if(in_array('Inquiry' , $columns)){
                    array_push($data_temp,$data->con_inquiry);
                }
                if(in_array('Enquired On' , $columns)){
                     array_push($data_temp,$data->con_date);
                }
                // $data_temp = array($count++, $data->con_firstname." ".$data->con_lastname,$data->con_email, $data->con_inquiry_type, $data->con_inquiry,$data->con_date);
                
				fputcsv($file, $data_temp);
			}
			fclose($file);
		};
		return Response::stream($callback, 200, $headers);
	}
	
	public function export_quote(Request $request)
	{

		$headers = array(
			"Content-type" => "text/csv",
			"Content-Disposition" => "attachment; filename=file.csv",
			"Pragma" => "no-cache",
			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
			"Expires" => "0"
		);

		// $reviews = Reviews::getReviewExport($this->hw->healthwatchID)->get();

// 		$datas = DB::table('quotation_submission')->orderBy('qt_id','desc')->get();

    	$date_from = $request->dateFrom;
	    $date_to = $request->dateTo;
	    $data_temp = DB::table('quotation_submission');
	    if(!empty($date_from) || !empty($date_to)){
	    	$date_from = date('Y-m-d' , strtotime($date_from));
		    $date_to = date('Y-m-d' , strtotime($date_to));
		    $data_temp->when($date_from, function ($query) use ($date_from , $date_to ) {
                                return $query->whereBetween('created_at', [$date_from , $date_to]);
                            });   
	    }
	    $datas = $data_temp->orderBy('qt_id','desc')->get();
	    
	    	
		if(count($datas) < 1){
		    $request->session()->flash('info', "No result to export");
            return Redirect::back();
		}

// 		$columns = array('ID', 'Name', 'Email', 'Phone', 'Comments','Details');
		
        if(!empty($request -> filters)){
           $columns=  array_keys($request -> filters);
           array_unshift($columns, 'ID' );
        }else{
            $columns = array('ID', 'First Name' ,'Last Name', 'Email', 'Phone', 'Comment', 'Details','Created Date' );
        }
        
		$callback = function() use ($datas, $columns)
		{

			$file = fopen('php://output', 'w');
			fputcsv($file, $columns);
		   $count=1;
		   
			foreach($datas as $data) {
                $data_temp = array($count++);
                if(in_array('First Name' , $columns)){
                    array_push($data_temp,$data->qt_firstname);
                }
                if(in_array('Last Name' , $columns)){
                    array_push($data_temp,$data->qt_lastname);
                }
                if(in_array('Email' , $columns)){
                    array_push($data_temp,$data->qt_email);
                }
                if(in_array('Phone' , $columns)){
                    array_push($data_temp,$data->qt_phone);
                }
                if(in_array('Comment' , $columns)){
                    array_push($data_temp,($data->qt_comments) ? $data->qt_comments : " " );
                }
                if(in_array('Details' , $columns)){
                    $details = 'Card Type :'.$data->qt_card_type."\n".'No Cards : '.$data->qt_no_of_cards."\n". 'Features :'.$data->qt_features."\n".'Card Thickness :'.$data->qt_cardthickness."\n".'Number of colors front :'.$data->qt_no_of_colors_front."\n".'Number of colors back :'.$data->qt_no_of_colors_back."\n".'Magnetic Stripe :'.$data->qt_magnetic_stripe."\n".'Date Card :'.$data->qt_date_card."\n".'Enquired On :'.$data->created_at;
                     array_push($data_temp,$details);
                }
                if(in_array('Created Date' , $columns)){
                    array_push($data_temp,$data->created_at);
                }

	            fputcsv($file, $data_temp);
			
				// $details = 'Card Type :'.$data->qt_card_type."\n".'No Cards : '.$data->qt_no_of_cards."\n". 'Features :'.$data->qt_features."\n".'Card Thickness :'.$data->qt_cardthickness."\n".'Number of colors front :'.$data->qt_no_of_colors_front."\n".'Number of colors back :'.$data->qt_no_of_colors_back."\n".'Magnetic Stripe :'.$data->qt_magnetic_stripe."\n".'Date Card :'.$data->qt_date_card."\n".'Enquired On :'.$data->created_at;


				// fputcsv($file, array($count++, $data->qt_firstname." ".$data->qt_lastname,$data->qt_email, $data->qt_phone, ($data->qt_comments) ? $data->qt_comments : " ", $details));
			}
			fclose($file);
		};
		return Response::stream($callback, 200, $headers);
	}
	public function export_order(Request $request)
	{

		$headers = array(
			"Content-type" => "text/csv",
			"Content-Disposition" => "attachment; filename=file.csv",
			"Pragma" => "no-cache",
			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
			"Expires" => "0"
		);

		// $reviews = Reviews::getReviewExport($this->hw->healthwatchID)->get();

// 		$datas = DB::table('submit_order_submissions')->orderBy('sbo_id','desc')->get();

// 		$columns = array('ID', 'Name', 'Email', 'Phone', 'Comments','Enquired On');
		$date_from = $request->dateFrom;
	    $date_to = $request->dateTo;
	    $data_temp = DB::table('submit_order_submissions');
		if(!empty($date_from) || !empty($date_to)){
		    $date_from = date('Y-m-d' , strtotime($date_from));
		    $date_to = date('Y-m-d' , strtotime($date_to));
		    $data_temp->when($date_from, function ($query) use ($date_from , $date_to ) {
                                return $query->whereBetween('con_date', [$date_from , $date_to]);
                            });
		}
	
	
		$datas = $data_temp->orderBy('sbo_id','desc')->get();
		if(count($datas) < 1){
		    $request->session()->flash('info', "No result to export");
            return Redirect::back();
		}
		    
	    
		
		if(!empty($request -> filters)){
           $columns=  array_keys($request -> filters);
           array_unshift($columns, 'ID' );
        }else{
            $columns = array('ID', 'First Name' ,'Last Name', 'Email', 'Phone', 'Comments','Enquired On');
        }
        


		$callback = function() use ($datas, $columns)
		{

			$file = fopen('php://output', 'w');
			fputcsv($file, $columns);
		   $count=1;
		   
			foreach($datas as $data) {
                $data_temp = array($count++);
                if(in_array('First Name' , $columns)){
                    array_push($data_temp,$data->sbo_firstname);
                }
                if(in_array('Last Name' , $columns)){
                    array_push($data_temp,$data->sbo_lastname);
                }
                if(in_array('Email' , $columns)){
                    array_push($data_temp,$data->sbo_email);
                }
                 if(in_array('Phone' , $columns)){
                    array_push($data_temp,$data->sbo_phone);
                }
                if(in_array('Comments' , $columns)){
                    array_push($data_temp,$data->sbo_comments);
                }
                if(in_array('Enquired On' , $columns)){
                     array_push($data_temp,$data->con_date);
                }

	            fputcsv($file, $data_temp);

			

				// fputcsv($file, array($count++, $data->sbo_firstname." ".$data->sbo_lastname,$data->sbo_email, $data->sbo_phone, $data->sbo_comments,$data->con_date));
			}
			fclose($file);
		};
		return Response::stream($callback, 200, $headers);
	}
	public function export_order_sample(Request $request)
	{

		$headers = array(
			"Content-type" => "text/csv",
			"Content-Disposition" => "attachment; filename=file.csv",
			"Pragma" => "no-cache",
			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
			"Expires" => "0"
		);

		// $reviews = Reviews::getReviewExport($this->hw->healthwatchID)->get();

// 		$datas = DB::table('ordersamples')->orderBy('ord_id','desc')->get();
        $date_from = $request->dateFrom;
	    $date_to = $request->dateTo;
	    $data_temp = DB::table('ordersamples');
		if(!empty($date_from) || !empty($date_to)){
		    $date_from = date('Y-m-d' , strtotime($date_from));
		    $date_to = date('Y-m-d' , strtotime($date_to));
		    $data_temp->when($date_from, function ($query) use ($date_from , $date_to ) {
                                return $query->whereBetween('ord_created_at', [$date_from , $date_to]);
                            });
		}
	
	
		$datas = $data_temp->orderBy('ord_id','desc')->paginate(10);	
		if(count($datas) < 1){
		    $request->session()->flash('info', "No result to export");
            return Redirect::back();
		}
		    

// 		$columns = array('ID', 'Name', 'Email', 'Phone', 'Comments','Details');
	    if(!empty($request -> filters)){
           $columns=  array_keys($request -> filters);
           array_unshift($columns, 'ID' );
        }else{
              $columns = array('ID', 'First Name' ,'Last Name', 'Email', 'Phone', 'Comments', 'Details' ,'Created Date');
        }
		

		$callback = function() use ($datas, $columns)
		{

			$file = fopen('php://output', 'w');
			fputcsv($file, $columns);
		   $count=1;
		   
			foreach($datas as $data) {
                $data_temp = array($count++);
                if(in_array('First Name' , $columns)){
                    array_push($data_temp,$data->ord_firstname);
                }
                if(in_array('Last Name' , $columns)){
                    array_push($data_temp,$data->ord_lastname);
                }
                if(in_array('Email' , $columns)){
                    array_push($data_temp,$data->ord_email);
                }
                 if(in_array('Phone' , $columns)){
                    array_push($data_temp,$data->ord_phone);
                }
                if(in_array('Comments' , $columns)){
                    array_push($data_temp,($data->ord_comments) ? $data->ord_comments : " ");
                }
                if(in_array('Details' , $columns)){
                    $details = 'Company :'.$data->ord_company."\n".'Address : '.$data->ord_address1." ".$data->ord_address2."\n". 'City : '.$data->ord_city."\n".'State :'.$data->ord_state."\n".'Country : '.$data->ord_country."\n".'Zip :'.$data->ord_zip."\n".'Find Us :'.$data->ord_how_findus."\n".'Enquired On :'.$data->ord_created_at;

                     array_push($data_temp,$details);
                }
                if(in_array('Created Date' , $columns)){
                    array_push($data_temp,$data->ord_created_at);
                }
	            fputcsv($file, $data_temp);
				// $details = 'Company :'.$data->ord_company."\n".'Address : '.$data->ord_address1." ".$data->ord_address2."\n". 'City : '.$data->ord_city."\n".'State :'.$data->ord_state."\n".'Country : '.$data->ord_country."\n".'Zip :'.$data->ord_zip."\n".'Find Us :'.$data->ord_how_findus."\n".'Enquired On :'.$data->ord_created_at;


				// fputcsv($file, array($count++, $data->ord_firstname." ".$data->ord_lastname,$data->ord_email, $data->ord_phone, ($data->ord_comments) ? $data->ord_comments : " ", $details));
			}
			fclose($file);
		};
		return Response::stream($callback, 200, $headers);
	}
}
