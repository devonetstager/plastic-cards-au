<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use View;
use Image;
use Redirect;
use Crypt;

class TestimonialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
	public function testimonial()
	{
		
		$data = DB::table('testimonials')
				->orderBy('tes_order','desc')    
              
				
				->get();
				
		
        //return view('cp-admin.pages.edit', compact('data','details'));
		return View::make('cp-admin.testimonial.testimonial_list')->with([
			'data'=>$data
			
		
		])->render();
		
       
    }
	public function testimonial_add()
	{
	
		
        //return view('cp-admin.pages.edit', compact('data','details'));
		return View::make('cp-admin.testimonial.testimonial_add')->render();
		
       
    }
	public function testimonial_save(Request $request)
	{ 
		
		$max_tes_order = DB::table('testimonials')
				->max('tes_order');
		if($max_tes_order=="") {
			$max_tes_order = 0;
		}
		

		$data_update	= DB::table('testimonials')				
				->insert([
					'tes_title' 	=> $request->tes_title,
					'tes_description' 	=> $request->tes_description,
					'tes_order' 	=> $max_tes_order+1
				]);
           

        

		if($data_update)
            $request->session()->flash('success', 'Testimonial added successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();


	}
	public function testimonial_edit($id)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('testimonials')
				->where('tes_id',$id)       
				->first();
        //return view('cp-admin.pages.edit', compact('data','details'));
		return View::make('cp-admin.testimonial.testimonial_edit')->with([
			'data'=>$data
		])->render();
    }
	public function testimonial_delete($id,Request $request)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('testimonials')
				->where('tes_id',$id)       
				->delete();
        if($data)
            $request->session()->flash('success', 'Testimonial deleted successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }
	public function testimonial_update(Request $request)
	{ 
		
		

		$data_update	= DB::table('testimonials')
				->where('tes_id',$request->tes_id)
				->update([
					'tes_title' 	=> $request->tes_title,
					'tes_description' 	=> $request->tes_description
				]);
           

        

		if($data_update)
            $request->session()->flash('success', 'Testimonial updated successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();


	}
}
