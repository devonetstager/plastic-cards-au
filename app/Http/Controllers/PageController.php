<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Crypt;
use Redirect;
use DB;
use View;
use Image;


class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
		//$page_list = Page::all()->orderBy('page_sort_order','desc')    ;
		$page_list = Page::orderBy('page_sort_order')->get();
		
				
		return view('cp-admin.pages.list', compact('page_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cp-admin.pages.create');
    }
	
	public function edit_page($id)
    {
		$id = Crypt::decrypt($id);
		
		$data = Page::where('id' ,$id)->first(); 
		
		$details = DB::table('gallery')
                ->orderBy('gallery_image_order','desc')    
				->where('gallery_page_id',$id)
				->get();
		
		
		
        //return view('cp-admin.pages.edit', compact('data','details'));
		return View::make('cp-admin.pages.edit')->with([
			'data'=>$data,
			'details'=>$details
		
		])->render();
			
    }
	public function update_page(Request $request)
    {
		
		$this->validate($request, [
            "page_heading" => "required",
        ],[
            'page_heading.required' => 'Heading is required',        
        ]);
		//page_image
		if($request->hasFile('page_image')) 
		{
			$imagefile 	= $request->file('page_image') ;
			$extension 	= $imagefile->getClientOriginalExtension();
			$ImgOriginalName = $imagefile->getClientOriginalName();
			//$fileName 	= uniqid().time().".".$extension;
			//check image exist or not starts here
				list($imgName, $ext) = explode(".", $ImgOriginalName);
				$image_name = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($imgName)));
				$image_name = $image_name;
				$fileName 	= $image_name.".".$extension;
				$img_path 	= public_path().'/galleryassets/';
				
				$gallery_file_name = $this->file_check($img_path,$fileName,$fileName);
				//check image name exist or not ends here
			$folderpath = public_path().'/pageassets/';
			$imagefile->move($folderpath , $fileName);
			$page_image_asset	= $fileName;
			
			$cmplteImageurl = $folderpath.$fileName;
				Image::make($cmplteImageurl)
						->resize(338,236)
						->save($cmplteImageurl);
						
			
		}
		else
		{
			$page_image_asset=$request->old_page_image;
		}
		
		$data_array = array(
          'page_heading' => $request -> page_heading,
          'page_description' => $request -> page_description,
          'page_image' => $page_image_asset,
          'meta_title' => $request -> meta_title,
          'meta_keywords' => $request -> meta_keywords,
          'meta_description' => $request -> meta_description,
          
        );
		
		$id = $request -> page_id;
        $data_update = Page::where('id' , $id)->update($data_array);
        if($data_update)
            $request->session()->flash('success', 'Page updated successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
		
	}
    public function upload_image(Request $request)
    {
    	//$imgName = request()->file->getClientOriginalName();
        //request()->file->move(public_path('mediauploads'), $imgName);

    	//return response()->json(['uploaded' => '/mediauploads/'.$imgName]);
		
		 $gallery_page_id = $request -> gallery_page_id;
		 $page_heading = $request -> page_heading;
		
		if ($ProImgs = $request->file('file')) 
		{	
			$claim_id =1;
			foreach($ProImgs as $imgs)
			{

				$extension = $imgs->getClientOriginalExtension();

				$ImgOriginalName = $imgs->getClientOriginalName();
				
				$nowtime = strtotime('now').rand(10,100);
				$acc_car_imgs = $imgs;
				//$gallery_file_name = "gallery_".$nowtime.".".$extension;
				//check image exist or not starts here
				list($imgName, $ext) = explode(".", $ImgOriginalName);
				$image_name = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($imgName)));
				$image_name = $image_name;
				$fileName 	= $image_name.".".$extension;
				$img_path 	= public_path().'/galleryassets/';
				
				$gallery_file_name = $this->file_check($img_path,$fileName,$fileName);
				//check image name exist or not ends here
				
				$filename = pathinfo($gallery_file_name, PATHINFO_FILENAME);
				$destinationPath = public_path('/galleryassets');
				$acc_car_imgs->move($destinationPath, $gallery_file_name);
				
				$insertData = DB::table('gallery')->insert([
					'gallery_page_id' => $gallery_page_id,
					'gallery_file' => $gallery_file_name,
					'gallery_original_name' => $ImgOriginalName,
					'gallery_created_date' => date("Y-m-d H:i:s")
					
							
				]);

                            
				
                           
			}
		}
		return response()->json(['uploaded' => 'DONE']);
		
    }
	public function delete_image(Request $request)
    {
		$gallery_id = $request -> gal_id;
		
		//unlink image starts
		$details = DB::table('gallery')
                
				->where('gallery_id',$gallery_id)
				->get();
		
		//echo $details[0]->gallery_file;
		unlink(public_path('/galleryassets/').$details[0]->gallery_file);
		
		
		//unlink iage ends 
		DB::table('gallery')
			-> where('gallery_id',$gallery_id)
			->delete();
		echo "yes";	
			
        //
    }
    public function upload_content_image(Request $request)
    {
		$nws_media = $request->file('nws_media_up');
	        $MediaName = $nws_media->getClientOriginalName();
	        $destinationPath = public_path('/mediauploads');
	        $nws_media->move($destinationPath, $MediaName);
        //
    }
	
	public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
	public function file_check($image_path, $filename,$org_filename)
	{
		
		if (file_exists($image_path.$filename)) 
		{  
			list($txt, $ext) = explode(".", $filename);
			$tmp = explode("-",$txt);
			
			
			$checknum = end($tmp);
			
			if(!is_numeric($checknum)) {
				$num = 1; 
			}
			else {
				
				$txt_arr = explode("-", $txt);
				array_pop($txt_arr);
				$txt = implode("-",$txt_arr);
				
				$num = $checknum +1;  
			}
			$filename 	= $txt."-".$num.".".$ext;
			
			return  $this->file_check($image_path,$filename, $org_filename);
	
		}
		else
		{
			return $filename;
		}
		
		
	}
}
