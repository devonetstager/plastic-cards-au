<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use View;
use Image;
use Redirect;
use Crypt;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('cp-admin/home');
    }
	public function mainpage()
    {
		$data = DB::table('homepage')
              
				->where('id',1)
				->first();
		
		
		
        //return view('cp-admin.pages.edit', compact('data','details'));
		return View::make('cp-admin.mainpage.edit')->with([
			'data'=>$data
			
		
		])->render();
		
       
    }
	public function update_mainpage(Request $request)
	{ 


		$id = 1;  
			

		if($request->hasFile('home_banner_image')) 
		{
			$imagefile 	= $request->file('home_banner_image') ;
			$extension 	= $imagefile->getClientOriginalExtension();
			$ImgOriginalName = $imagefile->getClientOriginalName();
			
			list($imgName, $ext) = explode(".", $ImgOriginalName);
				$image_name = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($imgName)));
				$image_name = $image_name;
				$fileName 	= $image_name.".".$extension;
				$img_path 	= public_path().'/homeassets/';
			
			$fileName = $this->file_check($img_path,$fileName,$fileName);
			//check image name exist or not ends here
			$folderpath = public_path().'/homeassets/';
			$imagefile->move($folderpath , $fileName);
			$bnr_asset	= $fileName;
			
			$cmplteBannerurl = $folderpath.$fileName;
				Image::make($cmplteBannerurl)
						->resize(1920,400)
						->save($cmplteBannerurl);
						
			
		}
		else
		{
			$bnr_asset=$request->old_banner_image;
		}
		
		if($request->hasFile('home_section2_image')) 
		{
			$imagefile 	= $request->file('home_section2_image') ;
			$extension 	= $imagefile->getClientOriginalExtension();
			$ImgOriginalName = $imagefile->getClientOriginalName();
			
			list($imgName, $ext) = explode(".", $ImgOriginalName);
				$image_name = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($imgName)));
				$image_name = $image_name;
				$fileName 	= $image_name.".".$extension;
				$img_path 	= public_path().'/homeassets/';
				
				
			
			$fileName = $this->file_check($img_path,$fileName,$fileName);
			//check image name exist or not ends here
			
			$folderpath = public_path().'/homeassets/';
			$imagefile->move($folderpath , $fileName);
			$home_section2_image	= $fileName;
			
			$cmplteBannerurl = $folderpath.$fileName;
				Image::make($cmplteBannerurl)
						->resize(624,409)
						->save($cmplteBannerurl);
						
			
		}
		else
		{
			$home_section2_image=$request->old_home_section2_image;
		}

		$data_update	= DB::table('homepage')
				->where('id',1)
				->update([
					'home_banner_title' 	=> $request->home_banner_title,
					'home_banner_image' 	=> $bnr_asset,
					'home_banner_features' 	=> $request->home_banner_features,
					'home_heading' 			=> $request->home_heading,
					'home_description' 		=> $request->home_description,
					'home_section2_title'	=> $request->home_section2_title,
					'home_section2_image'	=> $home_section2_image,
					'home_section2_description'	=> $request->home_section2_description,
					'home_testimonial_title'	=> $request->home_testimonial_title,
					'home_testimonial_description'	=> $request->home_testimonial_description,
					'meta_title'			=> $request->meta_title,
					'meta_keywords'			=> $request->meta_keywords,
					'meta_description'			=> $request->meta_description,
				]);
           

        

		if($data_update)
            $request->session()->flash('success', 'Home Page updated successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();


  }
  
	public function home_box()
	{
		$data = DB::table('homepage_boxes')
				->orderBy('hbox_order','desc') 
              
				
				->get();
		
		
		
        //return view('cp-admin.pages.edit', compact('data','details'));
		return View::make('cp-admin.mainpage.home_box_list')->with([
			'data'=>$data
			
		
		])->render();
		
       
    }
	public function home_box_edit($id)
	{
		$id = Crypt::decrypt($id);
		
		$data = DB::table('homepage_boxes')
				->where('hbox_id',$id)    
              
				
				->first();
		
		
		
        //return view('cp-admin.pages.edit', compact('data','details'));
		return View::make('cp-admin.mainpage.home_box_edit')->with([
			'data'=>$data
			
		
		])->render();
		
       
    }
	public function home_box_update(Request $request)
	{ 


		$id = 1;  
			

		if($request->hasFile('hbox_image')) 
		{
			
			//echo $image_name; exit();
			
			$imagefile 	= $request->file('hbox_image') ;

			
			$extension 	= $imagefile->getClientOriginalExtension();
			$ImgOriginalName = $imagefile->getClientOriginalName();
			
			list($imgName, $ext) = explode(".", $ImgOriginalName);
				$image_name = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($imgName)));
				$image_name = $image_name;
				$fileName 	= $image_name.".".$extension;
				$img_path 	= public_path().'/homeassets/';
				
				
			
			$fileName = $this->file_check($img_path,$fileName,$fileName);
			//check image name exist or not ends here
			
			
			//plastic-cards-australia-
			$folderpath = public_path().'/homeassets/';
			$imagefile->move($folderpath , $fileName);
			$hbox_image	= $fileName;
			
			$cmplteBannerurl = $folderpath.$fileName;
				Image::make($cmplteBannerurl)
						->resize(282,162)
						->save($cmplteBannerurl);
						
			
		}
		else
		{
			$hbox_image=$request->old_hbox_image;
		}
		
		

		$data_update	= DB::table('homepage_boxes')
				->where('hbox_id',$request->hbox_id)
				->update([
					'hbox_title' 	=> $request->hbox_title,
					'hbox_image' 	=> $hbox_image
				]);
           

        

		if($data_update)
            $request->session()->flash('success', 'Content updated successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();


	}
	
	// CLients session 
	
	public function home_clients()
	{
		$data = DB::table('homepage_clients')
				->orderBy('clients_order','asc')    
				->get();
		
		
		
        //return view('cp-admin.pages.edit', compact('data','details'));
		return View::make('cp-admin.mainpage.home_clients_list')->with([
			'data'=>$data
		])->render();
		
       
    }
	public function home_clients_add()
	{
	
		
        //return view('cp-admin.pages.edit', compact('data','details'));
		return View::make('cp-admin.mainpage.home_clients_add')->render();
		
       
    }
	public function home_clients_save(Request $request)
	{ 
		if($request->hasFile('clients_image')) 
		{
			$imagefile 	= $request->file('clients_image') ;
			$extension 	= $imagefile->getClientOriginalExtension();
			$ImgOriginalName = $imagefile->getClientOriginalName();
			
			list($imgName, $ext) = explode(".", $ImgOriginalName);
				$image_name = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($imgName)));
				$image_name = $image_name;
				$fileName 	= $image_name.".".$extension;
				$img_path 	= public_path().'/homeassets/';
			
			$fileName = $this->file_check($img_path,$fileName,$fileName);
			//check image name exist or not ends here
			
			$folderpath = public_path().'/homeassets/';
			$imagefile->move($folderpath , $fileName);
			$clients_image	= $fileName;
			
			$cmplteBannerurl = $folderpath.$fileName;
				Image::make($cmplteBannerurl)
						
						->save($cmplteBannerurl);
						
			
		}

		
		

		$data_update	= DB::table('homepage_clients')				
				->insert([
					'clients_title' 	=> $request->clients_title,
					'clients_image' 	=> $clients_image
				]);
           

        

		if($data_update)
            $request->session()->flash('success', 'Content updated successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();


	}
	public function home_clients_edit($id)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('homepage_clients')
				->where('clients_id',$id)       
				->first();
        //return view('cp-admin.pages.edit', compact('data','details'));
		return View::make('cp-admin.mainpage.home_clients_edit')->with([
			'data'=>$data
		])->render();
    }
	public function home_clients_delete($id,Request $request)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('homepage_clients')
				->where('clients_id',$id)       
				->delete();
        if($data)
            $request->session()->flash('success', 'Content deleted successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }
	public function home_clients_update(Request $request)
	{ 
		if($request->hasFile('clients_image')) 
		{
			$imagefile 	= $request->file('clients_image') ;
			$extension 	= $imagefile->getClientOriginalExtension();
			$ImgOriginalName = $imagefile->getClientOriginalName();
			
			list($imgName, $ext) = explode(".", $ImgOriginalName);
				$image_name = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($imgName)));
				$image_name = $image_name;
				$fileName 	= $image_name.".".$extension;
				$img_path 	= public_path().'/homeassets/';
			
			$fileName = $this->file_check($img_path,$fileName,$fileName);
			//check image name exist or not ends here
			
			$folderpath = public_path().'/homeassets/';
			$imagefile->move($folderpath , $fileName);
			$clients_image	= $fileName;
			
			$cmplteBannerurl = $folderpath.$fileName;
				Image::make($cmplteBannerurl)
						
						->save($cmplteBannerurl);
						
			
		}
		else
		{
			$clients_image=$request->old_clients_image;
		}
		
		

		$data_update	= DB::table('homepage_clients')
				->where('clients_id',$request->clients_id)
				->update([
					'clients_title' 	=> $request->clients_title,
					'clients_image' 	=> $clients_image
				]);
           

        

		if($data_update)
            $request->session()->flash('success', 'Content updated successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();


	}
	public function sort_order()
	{
		$new_order_array = $_REQUEST['item'];
		if(count($new_order_array)>0)
		{
			foreach($new_order_array as $key=>$value)
			{
				DB::table('homepage_clients')
					->where('clients_id',$value)					
					->update([
								'clients_order'		=> $key+1,						
								'clients_updated_at'   => date('Y-m-d h:i:s')
								
							]);
				
			}
			
		}
	
		
        //return view('cp-admin.pages.edit', compact('data','details'));
		//return View::make('cp-admin.mainpage.home_clients_add')->render();
		
       
    }
	public function file_check($image_path, $filename,$org_filename)
	{
		
		if (file_exists($image_path.$filename)) 
		{  
			list($txt, $ext) = explode(".", $filename);
			$tmp = explode("-",$txt);
			
			
			$checknum = end($tmp);
			
			if(!is_numeric($checknum)) {
				$num = 1; 
			}
			else {
				
				$txt_arr = explode("-", $txt);
				array_pop($txt_arr);
				$txt = implode("-",$txt_arr);
				
				$num = $checknum +1;  
			}
			$filename 	= $txt."-".$num.".".$ext;
			
			return  $this->file_check($image_path,$filename, $org_filename);
	
		}
		else
		{
			return $filename;
		}
		
		
	}
}
