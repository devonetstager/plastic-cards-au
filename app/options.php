<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class options extends Model
{
    protected $table = 'options';

    protected $primaryKey = 'id';

    protected $fillable = [
        "youtube", "linkedin", "googleplus", "instagram", "twitter", "facebook", "address", "email", "phonenumber", "mobile"
    ];
}
