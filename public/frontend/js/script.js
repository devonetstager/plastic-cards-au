//CUSTOM FUNCTIONS
function validation_check_contact() {
	
	var check = 0;
	$(".val_req").each(function(){
		if($(this).val()=="")
		{
			$(this).next('span').addClass('error').text("Required");
			check = 1;
		}
	});

	$(".val_email").each(function(){
		email	= $(this).val();
		if(email!="")
		{
			var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
			if (!pattern.test(email)) {
				$(this).next('span').addClass('error').text("Invalid Email Id");
				$(this).val("");
				check = 1;
			}
		}
	});
	$(".val_phone").each(function(){
			phone	= $(this).val();
			if(phone!="")
			{
				var pattern = /^[0-9-+ ]+$/;
				if (!pattern.test(phone)) {
					$(this).next('span').addClass('error').text("Invalid phone number");
					$(this).val("");
					 check = 1;
				}
			}
	});
	
	$(".val_number").each(function(){
			snumber	= $(this).val();
			if(snumber!="")
			{
				var pattern = /^[0-9-+ ]+$/;
				if (!pattern.test(snumber)) {
					$(this).next('span').addClass('error').text("Invalid number");
					$(this).val("");
					 check = 1;
				}
			}
	});
        



	if(check == 1){
		$('html, body').animate({
			scrollTop: $('.error').offset().top - 150 
		}, 'slow');
		return false;
	}
	else
	{
		 return true;
	}
}

$(".hdfrm").mouseover(function() {
	$(this).removeClass('error').html("");
});

//CUSTOM FUNCTIONS END

// Multi-Toggle Navigation

$(function() {
    $('body').addClass('js');
    var $menu = $('#menu'),
        $menulink = $('.menu-link'),
        $menuTrigger = $('.has-subnav');
		$menulink.click(function(e) {







        e.preventDefault();







        $menulink.toggleClass('active1');







        $menu.toggleClass('active1');







    });









    $menuTrigger.click(function(e) {







        e.preventDefault();







        var $this = $(this);







        $this.toggleClass('active1').next('ul').toggleClass('active1');







    });









});









// Remove "Active" Class from Menu on Resize







$(window).resize(function() {







    var viewportWidth = $(window).width();







    if (viewportWidth > 925) {







        $("#menu").removeClass("active1");







    }







});







document.querySelector("#nav-toggle").addEventListener("click", function() {







    this.classList.toggle(".active1");







});







$(window).scroll(function() {







    $("header").toggleClass("aniPos", $(this).scrollTop() > 0);







});







// Multi-Toggle Navigation







$(function() {







    $('body').addClass('js');







    $('#menu').find(".menu-item-has-children").prepend('<span class="has-subnav"></span>');







    var $menu = $('#menu'),







        $menulink = $('.menu-link'),







        $menuTrigger = $('.has-subnav');









    $menulink.click(function(e) {







        e.preventDefault();







        $menulink.toggleClass('.active');







        $menu.toggleClass('.active');







    });









    $menuTrigger.click(function(e) {







        e.preventDefault();







        var $this = $(this);







        $this.toggleClass('active1').next('a').next('ul').toggleClass('active1');







    });









    $(".caret").click(function() {







        $(".dropdown-menu").toggle();







    });









});









jQuery(".wpcf7-form-control-wrap").mouseover(function() {







    $obj = $("span.wpcf7-not-valid-tip", this);







    $obj.css('display', 'none');







});









//Light Slider









(function($, undefined) {









    'use strict';









    var defaults = {









        item: 3,









        autoWidth: false,









        slideMove: 1,









        slideMargin: 10,









        addClass: '',









        mode: 'slide',









        useCSS: true,









        cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',









        easing: 'linear', //'for jquery animation',//









        speed: 400, //ms'









        auto: false,









        pauseOnHover: false,









        loop: false,









        slideEndAnimation: true,









        pause: 2000,









        keyPress: false,









        controls: true,









        prevHtml: '',









        nextHtml: '',









        rtl: false,









        adaptiveHeight: false,









        vertical: false,









        verticalHeight: 500,









        vThumbWidth: 100,









        thumbItem: 10,









        pager: true,









        gallery: false,









        galleryMargin: 5,









        thumbMargin: 5,









        currentPagerPosition: 'middle',









        enableTouch: true,









        enableDrag: true,









        freeMove: true,









        swipeThreshold: 40,









        responsive: [],









        /* jshint ignore:start */









        onBeforeStart: function($el) {},









        onSliderLoad: function($el) {},









        onBeforeSlide: function($el, scene) {},









        onAfterSlide: function($el, scene) {},









        onBeforeNextSlide: function($el, scene) {},









        onBeforePrevSlide: function($el, scene) {}









        /* jshint ignore:end */









    };









    $.fn.lightSlider = function(options) {









        if (this.length === 0) {









            return this;









        }









        if (this.length > 1) {









            this.each(function() {









                $(this).lightSlider(options);









            });









            return this;









        }









        var plugin = {},









            settings = $.extend(true, {}, defaults, options),









            settingsTemp = {},









            $el = this;









        plugin.$el = this;









        if (settings.mode === 'fade') {









            settings.vertical = false;









        }









        var $children = $el.children(),









            windowW = $(window).width(),









            breakpoint = null,









            resposiveObj = null,









            length = 0,









            w = 0,









            on = false,









            elSize = 0,









            $slide = '',









            scene = 0,









            property = (settings.vertical === true) ? 'height' : 'width',









            gutter = (settings.vertical === true) ? 'margin-bottom' : 'margin-right',









            slideValue = 0,









            pagerWidth = 0,









            slideWidth = 0,









            thumbWidth = 0,









            interval = null,









            isTouch = ('ontouchstart' in document.documentElement);









        var refresh = {};









        refresh.chbreakpoint = function() {









            windowW = $(window).width();









            if (settings.responsive.length) {









                var item;









                if (settings.autoWidth === false) {









                    item = settings.item;









                }









                if (windowW < settings.responsive[0].breakpoint) {









                    for (var i = 0; i < settings.responsive.length; i++) {









                        if (windowW < settings.responsive[i].breakpoint) {









                            breakpoint = settings.responsive[i].breakpoint;









                            resposiveObj = settings.responsive[i];









                        }









                    }









                }









                if (typeof resposiveObj !== 'undefined' && resposiveObj !== null) {









                    for (var j in resposiveObj.settings) {









                        if (resposiveObj.settings.hasOwnProperty(j)) {









                            if (typeof settingsTemp[j] === 'undefined' || settingsTemp[j] === null) {









                                settingsTemp[j] = settings[j];









                            }









                            settings[j] = resposiveObj.settings[j];









                        }









                    }









                }









                if (!$.isEmptyObject(settingsTemp) && windowW > settings.responsive[0].breakpoint) {









                    for (var k in settingsTemp) {









                        if (settingsTemp.hasOwnProperty(k)) {









                            settings[k] = settingsTemp[k];









                        }









                    }









                }









                if (settings.autoWidth === false) {









                    if (slideValue > 0 && slideWidth > 0) {









                        if (item !== settings.item) {









                            scene = Math.round(slideValue / ((slideWidth + settings.slideMargin) * settings.slideMove));









                        }









                    }









                }









            }









        };









        refresh.calSW = function() {









            if (settings.autoWidth === false) {









                slideWidth = (elSize - ((settings.item * (settings.slideMargin)) - settings.slideMargin)) / settings.item;









            }









        };









        refresh.calWidth = function(cln) {









            var ln = cln === true ? $slide.find('.lslide').length : $children.length;









            if (settings.autoWidth === false) {









                w = ln * (slideWidth + settings.slideMargin);









            } else {









                w = 0;









                for (var i = 0; i < ln; i++) {









                    w += (parseInt($children.eq(i).width()) + settings.slideMargin);









                }









            }









            return w;









        };









        plugin = {









            doCss: function() {









                var support = function() {









                    var transition = ['transition', 'MozTransition', 'WebkitTransition', 'OTransition', 'msTransition', 'KhtmlTransition'];









                    var root = document.documentElement;









                    for (var i = 0; i < transition.length; i++) {









                        if (transition[i] in root.style) {









                            return true;









                        }









                    }









                };









                if (settings.useCSS && support()) {









                    return true;









                }









                return false;









            },









            keyPress: function() {









                if (settings.keyPress) {









                    $(document).on('keyup.lightslider', function(e) {









                        if (!$(':focus').is('input, textarea')) {









                            if (e.preventDefault) {









                                e.preventDefault();









                            } else {









                                e.returnValue = false;









                            }









                            if (e.keyCode === 37) {









                                $el.goToPrevSlide();









                            } else if (e.keyCode === 39) {









                                $el.goToNextSlide();









                            }









                        }









                    });









                }









            },









            controls: function() {









                if (settings.controls) {









                    $el.after('<div class="lSAction"><a class="lSPrev">' + settings.prevHtml + '</a><a class="lSNext">' + settings.nextHtml + '</a></div>');









                    if (!settings.autoWidth) {









                        if (length <= settings.item) {









                            $slide.find('.lSAction').hide();









                        }









                    } else {









                        if (refresh.calWidth(false) < elSize) {









                            $slide.find('.lSAction').hide();









                        }









                    }









                    $slide.find('.lSAction a').on('click', function(e) {









                        if (e.preventDefault) {









                            e.preventDefault();









                        } else {









                            e.returnValue = false;









                        }









                        if ($(this).attr('class') === 'lSPrev') {









                            $el.goToPrevSlide();









                        } else {









                            $el.goToNextSlide();









                        }









                        return false;









                    });









                }









            },









            initialStyle: function() {









                var $this = this;









                if (settings.mode === 'fade') {









                    settings.autoWidth = false;









                    settings.slideEndAnimation = false;









                }









                if (settings.auto) {









                    settings.slideEndAnimation = false;









                }









                if (settings.autoWidth) {









                    settings.slideMove = 1;









                    settings.item = 1;









                }









                if (settings.loop) {









                    settings.slideMove = 1;









                    settings.freeMove = false;









                }









                settings.onBeforeStart.call(this, $el);









                refresh.chbreakpoint();









                $el.addClass('lightSlider').wrap('<div class="lSSlideOuter ' + settings.addClass + '"><div class="lSSlideWrapper"></div></div>');









                $slide = $el.parent('.lSSlideWrapper');









                if (settings.rtl === true) {









                    $slide.parent().addClass('lSrtl');









                }









                if (settings.vertical) {









                    $slide.parent().addClass('vertical');









                    elSize = settings.verticalHeight;









                    $slide.css('height', elSize + 'px');









                } else {









                    elSize = $el.outerWidth();









                }









                $children.addClass('lslide');









                if (settings.loop === true && settings.mode === 'slide') {









                    refresh.calSW();









                    refresh.clone = function() {









                        if (refresh.calWidth(true) > elSize) {









                            /**/









                            var tWr = 0,









                                tI = 0;









                            for (var k = 0; k < $children.length; k++) {









                                tWr += (parseInt($el.find('.lslide').eq(k).width()) + settings.slideMargin);









                                tI++;









                                if (tWr >= (elSize + settings.slideMargin)) {









                                    break;









                                }









                            }









                            var tItem = settings.autoWidth === true ? tI : settings.item;









                            /**/









                            if (tItem < $el.find('.clone.left').length) {









                                for (var i = 0; i < $el.find('.clone.left').length - tItem; i++) {









                                    $children.eq(i).remove();









                                }









                            }









                            if (tItem < $el.find('.clone.right').length) {









                                for (var j = $children.length - 1; j > ($children.length - 1 - $el.find('.clone.right').length); j--) {









                                    scene--;









                                    $children.eq(j).remove();









                                }









                            }









                            /**/









                            for (var n = $el.find('.clone.right').length; n < tItem; n++) {









                                $el.find('.lslide').eq(n).clone().removeClass('lslide').addClass('clone right').appendTo($el);









                                scene++;









                            }









                            for (var m = $el.find('.lslide').length - $el.find('.clone.left').length; m > ($el.find('.lslide').length - tItem); m--) {









                                $el.find('.lslide').eq(m - 1).clone().removeClass('lslide').addClass('clone left').prependTo($el);









                            }









                            $children = $el.children();









                        } else {









                            if ($children.hasClass('clone')) {









                                $el.find('.clone').remove();









                                $this.move($el, 0);









                            }









                        }









                    };









                    refresh.clone();









                }









                refresh.sSW = function() {









                    length = $children.length;









                    if (settings.rtl === true && settings.vertical === false) {









                        gutter = 'margin-left';









                    }









                    if (settings.autoWidth === false) {









                        $children.css(property, slideWidth + 'px');









                    }









                    $children.css(gutter, settings.slideMargin + 'px');









                    w = refresh.calWidth(false);









                    $el.css(property, w + 'px');









                    if (settings.loop === true && settings.mode === 'slide') {









                        if (on === false) {









                            scene = $el.find('.clone.left').length;









                        }









                    }









                };









                refresh.calL = function() {









                    $children = $el.children();









                    length = $children.length;









                };









                if (this.doCss()) {









                    $slide.addClass('usingCss');









                }









                refresh.calL();









                if (settings.mode === 'slide') {









                    refresh.calSW();









                    refresh.sSW();









                    if (settings.loop === true) {









                        slideValue = $this.slideValue();









                        this.move($el, slideValue);









                    }









                    if (settings.vertical === false) {









                        this.setHeight($el, false);









                    }









                } else {









                    this.setHeight($el, true);









                    $el.addClass('lSFade');









                    if (!this.doCss()) {









                        $children.fadeOut(0);









                        $children.eq(scene).fadeIn(0);









                    }









                }









                if (settings.loop === true && settings.mode === 'slide') {









                    $children.eq(scene).addClass('active');









                } else {









                    $children.first().addClass('active');









                }









            },









            pager: function() {









                var $this = this;









                refresh.createPager = function() {









                    thumbWidth = (elSize - ((settings.thumbItem * (settings.thumbMargin)) - settings.thumbMargin)) / settings.thumbItem;









                    var $children = $slide.find('.lslide');









                    var length = $slide.find('.lslide').length;









                    var i = 0,









                        pagers = '',









                        v = 0;









                    for (i = 0; i < length; i++) {









                        if (settings.mode === 'slide') {









                            // calculate scene * slide value









                            if (!settings.autoWidth) {









                                v = i * ((slideWidth + settings.slideMargin) * settings.slideMove);









                            } else {









                                v += ((parseInt($children.eq(i).width()) + settings.slideMargin) * settings.slideMove);









                            }









                        }









                        var thumb = $children.eq(i * settings.slideMove).attr('data-thumb');









                        if (settings.gallery === true) {









                            pagers += '<li style="width:100%;' + property + ':' + thumbWidth + 'px;' + gutter + ':' + settings.thumbMargin + 'px"><a href="#"><img src="' + thumb + '" /></a></li>';









                        } else {









                            pagers += '<li><a href="#">' + (i + 1) + '</a></li>';









                        }









                        if (settings.mode === 'slide') {









                            if ((v) >= w - elSize - settings.slideMargin) {









                                i = i + 1;









                                var minPgr = 2;









                                if (settings.autoWidth) {









                                    pagers += '<li><a href="#">' + (i + 1) + '</a></li>';









                                    minPgr = 1;









                                }









                                if (i < minPgr) {









                                    pagers = null;









                                    $slide.parent().addClass('noPager');









                                } else {









                                    $slide.parent().removeClass('noPager');









                                }









                                break;









                            }









                        }









                    }









                    var $cSouter = $slide.parent();









                    $cSouter.find('.lSPager').html(pagers);









                    if (settings.gallery === true) {









                        if (settings.vertical === true) {









                            // set Gallery thumbnail width









                            $cSouter.find('.lSPager').css('width', settings.vThumbWidth + 'px');









                        }









                        pagerWidth = (i * (settings.thumbMargin + thumbWidth)) + 0.5;









                        $cSouter.find('.lSPager').css({









                            property: pagerWidth + 'px',









                            'transition-duration': settings.speed + 'ms'









                        });









                        if (settings.vertical === true) {









                            $slide.parent().css('padding-right', (settings.vThumbWidth + settings.galleryMargin) + 'px');









                        }









                        $cSouter.find('.lSPager').css(property, pagerWidth + 'px');









                    }









                    var $pager = $cSouter.find('.lSPager').find('li');









                    $pager.first().addClass('active');









                    $pager.on('click', function() {









                        if (settings.loop === true && settings.mode === 'slide') {









                            scene = scene + ($pager.index(this) - $cSouter.find('.lSPager').find('li.active').index());









                        } else {









                            scene = $pager.index(this);









                        }









                        $el.mode(false);









                        if (settings.gallery === true) {









                            $this.slideThumb();









                        }









                        return false;









                    });









                };









                if (settings.pager) {









                    var cl = 'lSpg';









                    if (settings.gallery) {









                        cl = 'lSGallery';









                    }









                    $slide.after('<ul class="lSPager ' + cl + '"></ul>');









                    var gMargin = (settings.vertical) ? 'margin-left' : 'margin-top';









                    $slide.parent().find('.lSPager').css(gMargin, settings.galleryMargin + 'px');









                    refresh.createPager();









                }









                setTimeout(function() {









                    refresh.init();









                }, 0);









            },









            setHeight: function(ob, fade) {









                var obj = null,









                    $this = this;









                if (settings.loop) {









                    obj = ob.children('.lslide ').first();









                } else {









                    obj = ob.children().first();









                }









                var setCss = function() {









                    var tH = obj.outerHeight(),









                        tP = 0,









                        tHT = tH;









                    if (fade) {









                        tH = 0;









                        tP = ((tHT) * 100) / elSize;









                    }









                    ob.css({









                        'height': tH + 'px',









                        'padding-bottom': tP + '%'









                    });









                };









                setCss();









                if (obj.find('img').length) {









                    if (obj.find('img')[0].complete) {









                        setCss();









                        if (!interval) {









                            $this.auto();









                        }









                    } else {









                        obj.find('img').load(function() {









                            setTimeout(function() {









                                setCss();









                                if (!interval) {









                                    $this.auto();









                                }









                            }, 100);









                        });









                    }









                } else {









                    if (!interval) {









                        $this.auto();









                    }









                }









            },









            active: function(ob, t) {









                if (this.doCss() && settings.mode === 'fade') {









                    $slide.addClass('on');









                }









                var sc = 0;









                if (scene * settings.slideMove < length) {









                    ob.removeClass('active');









                    if (!this.doCss() && settings.mode === 'fade' && t === false) {









                        ob.fadeOut(settings.speed);









                    }









                    if (t === true) {









                        sc = scene;









                    } else {









                        sc = scene * settings.slideMove;









                    }









                    //t === true ? sc = scene : sc = scene * settings.slideMove;









                    var l, nl;









                    if (t === true) {









                        l = ob.length;









                        nl = l - 1;









                        if (sc + 1 >= l) {









                            sc = nl;









                        }









                    }









                    if (settings.loop === true && settings.mode === 'slide') {









                        //t === true ? sc = scene - $el.find('.clone.left').length : sc = scene * settings.slideMove;









                        if (t === true) {









                            sc = scene - $el.find('.clone.left').length;









                        } else {









                            sc = scene * settings.slideMove;









                        }









                        if (t === true) {









                            l = ob.length;









                            nl = l - 1;









                            if (sc + 1 === l) {









                                sc = nl;









                            } else if (sc + 1 > l) {









                                sc = 0;









                            }









                        }









                    }









                    if (!this.doCss() && settings.mode === 'fade' && t === false) {









                        ob.eq(sc).fadeIn(settings.speed);









                    }









                    ob.eq(sc).addClass('active');









                } else {









                    ob.removeClass('active');









                    ob.eq(ob.length - 1).addClass('active');









                    if (!this.doCss() && settings.mode === 'fade' && t === false) {









                        ob.fadeOut(settings.speed);









                        ob.eq(sc).fadeIn(settings.speed);









                    }









                }









            },









            move: function(ob, v) {









                if (settings.rtl === true) {









                    v = -v;









                }









                if (this.doCss()) {









                    if (settings.vertical === true) {









                        ob.css({









                            'transform': 'translate3d(0px, ' + (-v) + 'px, 0px)',









                            '-webkit-transform': 'translate3d(0px, ' + (-v) + 'px, 0px)'









                        });









                    } else {









                        ob.css({









                            'transform': 'translate3d(' + (-v) + 'px, 0px, 0px)',









                            '-webkit-transform': 'translate3d(' + (-v) + 'px, 0px, 0px)',









                        });









                    }









                } else {









                    if (settings.vertical === true) {









                        ob.css('position', 'relative').animate({









                            top: -v + 'px'









                        }, settings.speed, settings.easing);









                    } else {









                        ob.css('position', 'relative').animate({









                            left: -v + 'px'









                        }, settings.speed, settings.easing);









                    }









                }









                var $thumb = $slide.parent().find('.lSPager').find('li');









                this.active($thumb, true);









            },









            fade: function() {









                this.active($children, false);









                var $thumb = $slide.parent().find('.lSPager').find('li');









                this.active($thumb, true);









            },









            slide: function() {









                var $this = this;









                refresh.calSlide = function() {









                    if (w > elSize) {









                        slideValue = $this.slideValue();









                        $this.active($children, false);









                        if ((slideValue) > w - elSize - settings.slideMargin) {









                            slideValue = w - elSize - settings.slideMargin;









                        } else if (slideValue < 0) {









                            slideValue = 0;









                        }









                        $this.move($el, slideValue);









                        if (settings.loop === true && settings.mode === 'slide') {









                            if (scene >= (length - ($el.find('.clone.left').length / settings.slideMove))) {









                                $this.resetSlide($el.find('.clone.left').length);









                            }









                            if (scene === 0) {









                                $this.resetSlide($slide.find('.lslide').length);









                            }









                        }









                    }









                };









                refresh.calSlide();









            },









            resetSlide: function(s) {









                var $this = this;









                $slide.find('.lSAction a').addClass('disabled');









                setTimeout(function() {









                    scene = s;









                    $slide.css('transition-duration', '0ms');









                    slideValue = $this.slideValue();









                    $this.active($children, false);









                    plugin.move($el, slideValue);









                    setTimeout(function() {









                        $slide.css('transition-duration', settings.speed + 'ms');









                        $slide.find('.lSAction a').removeClass('disabled');









                    }, 50);









                }, settings.speed + 100);









            },









            slideValue: function() {









                var _sV = 0;









                if (settings.autoWidth === false) {









                    _sV = scene * ((slideWidth + settings.slideMargin) * settings.slideMove);









                } else {









                    _sV = 0;









                    for (var i = 0; i < scene; i++) {









                        _sV += (parseInt($children.eq(i).width()) + settings.slideMargin);









                    }









                }









                return _sV;









            },









            slideThumb: function() {









                var position;









                switch (settings.currentPagerPosition) {









                    case 'left':









                        position = 0;









                        break;









                    case 'middle':









                        position = (elSize / 2) - (thumbWidth / 2);









                        break;









                    case 'right':









                        position = elSize - thumbWidth;









                }









                var sc = scene - $el.find('.clone.left').length;









                var $pager = $slide.parent().find('.lSPager');









                if (settings.mode === 'slide' && settings.loop === true) {









                    if (sc >= $pager.children().length) {









                        sc = 0;









                    } else if (sc < 0) {









                        sc = $pager.children().length;









                    }









                }









                var thumbSlide = sc * ((thumbWidth + settings.thumbMargin)) - (position);









                if ((thumbSlide + elSize) > pagerWidth) {









                    thumbSlide = pagerWidth - elSize - settings.thumbMargin;









                }









                if (thumbSlide < 0) {









                    thumbSlide = 0;









                }









                this.move($pager, thumbSlide);









            },









            auto: function() {









                if (settings.auto) {









                    clearInterval(interval);









                    interval = setInterval(function() {









                        $el.goToNextSlide();









                    }, settings.pause);









                }









            },









            pauseOnHover: function() {









                var $this = this;









                if (settings.auto && settings.pauseOnHover) {









                    $slide.on('mouseenter', function() {









                        $(this).addClass('ls-hover');









                        $el.pause();









                        settings.auto = true;









                    });









                    $slide.on('mouseleave', function() {









                        $(this).removeClass('ls-hover');









                        if (!$slide.find('.lightSlider').hasClass('lsGrabbing')) {









                            $this.auto();









                        }









                    });









                }









            },









            touchMove: function(endCoords, startCoords) {









                $slide.css('transition-duration', '0ms');









                if (settings.mode === 'slide') {









                    var distance = endCoords - startCoords;









                    var swipeVal = slideValue - distance;









                    if ((swipeVal) >= w - elSize - settings.slideMargin) {









                        if (settings.freeMove === false) {









                            swipeVal = w - elSize - settings.slideMargin;









                        } else {









                            var swipeValT = w - elSize - settings.slideMargin;









                            swipeVal = swipeValT + ((swipeVal - swipeValT) / 5);









                        }









                    } else if (swipeVal < 0) {









                        if (settings.freeMove === false) {









                            swipeVal = 0;









                        } else {









                            swipeVal = swipeVal / 5;









                        }









                    }









                    this.move($el, swipeVal);









                }









            },









            touchEnd: function(distance) {









                $slide.css('transition-duration', settings.speed + 'ms');









                if (settings.mode === 'slide') {









                    var mxVal = false;









                    var _next = true;









                    slideValue = slideValue - distance;









                    if ((slideValue) > w - elSize - settings.slideMargin) {









                        slideValue = w - elSize - settings.slideMargin;









                        if (settings.autoWidth === false) {









                            mxVal = true;









                        }









                    } else if (slideValue < 0) {









                        slideValue = 0;









                    }









                    var gC = function(next) {









                        var ad = 0;









                        if (!mxVal) {









                            if (next) {









                                ad = 1;









                            }









                        }









                        if (!settings.autoWidth) {









                            var num = slideValue / ((slideWidth + settings.slideMargin) * settings.slideMove);









                            scene = parseInt(num) + ad;









                            if (slideValue >= (w - elSize - settings.slideMargin)) {









                                if (num % 1 !== 0) {









                                    scene++;









                                }









                            }









                        } else {









                            var tW = 0;









                            for (var i = 0; i < $children.length; i++) {









                                tW += (parseInt($children.eq(i).width()) + settings.slideMargin);









                                scene = i + ad;









                                if (tW >= slideValue) {









                                    break;









                                }









                            }









                        }









                    };









                    if (distance >= settings.swipeThreshold) {









                        gC(false);









                        _next = false;









                    } else if (distance <= -settings.swipeThreshold) {









                        gC(true);









                        _next = false;









                    }









                    $el.mode(_next);









                    this.slideThumb();









                } else {









                    if (distance >= settings.swipeThreshold) {









                        $el.goToPrevSlide();









                    } else if (distance <= -settings.swipeThreshold) {









                        $el.goToNextSlide();









                    }









                }









            },









            enableDrag: function() {









                var $this = this;









                if (!isTouch) {









                    var startCoords = 0,









                        endCoords = 0,









                        isDraging = false;









                    $slide.find('.lightSlider').addClass('lsGrab');









                    $slide.on('mousedown', function(e) {









                        if (w < elSize) {









                            if (w !== 0) {









                                return false;









                            }









                        }









                        if ($(e.target).attr('class') !== ('lSPrev') && $(e.target).attr('class') !== ('lSNext')) {









                            startCoords = (settings.vertical === true) ? e.pageY : e.pageX;









                            isDraging = true;









                            if (e.preventDefault) {









                                e.preventDefault();









                            } else {









                                e.returnValue = false;









                            }









                            // ** Fix for webkit cursor issue https://code.google.com/p/chromium/issues/detail?id=26723









                            $slide.scrollLeft += 1;









                            $slide.scrollLeft -= 1;









                            // *









                            $slide.find('.lightSlider').removeClass('lsGrab').addClass('lsGrabbing');









                            clearInterval(interval);









                        }









                    });









                    $(window).on('mousemove', function(e) {









                        if (isDraging) {









                            endCoords = (settings.vertical === true) ? e.pageY : e.pageX;









                            $this.touchMove(endCoords, startCoords);









                        }









                    });









                    $(window).on('mouseup', function(e) {









                        if (isDraging) {









                            $slide.find('.lightSlider').removeClass('lsGrabbing').addClass('lsGrab');









                            isDraging = false;









                            endCoords = (settings.vertical === true) ? e.pageY : e.pageX;









                            var distance = endCoords - startCoords;









                            if (Math.abs(distance) >= settings.swipeThreshold) {









                                $(window).on('click.ls', function(e) {









                                    if (e.preventDefault) {









                                        e.preventDefault();









                                    } else {









                                        e.returnValue = false;









                                    }









                                    e.stopImmediatePropagation();









                                    e.stopPropagation();









                                    $(window).off('click.ls');









                                });









                            }









                            $this.touchEnd(distance);









                        }









                    });









                }









            },









            enableTouch: function() {









                var $this = this;









                if (isTouch) {









                    var startCoords = {},









                        endCoords = {};









                    $slide.on('touchstart', function(e) {









                        endCoords = e.originalEvent.targetTouches[0];









                        startCoords.pageX = e.originalEvent.targetTouches[0].pageX;









                        startCoords.pageY = e.originalEvent.targetTouches[0].pageY;









                        clearInterval(interval);









                    });









                    $slide.on('touchmove', function(e) {









                        if (w < elSize) {









                            if (w !== 0) {









                                return false;









                            }









                        }









                        var orig = e.originalEvent;









                        endCoords = orig.targetTouches[0];









                        var xMovement = Math.abs(endCoords.pageX - startCoords.pageX);









                        var yMovement = Math.abs(endCoords.pageY - startCoords.pageY);









                        if (settings.vertical === true) {









                            if ((yMovement * 3) > xMovement) {









                                e.preventDefault();









                            }









                            $this.touchMove(endCoords.pageY, startCoords.pageY);









                        } else {









                            if ((xMovement * 3) > yMovement) {









                                e.preventDefault();









                            }









                            $this.touchMove(endCoords.pageX, startCoords.pageX);









                        }









                    });









                    $slide.on('touchend', function() {









                        if (w < elSize) {









                            if (w !== 0) {









                                return false;









                            }









                        }









                        var distance;









                        if (settings.vertical === true) {









                            distance = endCoords.pageY - startCoords.pageY;









                        } else {









                            distance = endCoords.pageX - startCoords.pageX;









                        }









                        $this.touchEnd(distance);









                    });









                }









            },









            build: function() {









                var $this = this;









                $this.initialStyle();









                if (this.doCss()) {









                    if (settings.enableTouch === true) {









                        $this.enableTouch();









                    }









                    if (settings.enableDrag === true) {









                        $this.enableDrag();









                    }









                }









                $(window).on('focus', function() {









                    $this.auto();









                });









                $(window).on('blur', function() {









                    clearInterval(interval);









                });









                $this.pager();









                $this.pauseOnHover();









                $this.controls();









                $this.keyPress();









            }









        };









        plugin.build();









        refresh.init = function() {









            refresh.chbreakpoint();









            if (settings.vertical === true) {









                if (settings.item > 1) {









                    elSize = settings.verticalHeight;









                } else {









                    elSize = $children.outerHeight();









                }









                $slide.css('height', elSize + 'px');









            } else {









                elSize = $slide.outerWidth();









            }









            if (settings.loop === true && settings.mode === 'slide') {









                refresh.clone();









            }









            refresh.calL();









            if (settings.mode === 'slide') {









                $el.removeClass('lSSlide');









            }









            if (settings.mode === 'slide') {









                refresh.calSW();









                refresh.sSW();









            }









            setTimeout(function() {









                if (settings.mode === 'slide') {









                    $el.addClass('lSSlide');









                }









            }, 1000);









            if (settings.pager) {









                refresh.createPager();









            }









            if (settings.adaptiveHeight === true && settings.vertical === false) {









                $el.css('height', $children.eq(scene).outerHeight(true));









            }









            if (settings.adaptiveHeight === false) {









                if (settings.mode === 'slide') {









                    if (settings.vertical === false) {









                        plugin.setHeight($el, false);









                    } else {









                        plugin.auto();









                    }









                } else {









                    plugin.setHeight($el, true);









                }









            }









            if (settings.gallery === true) {









                plugin.slideThumb();









            }









            if (settings.mode === 'slide') {









                plugin.slide();









            }









            if (settings.autoWidth === false) {









                if ($children.length <= settings.item) {









                    $slide.find('.lSAction').hide();









                } else {









                    $slide.find('.lSAction').show();









                }









            } else {









                if ((refresh.calWidth(false) < elSize) && (w !== 0)) {









                    $slide.find('.lSAction').hide();









                } else {









                    $slide.find('.lSAction').show();









                }









            }









        };









        $el.goToPrevSlide = function() {









            if (scene > 0) {









                settings.onBeforePrevSlide.call(this, $el, scene);









                scene--;









                $el.mode(false);









                if (settings.gallery === true) {









                    plugin.slideThumb();









                }









            } else {









                if (settings.loop === true) {









                    settings.onBeforePrevSlide.call(this, $el, scene);









                    if (settings.mode === 'fade') {









                        var l = (length - 1);









                        scene = parseInt(l / settings.slideMove);









                    }









                    $el.mode(false);









                    if (settings.gallery === true) {









                        plugin.slideThumb();









                    }









                } else if (settings.slideEndAnimation === true) {









                    $el.addClass('leftEnd');









                    setTimeout(function() {









                        $el.removeClass('leftEnd');









                    }, 400);









                }









            }









        };









        $el.goToNextSlide = function() {









            var nextI = true;









            if (settings.mode === 'slide') {









                var _slideValue = plugin.slideValue();









                nextI = _slideValue < w - elSize - settings.slideMargin;









            }









            if (((scene * settings.slideMove) < length - settings.slideMove) && nextI) {









                settings.onBeforeNextSlide.call(this, $el, scene);









                scene++;









                $el.mode(false);









                if (settings.gallery === true) {









                    plugin.slideThumb();









                }









            } else {









                if (settings.loop === true) {









                    settings.onBeforeNextSlide.call(this, $el, scene);









                    scene = 0;









                    $el.mode(false);









                    if (settings.gallery === true) {









                        plugin.slideThumb();









                    }









                } else if (settings.slideEndAnimation === true) {









                    $el.addClass('rightEnd');









                    setTimeout(function() {









                        $el.removeClass('rightEnd');









                    }, 400);









                }









            }









        };









        $el.mode = function(_touch) {









            if (settings.adaptiveHeight === true && settings.vertical === false) {









                $el.css('height', $children.eq(scene).outerHeight(true));









            }









            if (on === false) {









                if (settings.mode === 'slide') {









                    if (plugin.doCss()) {









                        $el.addClass('lSSlide');









                        if (settings.speed !== '') {









                            $slide.css('transition-duration', settings.speed + 'ms');









                        }









                        if (settings.cssEasing !== '') {









                            $slide.css('transition-timing-function', settings.cssEasing);









                        }









                    }









                } else {









                    if (plugin.doCss()) {









                        if (settings.speed !== '') {









                            $el.css('transition-duration', settings.speed + 'ms');









                        }









                        if (settings.cssEasing !== '') {









                            $el.css('transition-timing-function', settings.cssEasing);









                        }









                    }









                }









            }









            if (!_touch) {









                settings.onBeforeSlide.call(this, $el, scene);









            }









            if (settings.mode === 'slide') {









                plugin.slide();









            } else {









                plugin.fade();









            }









            if (!$slide.hasClass('ls-hover')) {









                plugin.auto();









            }









            setTimeout(function() {









                if (!_touch) {









                    settings.onAfterSlide.call(this, $el, scene);









                }









            }, settings.speed);









            on = true;









        };









        $el.play = function() {









            $el.goToNextSlide();









            settings.auto = true;









            plugin.auto();









        };









        $el.pause = function() {









            settings.auto = false;









            clearInterval(interval);









        };









        $el.refresh = function() {









            refresh.init();









        };









        $el.getCurrentSlideCount = function() {









            var sc = scene;









            if (settings.loop) {









                var ln = $slide.find('.lslide').length,









                    cl = $el.find('.clone.left').length;









                if (scene <= cl - 1) {









                    sc = ln + (scene - cl);









                } else if (scene >= (ln + cl)) {









                    sc = scene - ln - cl;









                } else {









                    sc = scene - cl;









                }









            }









            return sc + 1;









        };









        $el.getTotalSlideCount = function() {









            return $slide.find('.lslide').length;









        };









        $el.goToSlide = function(s) {









            if (settings.loop) {









                scene = (s + $el.find('.clone.left').length - 1);









            } else {









                scene = s;









            }









            $el.mode(false);









            if (settings.gallery === true) {









                plugin.slideThumb();









            }









        };









        $el.destroy = function() {









            if ($el.lightSlider) {









                $el.goToPrevSlide = function() {};









                $el.goToNextSlide = function() {};









                $el.mode = function() {};









                $el.play = function() {};









                $el.pause = function() {};









                $el.refresh = function() {};









                $el.getCurrentSlideCount = function() {};









                $el.getTotalSlideCount = function() {};









                $el.goToSlide = function() {};









                $el.lightSlider = null;









                refresh = {









                    init: function() {}









                };









                $el.parent().parent().find('.lSAction, .lSPager').remove();









                $el.removeClass('lightSlider lSFade lSSlide lsGrab lsGrabbing leftEnd right').removeAttr('style').unwrap().unwrap();









                $el.children().removeAttr('style');









                $children.removeClass('lslide active');









                $el.find('.clone').remove();









                $children = null;









                interval = null;









                on = false;









                scene = 0;









            }









        };









        setTimeout(function() {









            settings.onSliderLoad.call(this, $el);









        }, 10);









        $(window).on('resize orientationchange', function(e) {









            setTimeout(function() {









                if (e.preventDefault) {









                    e.preventDefault();









                } else {









                    e.returnValue = false;









                }









                refresh.init();









            }, 200);









        });









        return this;









    };









}(jQuery));









$(document).ready(function() {

    $('#brandsSlider').lightSlider({

 item: 6,

 auto: false,

 loop: false,

slideMove: 1,

 mode: 'slide',

addClass: 'brandsSlider',

 easing: 'cubic-bezier(0.25, 0, 0.25, 1)',

speed: 1000,

pause: 3000,

adaptiveHeight: true,



responsive: [

   {

 breakpoint: 995,

 settings: {

 item: 4,

 slideMargin: 6,

 auto: true,

 loop: true,

 }



 },



 {



 breakpoint: 767,

settings: {



item: 3,

slideMove: 1

}

},



{



breakpoint: 479,

settings: {



item: 2,

slideMove: 1

}

}



 ]



 });





    $('#clientSlider').lightSlider({

 item: 5,

 auto: false,

 loop: false,

slideMove: 1,

 mode: 'slide',

addClass: 'clientSlider',

 easing: 'cubic-bezier(0.25, 0, 0.25, 1)',

speed: 1000,

pause: 3000,

adaptiveHeight: true,



responsive: [

   {

 breakpoint: 995,

 settings: {

 item: 4,

 slideMargin: 6,

 auto: true,

 loop: true,

 }



 },



 {



 breakpoint: 767,

settings: {



item: 3,

slideMove: 1

}

},



{



breakpoint: 479,

settings: {



item: 2,

slideMove: 1

}

}



 ]



 });



    $('#testimonial').lightSlider({

 item: 3,

 auto: true,

 loop: true,

slideMove: 1,

 mode: 'slide',

addClass: 'testimonial',

 easing: 'cubic-bezier(0.25, 0, 0.25, 1)',

speed: 1000,

pause: 3000,

adaptiveHeight: true,



responsive: [

   {

 breakpoint: 800,

 settings: {

 item: 2,

 slideMargin: 6,

 }



 },



  {



  breakpoint: 767,

 settings: {



 item: 1,

slideMove: 1

}

 }



 ]



 });


 $('#vehicle-gallery').lightSlider({
    gallery:true,
    item:1,
    thumbItem:4,
    slideMargin: 0,
    speed:1000,
    auto:true,
    loop:true,
    onSliderLoad: function() {
        $('#vehicle-gallery').removeClass('cS-hidden');
    }  
});







});









/*###Popup*/









/*!















 * jQuery Popup Overlay















 *















 * @version 1.7.13















 * @requires jQuery v1.7.1+















 * @link http://vast-engineering.github.com/jquery-popup-overlay/















 */









;

(function($) {









    var $window = $(window);









    var options = {};









    var zindexvalues = [];









    var lastclicked = [];









    var scrollbarwidth;









    var bodymarginright = null;









    var opensuffix = '_open';









    var closesuffix = '_close';









    var visiblePopupsArray = [];









    var transitionsupport = null;









    var opentimer;









    var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);









    var focusableElementsString = "a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]";









    var methods = {









        _init: function(el) {









            var $el = $(el);









            var options = $el.data('popupoptions');









            lastclicked[el.id] = false;









            zindexvalues[el.id] = 0;









            if (!$el.data('popup-initialized')) {









                $el.attr('data-popup-initialized', 'true');









                methods._initonce(el);









            }









            if (options.autoopen) {









                setTimeout(function() {









                    methods.show(el, 0);









                }, 0);









            }









        },









        _initonce: function(el) {









            var $el = $(el);









            var $body = $('body');









            var $wrapper;









            var options = $el.data('popupoptions');









            var css;









            bodymarginright = parseInt($body.css('margin-right'), 10);









            transitionsupport = document.body.style.webkitTransition !== undefined ||









                document.body.style.MozTransition !== undefined ||









                document.body.style.msTransition !== undefined ||









                document.body.style.OTransition !== undefined ||









                document.body.style.transition !== undefined;









            if (options.type == 'tooltip') {









                options.background = false;









                options.scrolllock = false;









            }









            if (options.backgroundactive) {









                options.background = false;









                options.blur = false;









                options.scrolllock = false;









            }









            if (options.scrolllock) {









                // Calculate the browser's scrollbar width dynamically









                var parent;









                var child;









                if (typeof scrollbarwidth === 'undefined') {









                    parent = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body');









                    child = parent.children();









                    scrollbarwidth = child.innerWidth() - child.height(99).innerWidth();









                    parent.remove();









                }









            }









            if (!$el.attr('id')) {









                $el.attr('id', 'j-popup-' + parseInt((Math.random() * 100000000), 10));









            }









            $el.addClass('popup_content');









            if ((options.background) && (!$('#' + el.id + '_background').length)) {









                $body.append('<div id="' + el.id + '_background" class="popup_background"></div>');









                var $background = $('#' + el.id + '_background');









                $background.css({









                    opacity: 0,

                    display: 'none',









                    visibility: 'hidden',









                    backgroundColor: options.color,









                    position: 'fixed',









                    top: 0,









                    right: 0,









                    bottom: 0,









                    left: 0









                });









                if (options.setzindex && !options.autozindex) {









                    $background.css('z-index', '100000');









                }









                if (options.transition) {









                    $background.css('transition', options.transition);









                }









            }









            $body.append(el);









            $el.wrap('<div id="' + el.id + '_wrapper" class="popup_wrapper" />');









            $wrapper = $('#' + el.id + '_wrapper');









            $wrapper.css({









                opacity: 0,

                display: 'none',









                visibility: 'hidden',









                position: 'absolute'









            });









            // Make div clickable in iOS









            if (iOS) {









                $wrapper.css('cursor', 'pointer');









            }









            if (options.type == 'overlay') {









                $wrapper.css('overflow', 'auto');









            }









            $el.css({









                opacity: 0,

                display: 'none',









                visibility: 'hidden',









                display: 'inline-block'









            });









            if (options.setzindex && !options.autozindex) {









                $wrapper.css('z-index', '100001');









            }









            if (!options.outline) {









                $el.css('outline', 'none');









            }









            if (options.transition) {









                $el.css('transition', options.transition);









                $wrapper.css('transition', options.transition);









            }









            // Hide popup content from screen readers initially









            $el.attr('aria-hidden', true);









            if (options.type == 'overlay') {









                $el.css({









                    textAlign: 'left',









                    position: 'relative',









                    verticalAlign: 'middle'









                });









                css = {









                    position: 'fixed',









                    width: '100%',









                    height: '100%',









                    top: 0,









                    left: 0,









                    textAlign: 'center'









                };









                if (options.backgroundactive) {









                    css.position = 'absolute';









                    css.height = '0';









                    css.overflow = 'visible';









                }









                $wrapper.css(css);









                // CSS vertical align helper









                $wrapper.append('<div class="popup_align" />');









                $('.popup_align').css({









                    display: 'inline-block',









                    verticalAlign: 'middle',









                    height: '100%'









                });









            }









            // Add WAI ARIA role to announce dialog to screen readers









            $el.attr('role', 'dialog');









            var openelement = (options.openelement) ? options.openelement : ('.' + el.id + opensuffix);









            $(openelement).each(function(i, item) {









                $(item).attr('data-popup-ordinal', i);









                if (!item.id) {









                    $(item).attr('id', 'open_' + parseInt((Math.random() * 100000000), 10));









                }









            });









            // Set aria-labelledby (if aria-label or aria-labelledby is not set in html)









            if (!($el.attr('aria-labelledby') || $el.attr('aria-label'))) {









                $el.attr('aria-labelledby', $(openelement).attr('id'));









            }









            // Show and hide tooltips on hover









            if (options.action == 'hover') {









                options.keepfocus = false;









                // Handler: mouseenter, focusin









                $(openelement).on('mouseenter', function(event) {









                    methods.show(el, $(this).data('popup-ordinal'));









                });









                // Handler: mouseleave, focusout









                $(openelement).on('mouseleave', function(event) {









                    methods.hide(el);









                });









            } else {









                // Handler: Show popup when clicked on `open` element









                $(document).on('click', openelement, function(event) {









                    event.preventDefault();









                    var ord = $(this).data('popup-ordinal');









                    setTimeout(function() { // setTimeout is to allow `close` method to finish (for issues with multiple tooltips)









                        methods.show(el, ord);









                    }, 0);









                });









            }









            if (options.closebutton) {









                methods.addclosebutton(el);









            }









            if (options.detach) {









                $el.hide().detach();









            } else {









                $wrapper.hide();









            }









        },









        /**















         * Show method















         *















         * @param {object} el - popup instance DOM node















         * @param {number} ordinal - order number of an `open` element















         */









        show: function(el, ordinal) {









            var $el = $(el);









            if ($el.data('popup-visible')) return;









            // Initialize if not initialized. Required for: $('#popup').popup('show')









            if (!$el.data('popup-initialized')) {









                methods._init(el);









            }









            $el.attr('data-popup-initialized', 'true');









            var $body = $('body');









            var options = $el.data('popupoptions');









            var $wrapper = $('#' + el.id + '_wrapper');









            var $background = $('#' + el.id + '_background');









            // `beforeopen` callback event









            callback(el, ordinal, options.beforeopen);









            // Remember last clicked place









            lastclicked[el.id] = ordinal;









            // Add popup id to visiblePopupsArray









            setTimeout(function() {









                visiblePopupsArray.push(el.id);









            }, 0);









            // Calculating maximum z-index









            if (options.autozindex) {









                var elements = document.getElementsByTagName('*');









                var len = elements.length;









                var maxzindex = 0;









                for (var i = 0; i < len; i++) {









                    var elementzindex = $(elements[i]).css('z-index');









                    if (elementzindex !== 'auto') {









                        elementzindex = parseInt(elementzindex, 10);









                        if (maxzindex < elementzindex) {









                            maxzindex = elementzindex;









                        }









                    }









                }









                zindexvalues[el.id] = maxzindex;









                // Add z-index to the background









                if (options.background) {









                    if (zindexvalues[el.id] > 0) {









                        $('#' + el.id + '_background').css({









                            zIndex: (zindexvalues[el.id] + 1)









                        });









                    }









                }









                // Add z-index to the wrapper









                if (zindexvalues[el.id] > 0) {









                    $wrapper.css({









                        zIndex: (zindexvalues[el.id] + 2)









                    });









                }









            }









            if (options.detach) {









                $wrapper.prepend(el);









                $el.show();









            } else {









                $wrapper.show();









            }









            opentimer = setTimeout(function() {









                $wrapper.css({









                    visibility: 'visible',

                    display: 'inline-block',









                    opacity: 1









                });









                $('html').addClass('popup_visible').addClass('popup_visible_' + el.id);









                $wrapper.addClass('popup_wrapper_visible');









            }, 20); // 20ms required for opening animation to occur in FF









            // Disable background layer scrolling when popup is opened









            if (options.scrolllock) {









                $body.css('overflow', 'hidden');









                if ($body.height() > $window.height()) {









                    $body.css('margin-right', bodymarginright + scrollbarwidth);









                }









            }









            if (options.backgroundactive) {









                //calculates the vertical align









                $el.css({









                    top: (









                        $window.height() - (









                            $el.get(0).offsetHeight +









                            parseInt($el.css('margin-top'), 10) +









                            parseInt($el.css('margin-bottom'), 10)









                        )









                    ) / 2 + 'px'









                });









            }









            $el.css({









                'visibility': 'visible',

                display: 'inline-block',









                'opacity': 1









            });









            // Show background









            if (options.background) {









                $background.css({









                    'visibility': 'visible',









                    'opacity': options.opacity









                });









                // Fix IE8 issue with background not appearing









                setTimeout(function() {









                    $background.css({









                        'opacity': options.opacity









                    });









                }, 0);









            }









            $el.data('popup-visible', true);









            // Position popup









            methods.reposition(el, ordinal);









            // Remember which element had focus before opening a popup









            $el.data('focusedelementbeforepopup', document.activeElement);









            // Handler: Keep focus inside dialog box









            if (options.keepfocus) {









                // Make holder div focusable









                $el.attr('tabindex', -1);









                // Focus popup or user specified element.









                // Initial timeout of 50ms is set to give some time to popup to show after clicking on









                // `open` element, and after animation is complete to prevent background scrolling.









                setTimeout(function() {









                    if (options.focuselement === 'closebutton') {









                        $('#' + el.id + ' .' + el.id + closesuffix + ':first').focus();









                    } else if (options.focuselement) {









                        $(options.focuselement).focus();









                    } else {









                        $el.focus();









                    }









                }, options.focusdelay);









            }









            // Hide main content from screen readers









            $(options.pagecontainer).attr('aria-hidden', true);









            // Reveal popup content to screen readers









            $el.attr('aria-hidden', false);









            callback(el, ordinal, options.onopen);









            if (transitionsupport) {









                $wrapper.one('transitionend', function() {









                    callback(el, ordinal, options.opentransitionend);









                });









            } else {









                callback(el, ordinal, options.opentransitionend);









            }









            // Handler: Reposition tooltip when window is resized









            if (options.type == 'tooltip') {









                $(window).on('resize.' + el.id, function() {









                    methods.reposition(el, ordinal);









                });









            }









        },









        /**















         * Hide method















         *















         * @param object el - popup instance DOM node















         * @param boolean outerClick - click on the outer content below popup















         */









        hide: function(el, outerClick) {









            // Get index of popup ID inside of visiblePopupsArray









            var popupIdIndex = $.inArray(el.id, visiblePopupsArray);









            // If popup is not opened, ignore the rest of the function









            if (popupIdIndex === -1) {









                return;









            }









            if (opentimer) clearTimeout(opentimer);









            var $body = $('body');









            var $el = $(el);









            var options = $el.data('popupoptions');









            var $wrapper = $('#' + el.id + '_wrapper');









            var $background = $('#' + el.id + '_background');











            $el.data('popup-visible', false);









            if (visiblePopupsArray.length === 1) {









                $('html').removeClass('popup_visible').removeClass('popup_visible_' + el.id);









            } else {









                if ($('html').hasClass('popup_visible_' + el.id)) {









                    $('html').removeClass('popup_visible_' + el.id);









                }









            }









            // Remove popup from the visiblePopupsArray









            visiblePopupsArray.splice(popupIdIndex, 1);









            if ($wrapper.hasClass('popup_wrapper_visible')) {









                $wrapper.removeClass('popup_wrapper_visible');









            }









            // Focus back on saved element









            if (options.keepfocus && !outerClick) {









                setTimeout(function() {









                    if ($($el.data('focusedelementbeforepopup')).is(':visible')) {









                        $el.data('focusedelementbeforepopup').focus();









                    }









                }, 0);









            }









            // Hide popup









            $wrapper.css({









                'visibility': 'hidden',

                display: 'none',









                'opacity': 0









            });









            $el.css({









                'visibility': 'hidden',

                display: 'none',









                'opacity': 0









            });









            // Hide background









            if (options.background) {









                $background.css({









                    'visibility': 'hidden',

                    display: 'none',









                    'opacity': 0









                });









            }









            // Reveal main content to screen readers









            $(options.pagecontainer).attr('aria-hidden', false);









            // Hide popup content from screen readers









            $el.attr('aria-hidden', true);









            // `onclose` callback event









            callback(el, lastclicked[el.id], options.onclose);









            if (transitionsupport && $el.css('transition-duration') !== '0s') {









                $el.one('transitionend', function(e) {









                    if (!($el.data('popup-visible'))) {









                        if (options.detach) {









                            $el.hide().detach();









                        } else {









                            $wrapper.hide();









                        }









                    }









                    // Re-enable scrolling of background layer









                    if (options.scrolllock) {









                        setTimeout(function() {









                            $body.css({









                                overflow: 'visible',









                                'margin-right': bodymarginright









                            });









                        }, 10); // 10ms added for CSS transition in Firefox which doesn't like overflow:auto









                    }









                    callback(el, lastclicked[el.id], options.closetransitionend);









                });









            } else {









                if (options.detach) {









                    $el.hide().detach();









                } else {









                    $wrapper.hide();









                }









                // Re-enable scrolling of background layer









                if (options.scrolllock) {









                    setTimeout(function() {









                        $body.css({









                            overflow: 'visible',









                            'margin-right': bodymarginright









                        });









                    }, 10); // 10ms added for CSS transition in Firefox which doesn't like overflow:auto









                }









                callback(el, lastclicked[el.id], options.closetransitionend);









            }









            if (options.type == 'tooltip') {









                $(window).off('resize.' + el.id);









            }









        },









        /**















         * Toggle method















         *















         * @param {object} el - popup instance DOM node















         * @param {number} ordinal - order number of an `open` element















         */









        toggle: function(el, ordinal) {









            if ($(el).data('popup-visible')) {









                methods.hide(el);









            } else {









                setTimeout(function() {









                    methods.show(el, ordinal);









                }, 0);









            }









        },









        /**















         * Reposition method















         *















         * @param {object} el - popup instance DOM node















         * @param {number} ordinal - order number of an `open` element















         */









        reposition: function(el, ordinal) {









            var $el = $(el);









            var options = $el.data('popupoptions');









            var $wrapper = $('#' + el.id + '_wrapper');









            var $background = $('#' + el.id + '_background');









            ordinal = ordinal || 0;









            // Tooltip type









            if (options.type == 'tooltip') {









                $wrapper.css({









                    'position': 'absolute'









                });









                var $tooltipanchor;









                if (options.tooltipanchor) {









                    $tooltipanchor = $(options.tooltipanchor);









                } else if (options.openelement) {









                    $tooltipanchor = $(options.openelement).filter('[data-popup-ordinal="' + ordinal + '"]');









                } else {









                    $tooltipanchor = $('.' + el.id + opensuffix + '[data-popup-ordinal="' + ordinal + '"]');









                }









                var linkOffset = $tooltipanchor.offset();









                // Horizontal position for tooltip









                if (options.horizontal == 'right') {









                    $wrapper.css('left', linkOffset.left + $tooltipanchor.outerWidth() + options.offsetleft);









                } else if (options.horizontal == 'leftedge') {









                    $wrapper.css('left', linkOffset.left + $tooltipanchor.outerWidth() - $tooltipanchor.outerWidth() + options.offsetleft);









                } else if (options.horizontal == 'left') {









                    $wrapper.css('right', $window.width() - linkOffset.left - options.offsetleft);









                } else if (options.horizontal == 'rightedge') {









                    $wrapper.css('right', $window.width() - linkOffset.left - $tooltipanchor.outerWidth() - options.offsetleft);









                } else {









                    $wrapper.css('left', linkOffset.left + ($tooltipanchor.outerWidth() / 2) - ($el.outerWidth() / 2) - parseFloat($el.css('marginLeft')) + options.offsetleft);









                }









                // Vertical position for tooltip









                if (options.vertical == 'bottom') {









                    $wrapper.css('top', linkOffset.top + $tooltipanchor.outerHeight() + options.offsettop);









                } else if (options.vertical == 'bottomedge') {









                    $wrapper.css('top', linkOffset.top + $tooltipanchor.outerHeight() - $el.outerHeight() + options.offsettop);









                } else if (options.vertical == 'top') {









                    $wrapper.css('bottom', $window.height() - linkOffset.top - options.offsettop);









                } else if (options.vertical == 'topedge') {









                    $wrapper.css('bottom', $window.height() - linkOffset.top - $el.outerHeight() - options.offsettop);









                } else {









                    $wrapper.css('top', linkOffset.top + ($tooltipanchor.outerHeight() / 2) - ($el.outerHeight() / 2) - parseFloat($el.css('marginTop')) + options.offsettop);









                }









                // Overlay type









            } else if (options.type == 'overlay') {









                // Horizontal position for overlay









                if (options.horizontal) {









                    $wrapper.css('text-align', options.horizontal);









                } else {









                    $wrapper.css('text-align', 'center');









                }









                // Vertical position for overlay









                if (options.vertical) {









                    $el.css('vertical-align', options.vertical);









                } else {









                    $el.css('vertical-align', 'middle');









                }









            }









        },









        /**















         * Add-close-button method















         *















         * @param {object} el - popup instance DOM node















         */









        addclosebutton: function(el) {









            var genericCloseButton;









            if ($(el).data('popupoptions').closebuttonmarkup) {









                genericCloseButton = $(options.closebuttonmarkup).addClass(el.id + '_close');









            } else {









                genericCloseButton = '<button class="popup_close ' + el.id + '_close" title="Close" aria-label="Close"><span aria-hidden="true">×</span></button>';









            }









            if ($(el).data('popup-initialized')) {









                $(el).append(genericCloseButton);









            }









        }









    };









    /**















     * Callback event calls















     *















     * @param {object} el - popup instance DOM node















     * @param {number} ordinal - order number of an `open` element















     * @param {function} func - callback function















     */









    var callback = function(el, ordinal, func) {









        var options = $(el).data('popupoptions');









        var openelement = (options.openelement) ? options.openelement : ('.' + el.id + opensuffix);









        var elementclicked = $(openelement + '[data-popup-ordinal="' + ordinal + '"]');









        if (typeof func == 'function') {









            func.call($(el), el, elementclicked);









        }









    };









    // Hide popup if ESC key is pressed









    $(document).on('keydown', function(event) {









        if (visiblePopupsArray.length) {









            var elementId = visiblePopupsArray[visiblePopupsArray.length - 1];









            var el = document.getElementById(elementId);









            if ($(el).data('popupoptions').escape && event.keyCode == 27) {









                methods.hide(el);









            }









        }









    });









    // Hide popup on click









    $(document).on('click', function(event) {









        if (visiblePopupsArray.length) {









            var elementId = visiblePopupsArray[visiblePopupsArray.length - 1];









            var el = document.getElementById(elementId);









            var closeButton = ($(el).data('popupoptions').closeelement) ? $(el).data('popupoptions').closeelement : ('.' + el.id + closesuffix);









            // If Close button clicked









            if ($(event.target).closest(closeButton).length) {









                event.preventDefault();









                methods.hide(el);









            }









            // If clicked outside of popup









            if ($(el).data('popupoptions').blur && !$(event.target).closest('#' + elementId).length && event.which !== 2 && $(event.target).is(':visible')) {









                if ($(el).data('popupoptions').background) {









                    // If clicked on popup cover









                    methods.hide(el);









                    // Older iOS/Safari will trigger a click on the elements below the cover,









                    // when tapping on the cover, so the default action needs to be prevented.









                    event.preventDefault();









                } else {









                    // If clicked on outer content









                    methods.hide(el, true);









                }









            }









        }









    });









    // Keep keyboard focus inside of popup









    $(document).on('keydown', function(event) {









        if (visiblePopupsArray.length && event.which == 9) {









            // If tab or shift-tab pressed









            var elementId = visiblePopupsArray[visiblePopupsArray.length - 1];









            var el = document.getElementById(elementId);









            // Get list of all children elements in given object









            var popupItems = $(el).find('*');









            // Get list of focusable items









            var focusableItems = popupItems.filter(focusableElementsString).filter(':visible');









            // Get currently focused item









            var focusedItem = $(':focus');









            // Get the number of focusable items









            var numberOfFocusableItems = focusableItems.length;









            // Get the index of the currently focused item









            var focusedItemIndex = focusableItems.index(focusedItem);









            // If popup doesn't contain focusable elements, focus popup itself









            if (numberOfFocusableItems === 0) {









                $(el).focus();









                event.preventDefault();









            } else {









                if (event.shiftKey) {









                    // Back tab









                    // If focused on first item and user preses back-tab, go to the last focusable item









                    if (focusedItemIndex === 0) {









                        focusableItems.get(numberOfFocusableItems - 1).focus();









                        event.preventDefault();









                    }









                } else {









                    // Forward tab









                    // If focused on the last item and user preses tab, go to the first focusable item









                    if (focusedItemIndex == numberOfFocusableItems - 1) {









                        focusableItems.get(0).focus();









                        event.preventDefault();









                    }









                }









            }









        }









    });









    /**















     * Plugin API















     */









    $.fn.popup = function(customoptions) {









        return this.each(function() {









            var $el = $(this);









            if (typeof customoptions === 'object') { // e.g. $('#popup').popup({'color':'blue'})









                var opt = $.extend({}, $.fn.popup.defaults, $el.data('popupoptions'), customoptions);









                $el.data('popupoptions', opt);









                options = $el.data('popupoptions');









                methods._init(this);









            } else if (typeof customoptions === 'string') { // e.g. $('#popup').popup('hide')









                if (!($el.data('popupoptions'))) {









                    $el.data('popupoptions', $.fn.popup.defaults);









                    options = $el.data('popupoptions');









                }









                methods[customoptions].call(this, this);











            } else { // e.g. $('#popup').popup()









                if (!($el.data('popupoptions'))) {









                    $el.data('popupoptions', $.fn.popup.defaults);









                    options = $el.data('popupoptions');









                }









                methods._init(this);









            }









        });









    };









    $.fn.popup.defaults = {









        type: 'overlay',









        autoopen: false,









        background: true,









        backgroundactive: false,









        color: 'black',









        opacity: '0.5',









        horizontal: 'center',









        vertical: 'middle',









        offsettop: 0,









        offsetleft: 0,









        escape: true,









        blur: true,









        setzindex: true,









        autozindex: false,









        scrolllock: false,









        closebutton: false,









        closebuttonmarkup: null,









        keepfocus: true,









        focuselement: null,









        focusdelay: 50,









        outline: false,









        pagecontainer: null,









        detach: false,









        openelement: null,









        closeelement: null,









        transition: null,









        tooltipanchor: null,









        beforeopen: null,









        onclose: null,









        onopen: null,









        opentransitionend: null,









        closetransitionend: null









    };









})(jQuery);









$(document).ready(function() {









    // Initialize the plugin









    $('.myPopupDv').popup();









});



/*Easy Responsive Tab Starts*/

// Easy Responsive Tabs Plugin

// Author: Samson.Onna <Email : samson3d@gmail.com> 

(function ($) {

    $.fn.extend({

        easyResponsiveTabs: function (options) {

            //Set the default values, use comma to separate the settings, example:

            var defaults = {

                type: 'default', //default, vertical, accordion;

                width: 'auto',

                fit: true,

                closed: false,

                tabidentify: '',

                activetab_bg: 'white',

                inactive_bg: '#F5F5F5',

                active_border_color: '#c1c1c1',

                active_content_border_color: '#c1c1c1',

                activate: function () {

                }

            }

            //Variables

            var options = $.extend(defaults, options);

            var opt = options, jtype = opt.type, jfit = opt.fit, jwidth = opt.width, vtabs = 'vertical', accord = 'accordion';

            var hash = window.location.hash;

            var historyApi = !!(window.history && history.replaceState);



            //Events

            $(this).bind('tabactivate', function (e, currentTab) {

                if (typeof options.activate === 'function') {

                    options.activate.call(currentTab, e)

                }

            });



            //Main function

            this.each(function () {

                var $respTabs = $(this);

                var $respTabsList = $respTabs.find('ul.resp-tabs-list.' + options.tabidentify);

                var respTabsId = $respTabs.attr('id');

                $respTabs.find('ul.resp-tabs-list.' + options.tabidentify + ' li').addClass('resp-tab-item').addClass(options.tabidentify);

                $respTabs.css({

                    'display': 'block',

                    'width': jwidth

                });



                if (options.type == 'vertical')

                    $respTabsList.css('margin-top', '3px');



                $respTabs.find('.resp-tabs-container.' + options.tabidentify).css('border-color', options.active_content_border_color);

                $respTabs.find('.resp-tabs-container.' + options.tabidentify + ' > div').addClass('resp-tab-content').addClass(options.tabidentify);

                jtab_options();

                //Properties Function

                function jtab_options() {

                    if (jtype == vtabs) {

                        $respTabs.addClass('resp-vtabs').addClass(options.tabidentify);

                    }

                    if (jfit == true) {

                        $respTabs.css({ width: '100%', margin: '0px' });

                    }

                    if (jtype == accord) {

                        $respTabs.addClass('resp-easy-accordion').addClass(options.tabidentify);

                        $respTabs.find('.resp-tabs-list').css('display', 'none');

                    }

                }



                //Assigning the h2 markup to accordion title

                var $tabItemh2;

                $respTabs.find('.resp-tab-content.' + options.tabidentify).before("<h2 class='resp-accordion " + options.tabidentify + "' role='tab'><span class='resp-arrow'></span></h2>");



                $respTabs.find('.resp-tab-content.' + options.tabidentify).prev("h2").css({

                    'background-color': options.inactive_bg,

                    'border-color': options.active_border_color

                });



                var itemCount = 0;

                $respTabs.find('.resp-accordion').each(function () {

                    $tabItemh2 = $(this);

                    var $tabItem = $respTabs.find('.resp-tab-item:eq(' + itemCount + ')');

                    var $accItem = $respTabs.find('.resp-accordion:eq(' + itemCount + ')');

                    $accItem.append($tabItem.html());

                    $accItem.data($tabItem.data());

                    $tabItemh2.attr('aria-controls', options.tabidentify + '_tab_item-' + (itemCount));

                    itemCount++;

                });



                //Assigning the 'aria-controls' to Tab items

                var count = 0,

                    $tabContent;

                $respTabs.find('.resp-tab-item').each(function () {

                    $tabItem = $(this);

                    $tabItem.attr('aria-controls', options.tabidentify + '_tab_item-' + (count));

                    $tabItem.attr('role', 'tab');

                    $tabItem.css({

                        'background-color': options.inactive_bg,

                        'border-color': 'none'

                    });



                    //Assigning the 'aria-labelledby' attr to tab-content

                    var tabcount = 0;

                    $respTabs.find('.resp-tab-content.' + options.tabidentify).each(function () {

                        $tabContent = $(this);

                        $tabContent.attr('aria-labelledby', options.tabidentify + '_tab_item-' + (tabcount)).css({

                            'border-color': options.active_border_color

                        });

                        tabcount++;

                    });

                    count++;

                });



                // Show correct content area

                var tabNum = 0;

                if (hash != '') {

                    var matches = hash.match(new RegExp(respTabsId + "([0-9]+)"));

                    if (matches !== null && matches.length === 2) {

                        tabNum = parseInt(matches[1], 10) - 1;

                        if (tabNum > count) {

                            tabNum = 0;

                        }

                    }

                }



                //Active correct tab

                $($respTabs.find('.resp-tab-item.' + options.tabidentify)[tabNum]).addClass('resp-tab-active').css({

                    'background-color': options.activetab_bg,

                    'border-color': options.active_border_color

                });



                //keep closed if option = 'closed' or option is 'accordion' and the element is in accordion mode

                if (options.closed !== true && !(options.closed === 'accordion' && !$respTabsList.is(':visible')) && !(options.closed === 'tabs' && $respTabsList.is(':visible'))) {

                    $($respTabs.find('.resp-accordion.' + options.tabidentify)[tabNum]).addClass('resp-tab-active').css({

                        'background-color': options.activetab_bg + ' !important',

                        'border-color': options.active_border_color,

                        'background': 'none'

                    });



                    $($respTabs.find('.resp-tab-content.' + options.tabidentify)[tabNum]).addClass('resp-tab-content-active').addClass(options.tabidentify).attr('style', 'display:block');

                }

                //assign proper classes for when tabs mode is activated before making a selection in accordion mode

                else {

                   // $($respTabs.find('.resp-tab-content.' + options.tabidentify)[tabNum]).addClass('resp-accordion-closed'); //removed resp-tab-content-active

                }



                //Tab Click action function

                $respTabs.find("[role=tab]").each(function () {



                    var $currentTab = $(this);

                    $currentTab.click(function () {



                        var $currentTab = $(this);

                        var $tabAria = $currentTab.attr('aria-controls');



                        if ($currentTab.hasClass('resp-accordion') && $currentTab.hasClass('resp-tab-active')) {

                            $respTabs.find('.resp-tab-content-active.' + options.tabidentify).slideUp('', function () {

                                $(this).addClass('resp-accordion-closed');

                            });

                            $currentTab.removeClass('resp-tab-active').css({

                                'background-color': options.inactive_bg,

                                'border-color': 'none'

                            });

                            return false;

                        }

                        if (!$currentTab.hasClass('resp-tab-active') && $currentTab.hasClass('resp-accordion')) {

                            $respTabs.find('.resp-tab-active.' + options.tabidentify).removeClass('resp-tab-active').css({

                                'background-color': options.inactive_bg,

                                'border-color': 'none'

                            });

                            $respTabs.find('.resp-tab-content-active.' + options.tabidentify).slideUp().removeClass('resp-tab-content-active resp-accordion-closed');

                            $respTabs.find("[aria-controls=" + $tabAria + "]").addClass('resp-tab-active').css({

                                'background-color': options.activetab_bg,

                                'border-color': options.active_border_color

                            });



                            $respTabs.find('.resp-tab-content[aria-labelledby = ' + $tabAria + '].' + options.tabidentify).slideDown().addClass('resp-tab-content-active');

                        } else {

                            console.log('here');

                            $respTabs.find('.resp-tab-active.' + options.tabidentify).removeClass('resp-tab-active').css({

                                'background-color': options.inactive_bg,

                                'border-color': 'none'

                            });



                            $respTabs.find('.resp-tab-content-active.' + options.tabidentify).removeAttr('style').removeClass('resp-tab-content-active').removeClass('resp-accordion-closed');



                            $respTabs.find("[aria-controls=" + $tabAria + "]").addClass('resp-tab-active').css({

                                'background-color': options.activetab_bg,

                                'border-color': options.active_border_color

                            });



                            $respTabs.find('.resp-tab-content[aria-labelledby = ' + $tabAria + '].' + options.tabidentify).addClass('resp-tab-content-active').attr('style', 'display:block');

                        }

                        //Trigger tab activation event

                        $currentTab.trigger('tabactivate', $currentTab);



                        //Update Browser History

                        if (historyApi) {

                            var currentHash = window.location.hash;

                            var tabAriaParts = $tabAria.split('tab_item-');

                            // var newHash = respTabsId + (parseInt($tabAria.substring(9), 10) + 1).toString();

                            var newHash = respTabsId + (parseInt(tabAriaParts[1], 10) + 1).toString();

                            if (currentHash != "") {

                                var re = new RegExp(respTabsId + "[0-9]+");

                                if (currentHash.match(re) != null) {

                                    newHash = currentHash.replace(re, newHash);

                                }

                                else {

                                    newHash = currentHash + "|" + newHash;

                                }

                            }

                            else {

                                newHash = '#' + newHash;

                            }



                            history.replaceState(null, null, newHash);

                        }

                    });



                });



                //Window resize function                   

                $(window).resize(function () {

                    $respTabs.find('.resp-accordion-closed').removeAttr('style');

                });

            });

        }

    });

})(jQuery);



$(document).ready(function() {



    //Vertical Tab

    $('#parentVerticalTab').easyResponsiveTabs({

        type: 'vertical', //Types: default, vertical, accordion

        width: 'auto', //auto or any width like 600px

        fit: true, // 100% fit in a container

        closed: 'accordion', // Start closed if in accordion view

        tabidentify: 'hor_1', // The tab groups identifier

        activate: function(event) { // Callback function if tab is switched

            var $tab = $(this);

            var $info = $('#nested-tabInfo2');

            var $name = $('span', $info);

            $name.text($tab.text());

            $info.show();

        }

    });

});



/*Easy Responsive Tab Ends*/





/*Light Gallery Starts*/





!function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof module&&module.exports?module.exports=b(require("jquery")):b(a.jQuery)}(this,function(a){!function(){"use strict";function b(b,d){if(this.el=b,this.$el=a(b),this.s=a.extend({},c,d),this.s.dynamic&&"undefined"!==this.s.dynamicEl&&this.s.dynamicEl.constructor===Array&&!this.s.dynamicEl.length)throw"When using dynamic mode, you must also define dynamicEl as an Array.";return this.modules={},this.lGalleryOn=!1,this.lgBusy=!1,this.hideBartimeout=!1,this.isTouch="ontouchstart"in document.documentElement,this.s.slideEndAnimatoin&&(this.s.hideControlOnEnd=!1),this.s.dynamic?this.$items=this.s.dynamicEl:"this"===this.s.selector?this.$items=this.$el:""!==this.s.selector?this.s.selectWithin?this.$items=a(this.s.selectWithin).find(this.s.selector):this.$items=this.$el.find(a(this.s.selector)):this.$items=this.$el.children(),this.$slide="",this.$outer="",this.init(),this}var c={mode:"lg-slide",cssEasing:"ease",easing:"linear",speed:600,height:"100%",width:"100%",addClass:"",startClass:"lg-start-zoom",backdropDuration:150,hideBarsDelay:6e3,useLeft:!1,closable:!0,loop:!0,escKey:!0,keyPress:!0,controls:!0,slideEndAnimatoin:!0,hideControlOnEnd:!1,mousewheel:!0,getCaptionFromTitleOrAlt:!0,appendSubHtmlTo:".lg-sub-html",subHtmlSelectorRelative:!1,preload:1,showAfterLoad:!0,selector:"",selectWithin:"",nextHtml:"",prevHtml:"",index:!1,iframeMaxWidth:"100%",download:!0,counter:!0,appendCounterTo:".lg-toolbar",swipeThreshold:50,enableSwipe:!0,enableDrag:!0,dynamic:!1,dynamicEl:[],galleryId:1};b.prototype.init=function(){var b=this;b.s.preload>b.$items.length&&(b.s.preload=b.$items.length);var c=window.location.hash;c.indexOf("lg="+this.s.galleryId)>0&&(b.index=parseInt(c.split("&slide=")[1],10),a("body").addClass("lg-from-hash"),a("body").hasClass("lg-on")||(setTimeout(function(){b.build(b.index)}),a("body").addClass("lg-on"))),b.s.dynamic?(b.$el.trigger("onBeforeOpen.lg"),b.index=b.s.index||0,a("body").hasClass("lg-on")||setTimeout(function(){b.build(b.index),a("body").addClass("lg-on")})):b.$items.on("click.lgcustom",function(c){try{c.preventDefault(),c.preventDefault()}catch(a){c.returnValue=!1}b.$el.trigger("onBeforeOpen.lg"),b.index=b.s.index||b.$items.index(this),a("body").hasClass("lg-on")||(b.build(b.index),a("body").addClass("lg-on"))})},b.prototype.build=function(b){var c=this;c.structure(),a.each(a.fn.lightGallery.modules,function(b){c.modules[b]=new a.fn.lightGallery.modules[b](c.el)}),c.slide(b,!1,!1,!1),c.s.keyPress&&c.keyPress(),c.$items.length>1?(c.arrow(),setTimeout(function(){c.enableDrag(),c.enableSwipe()},50),c.s.mousewheel&&c.mousewheel()):c.$slide.on("click.lg",function(){c.$el.trigger("onSlideClick.lg")}),c.counter(),c.closeGallery(),c.$el.trigger("onAfterOpen.lg"),c.$outer.on("mousemove.lg click.lg touchstart.lg",function(){c.$outer.removeClass("lg-hide-items"),clearTimeout(c.hideBartimeout),c.hideBartimeout=setTimeout(function(){c.$outer.addClass("lg-hide-items")},c.s.hideBarsDelay)}),c.$outer.trigger("mousemove.lg")},b.prototype.structure=function(){var b,c="",d="",e=0,f="",g=this;for(a("body").append('<div class="lg-backdrop"></div>'),a(".lg-backdrop").css("transition-duration",this.s.backdropDuration+"ms"),e=0;e<this.$items.length;e++)c+='<div class="lg-item"></div>';if(this.s.controls&&this.$items.length>1&&(d='<div class="lg-actions"><button class="lg-prev lg-icon">'+this.s.prevHtml+'</button><button class="lg-next lg-icon">'+this.s.nextHtml+"</button></div>"),".lg-sub-html"===this.s.appendSubHtmlTo&&(f='<div class="lg-sub-html"></div>'),b='<div class="lg-outer '+this.s.addClass+" "+this.s.startClass+'"><div class="lg" style="width:'+this.s.width+"; height:"+this.s.height+'"><div class="lg-inner">'+c+'</div><div class="lg-toolbar lg-group"><span class="lg-close lg-icon"></span></div>'+d+f+"</div></div>",a("body").append(b),this.$outer=a(".lg-outer"),this.$slide=this.$outer.find(".lg-item"),this.s.useLeft?(this.$outer.addClass("lg-use-left"),this.s.mode="lg-slide"):this.$outer.addClass("lg-use-css3"),g.setTop(),a(window).on("resize.lg orientationchange.lg",function(){setTimeout(function(){g.setTop()},100)}),this.$slide.eq(this.index).addClass("lg-current"),this.doCss()?this.$outer.addClass("lg-css3"):(this.$outer.addClass("lg-css"),this.s.speed=0),this.$outer.addClass(this.s.mode),this.s.enableDrag&&this.$items.length>1&&this.$outer.addClass("lg-grab"),this.s.showAfterLoad&&this.$outer.addClass("lg-show-after-load"),this.doCss()){var h=this.$outer.find(".lg-inner");h.css("transition-timing-function",this.s.cssEasing),h.css("transition-duration",this.s.speed+"ms")}setTimeout(function(){a(".lg-backdrop").addClass("in")}),setTimeout(function(){g.$outer.addClass("lg-visible")},this.s.backdropDuration),this.s.download&&this.$outer.find(".lg-toolbar").append('<a id="lg-download" target="_blank" download class="lg-download lg-icon"></a>'),this.prevScrollTop=a(window).scrollTop()},b.prototype.setTop=function(){if("100%"!==this.s.height){var b=a(window).height(),c=(b-parseInt(this.s.height,10))/2,d=this.$outer.find(".lg");b>=parseInt(this.s.height,10)?d.css("top",c+"px"):d.css("top","0px")}},b.prototype.doCss=function(){return!!function(){var a=["transition","MozTransition","WebkitTransition","OTransition","msTransition","KhtmlTransition"],b=document.documentElement,c=0;for(c=0;c<a.length;c++)if(a[c]in b.style)return!0}()},b.prototype.isVideo=function(a,b){var c;if(c=this.s.dynamic?this.s.dynamicEl[b].html:this.$items.eq(b).attr("data-html"),!a)return c?{html5:!0}:(console.error("lightGallery :- data-src is not pvovided on slide item "+(b+1)+". Please make sure the selector property is properly configured. More info - http://sachinchoolur.github.io/lightGallery/demos/html-markup.html"),!1);var d=a.match(/\/\/(?:www\.)?youtu(?:\.be|be\.com|be-nocookie\.com)\/(?:watch\?v=|embed\/)?([a-z0-9\-\_\%]+)/i),e=a.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i),f=a.match(/\/\/(?:www\.)?dai.ly\/([0-9a-z\-_]+)/i),g=a.match(/\/\/(?:www\.)?(?:vk\.com|vkontakte\.ru)\/(?:video_ext\.php\?)(.*)/i);return d?{youtube:d}:e?{vimeo:e}:f?{dailymotion:f}:g?{vk:g}:void 0},b.prototype.counter=function(){this.s.counter&&a(this.s.appendCounterTo).append('<div id="lg-counter"><span id="lg-counter-current">'+(parseInt(this.index,10)+1)+'</span> / <span id="lg-counter-all">'+this.$items.length+"</span></div>")},b.prototype.addHtml=function(b){var c,d,e=null;if(this.s.dynamic?this.s.dynamicEl[b].subHtmlUrl?c=this.s.dynamicEl[b].subHtmlUrl:e=this.s.dynamicEl[b].subHtml:(d=this.$items.eq(b),d.attr("data-sub-html-url")?c=d.attr("data-sub-html-url"):(e=d.attr("data-sub-html"),this.s.getCaptionFromTitleOrAlt&&!e&&(e=d.attr("title")||d.find("img").first().attr("alt")))),!c)if(void 0!==e&&null!==e){var f=e.substring(0,1);"."!==f&&"#"!==f||(e=this.s.subHtmlSelectorRelative&&!this.s.dynamic?d.find(e).html():a(e).html())}else e="";".lg-sub-html"===this.s.appendSubHtmlTo?c?this.$outer.find(this.s.appendSubHtmlTo).load(c):this.$outer.find(this.s.appendSubHtmlTo).html(e):c?this.$slide.eq(b).load(c):this.$slide.eq(b).append(e),void 0!==e&&null!==e&&(""===e?this.$outer.find(this.s.appendSubHtmlTo).addClass("lg-empty-html"):this.$outer.find(this.s.appendSubHtmlTo).removeClass("lg-empty-html")),this.$el.trigger("onAfterAppendSubHtml.lg",[b])},b.prototype.preload=function(a){var b=1,c=1;for(b=1;b<=this.s.preload&&!(b>=this.$items.length-a);b++)this.loadContent(a+b,!1,0);for(c=1;c<=this.s.preload&&!(a-c<0);c++)this.loadContent(a-c,!1,0)},b.prototype.loadContent=function(b,c,d){var e,f,g,h,i,j,k=this,l=!1,m=function(b){for(var c=[],d=[],e=0;e<b.length;e++){var g=b[e].split(" ");""===g[0]&&g.splice(0,1),d.push(g[0]),c.push(g[1])}for(var h=a(window).width(),i=0;i<c.length;i++)if(parseInt(c[i],10)>h){f=d[i];break}};if(k.s.dynamic){if(k.s.dynamicEl[b].poster&&(l=!0,g=k.s.dynamicEl[b].poster),j=k.s.dynamicEl[b].html,f=k.s.dynamicEl[b].src,k.s.dynamicEl[b].responsive){m(k.s.dynamicEl[b].responsive.split(","))}h=k.s.dynamicEl[b].srcset,i=k.s.dynamicEl[b].sizes}else{if(k.$items.eq(b).attr("data-poster")&&(l=!0,g=k.$items.eq(b).attr("data-poster")),j=k.$items.eq(b).attr("data-html"),f=k.$items.eq(b).attr("href")||k.$items.eq(b).attr("data-src"),k.$items.eq(b).attr("data-responsive")){m(k.$items.eq(b).attr("data-responsive").split(","))}h=k.$items.eq(b).attr("data-srcset"),i=k.$items.eq(b).attr("data-sizes")}var n=!1;k.s.dynamic?k.s.dynamicEl[b].iframe&&(n=!0):"true"===k.$items.eq(b).attr("data-iframe")&&(n=!0);var o=k.isVideo(f,b);if(!k.$slide.eq(b).hasClass("lg-loaded")){if(n)k.$slide.eq(b).prepend('<div class="lg-video-cont lg-has-iframe" style="max-width:'+k.s.iframeMaxWidth+'"><div class="lg-video"><iframe class="lg-object" frameborder="0" src="'+f+'"  allowfullscreen="true"></iframe></div></div>');else if(l){var p="";p=o&&o.youtube?"lg-has-youtube":o&&o.vimeo?"lg-has-vimeo":"lg-has-html5",k.$slide.eq(b).prepend('<div class="lg-video-cont '+p+' "><div class="lg-video"><span class="lg-video-play"></span><img class="lg-object lg-has-poster" src="'+g+'" /></div></div>')}else o?(k.$slide.eq(b).prepend('<div class="lg-video-cont "><div class="lg-video"></div></div>'),k.$el.trigger("hasVideo.lg",[b,f,j])):k.$slide.eq(b).prepend('<div class="lg-img-wrap"><img class="lg-object lg-image" src="'+f+'" /></div>');if(k.$el.trigger("onAferAppendSlide.lg",[b]),e=k.$slide.eq(b).find(".lg-object"),i&&e.attr("sizes",i),h){e.attr("srcset",h);try{picturefill({elements:[e[0]]})}catch(a){console.warn("lightGallery :- If you want srcset to be supported for older browser please include picturefil version 2 javascript library in your document.")}}".lg-sub-html"!==this.s.appendSubHtmlTo&&k.addHtml(b),k.$slide.eq(b).addClass("lg-loaded")}k.$slide.eq(b).find(".lg-object").on("load.lg error.lg",function(){var c=0;d&&!a("body").hasClass("lg-from-hash")&&(c=d),setTimeout(function(){k.$slide.eq(b).addClass("lg-complete"),k.$el.trigger("onSlideItemLoad.lg",[b,d||0])},c)}),o&&o.html5&&!l&&k.$slide.eq(b).addClass("lg-complete"),!0===c&&(k.$slide.eq(b).hasClass("lg-complete")?k.preload(b):k.$slide.eq(b).find(".lg-object").on("load.lg error.lg",function(){k.preload(b)}))},b.prototype.slide=function(b,c,d,e){var f=this.$outer.find(".lg-current").index(),g=this;if(!g.lGalleryOn||f!==b){var h=this.$slide.length,i=g.lGalleryOn?this.s.speed:0;if(!g.lgBusy){if(this.s.download){var j;j=g.s.dynamic?!1!==g.s.dynamicEl[b].downloadUrl&&(g.s.dynamicEl[b].downloadUrl||g.s.dynamicEl[b].src):"false"!==g.$items.eq(b).attr("data-download-url")&&(g.$items.eq(b).attr("data-download-url")||g.$items.eq(b).attr("href")||g.$items.eq(b).attr("data-src")),j?(a("#lg-download").attr("href",j),g.$outer.removeClass("lg-hide-download")):g.$outer.addClass("lg-hide-download")}if(this.$el.trigger("onBeforeSlide.lg",[f,b,c,d]),g.lgBusy=!0,clearTimeout(g.hideBartimeout),".lg-sub-html"===this.s.appendSubHtmlTo&&setTimeout(function(){g.addHtml(b)},i),this.arrowDisable(b),e||(b<f?e="prev":b>f&&(e="next")),c){this.$slide.removeClass("lg-prev-slide lg-current lg-next-slide");var k,l;h>2?(k=b-1,l=b+1,0===b&&f===h-1?(l=0,k=h-1):b===h-1&&0===f&&(l=0,k=h-1)):(k=0,l=1),"prev"===e?g.$slide.eq(l).addClass("lg-next-slide"):g.$slide.eq(k).addClass("lg-prev-slide"),g.$slide.eq(b).addClass("lg-current")}else g.$outer.addClass("lg-no-trans"),this.$slide.removeClass("lg-prev-slide lg-next-slide"),"prev"===e?(this.$slide.eq(b).addClass("lg-prev-slide"),this.$slide.eq(f).addClass("lg-next-slide")):(this.$slide.eq(b).addClass("lg-next-slide"),this.$slide.eq(f).addClass("lg-prev-slide")),setTimeout(function(){g.$slide.removeClass("lg-current"),g.$slide.eq(b).addClass("lg-current"),g.$outer.removeClass("lg-no-trans")},50);g.lGalleryOn?(setTimeout(function(){g.loadContent(b,!0,0)},this.s.speed+50),setTimeout(function(){g.lgBusy=!1,g.$el.trigger("onAfterSlide.lg",[f,b,c,d])},this.s.speed)):(g.loadContent(b,!0,g.s.backdropDuration),g.lgBusy=!1,g.$el.trigger("onAfterSlide.lg",[f,b,c,d])),g.lGalleryOn=!0,this.s.counter&&a("#lg-counter-current").text(b+1)}g.index=b}},b.prototype.goToNextSlide=function(a){var b=this,c=b.s.loop;a&&b.$slide.length<3&&(c=!1),b.lgBusy||(b.index+1<b.$slide.length?(b.index++,b.$el.trigger("onBeforeNextSlide.lg",[b.index]),b.slide(b.index,a,!1,"next")):c?(b.index=0,b.$el.trigger("onBeforeNextSlide.lg",[b.index]),b.slide(b.index,a,!1,"next")):b.s.slideEndAnimatoin&&!a&&(b.$outer.addClass("lg-right-end"),setTimeout(function(){b.$outer.removeClass("lg-right-end")},400)))},b.prototype.goToPrevSlide=function(a){var b=this,c=b.s.loop;a&&b.$slide.length<3&&(c=!1),b.lgBusy||(b.index>0?(b.index--,b.$el.trigger("onBeforePrevSlide.lg",[b.index,a]),b.slide(b.index,a,!1,"prev")):c?(b.index=b.$items.length-1,b.$el.trigger("onBeforePrevSlide.lg",[b.index,a]),b.slide(b.index,a,!1,"prev")):b.s.slideEndAnimatoin&&!a&&(b.$outer.addClass("lg-left-end"),setTimeout(function(){b.$outer.removeClass("lg-left-end")},400)))},b.prototype.keyPress=function(){var b=this;this.$items.length>1&&a(window).on("keyup.lg",function(a){b.$items.length>1&&(37===a.keyCode&&(a.preventDefault(),b.goToPrevSlide()),39===a.keyCode&&(a.preventDefault(),b.goToNextSlide()))}),a(window).on("keydown.lg",function(a){!0===b.s.escKey&&27===a.keyCode&&(a.preventDefault(),b.$outer.hasClass("lg-thumb-open")?b.$outer.removeClass("lg-thumb-open"):b.destroy())})},b.prototype.arrow=function(){var a=this;this.$outer.find(".lg-prev").on("click.lg",function(){a.goToPrevSlide()}),this.$outer.find(".lg-next").on("click.lg",function(){a.goToNextSlide()})},b.prototype.arrowDisable=function(a){!this.s.loop&&this.s.hideControlOnEnd&&(a+1<this.$slide.length?this.$outer.find(".lg-next").removeAttr("disabled").removeClass("disabled"):this.$outer.find(".lg-next").attr("disabled","disabled").addClass("disabled"),a>0?this.$outer.find(".lg-prev").removeAttr("disabled").removeClass("disabled"):this.$outer.find(".lg-prev").attr("disabled","disabled").addClass("disabled"))},b.prototype.setTranslate=function(a,b,c){this.s.useLeft?a.css("left",b):a.css({transform:"translate3d("+b+"px, "+c+"px, 0px)"})},b.prototype.touchMove=function(b,c){var d=c-b;Math.abs(d)>15&&(this.$outer.addClass("lg-dragging"),this.setTranslate(this.$slide.eq(this.index),d,0),this.setTranslate(a(".lg-prev-slide"),-this.$slide.eq(this.index).width()+d,0),this.setTranslate(a(".lg-next-slide"),this.$slide.eq(this.index).width()+d,0))},b.prototype.touchEnd=function(a){var b=this;"lg-slide"!==b.s.mode&&b.$outer.addClass("lg-slide"),this.$slide.not(".lg-current, .lg-prev-slide, .lg-next-slide").css("opacity","0"),setTimeout(function(){b.$outer.removeClass("lg-dragging"),a<0&&Math.abs(a)>b.s.swipeThreshold?b.goToNextSlide(!0):a>0&&Math.abs(a)>b.s.swipeThreshold?b.goToPrevSlide(!0):Math.abs(a)<5&&b.$el.trigger("onSlideClick.lg"),b.$slide.removeAttr("style")}),setTimeout(function(){b.$outer.hasClass("lg-dragging")||"lg-slide"===b.s.mode||b.$outer.removeClass("lg-slide")},b.s.speed+100)},b.prototype.enableSwipe=function(){var a=this,b=0,c=0,d=!1;a.s.enableSwipe&&a.doCss()&&(a.$slide.on("touchstart.lg",function(c){a.$outer.hasClass("lg-zoomed")||a.lgBusy||(c.preventDefault(),a.manageSwipeClass(),b=c.originalEvent.targetTouches[0].pageX)}),a.$slide.on("touchmove.lg",function(e){a.$outer.hasClass("lg-zoomed")||(e.preventDefault(),c=e.originalEvent.targetTouches[0].pageX,a.touchMove(b,c),d=!0)}),a.$slide.on("touchend.lg",function(){a.$outer.hasClass("lg-zoomed")||(d?(d=!1,a.touchEnd(c-b)):a.$el.trigger("onSlideClick.lg"))}))},b.prototype.enableDrag=function(){var b=this,c=0,d=0,e=!1,f=!1;b.s.enableDrag&&b.doCss()&&(b.$slide.on("mousedown.lg",function(d){b.$outer.hasClass("lg-zoomed")||b.lgBusy||a(d.target).text().trim()||(d.preventDefault(),b.manageSwipeClass(),c=d.pageX,e=!0,b.$outer.scrollLeft+=1,b.$outer.scrollLeft-=1,b.$outer.removeClass("lg-grab").addClass("lg-grabbing"),b.$el.trigger("onDragstart.lg"))}),a(window).on("mousemove.lg",function(a){e&&(f=!0,d=a.pageX,b.touchMove(c,d),b.$el.trigger("onDragmove.lg"))}),a(window).on("mouseup.lg",function(g){f?(f=!1,b.touchEnd(d-c),b.$el.trigger("onDragend.lg")):(a(g.target).hasClass("lg-object")||a(g.target).hasClass("lg-video-play"))&&b.$el.trigger("onSlideClick.lg"),e&&(e=!1,b.$outer.removeClass("lg-grabbing").addClass("lg-grab"))}))},b.prototype.manageSwipeClass=function(){var a=this.index+1,b=this.index-1;this.s.loop&&this.$slide.length>2&&(0===this.index?b=this.$slide.length-1:this.index===this.$slide.length-1&&(a=0)),this.$slide.removeClass("lg-next-slide lg-prev-slide"),b>-1&&this.$slide.eq(b).addClass("lg-prev-slide"),this.$slide.eq(a).addClass("lg-next-slide")},b.prototype.mousewheel=function(){var a=this;a.$outer.on("mousewheel.lg",function(b){b.deltaY&&(b.deltaY>0?a.goToPrevSlide():a.goToNextSlide(),b.preventDefault())})},b.prototype.closeGallery=function(){var b=this,c=!1;this.$outer.find(".lg-close").on("click.lg",function(){b.destroy()}),b.s.closable&&(b.$outer.on("mousedown.lg",function(b){c=!!(a(b.target).is(".lg-outer")||a(b.target).is(".lg-item ")||a(b.target).is(".lg-img-wrap"))}),b.$outer.on("mousemove.lg",function(){c=!1}),b.$outer.on("mouseup.lg",function(d){(a(d.target).is(".lg-outer")||a(d.target).is(".lg-item ")||a(d.target).is(".lg-img-wrap")&&c)&&(b.$outer.hasClass("lg-dragging")||b.destroy())}))},b.prototype.destroy=function(b){var c=this;b||(c.$el.trigger("onBeforeClose.lg"),a(window).scrollTop(c.prevScrollTop)),b&&(c.s.dynamic||this.$items.off("click.lg click.lgcustom"),a.removeData(c.el,"lightGallery")),this.$el.off(".lg.tm"),a.each(a.fn.lightGallery.modules,function(a){c.modules[a]&&c.modules[a].destroy()}),this.lGalleryOn=!1,clearTimeout(c.hideBartimeout),this.hideBartimeout=!1,a(window).off(".lg"),a("body").removeClass("lg-on lg-from-hash"),c.$outer&&c.$outer.removeClass("lg-visible"),a(".lg-backdrop").removeClass("in"),setTimeout(function(){c.$outer&&c.$outer.remove(),a(".lg-backdrop").remove(),b||c.$el.trigger("onCloseAfter.lg")},c.s.backdropDuration+50)},a.fn.lightGallery=function(c){return this.each(function(){if(a.data(this,"lightGallery"))try{a(this).data("lightGallery").init()}catch(a){console.error("lightGallery has not initiated properly")}else a.data(this,"lightGallery",new b(this,c))})},a.fn.lightGallery.modules={}}()}),function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(0,function(a){!function(){"use strict";var b={autoplay:!1,pause:5e3,progressBar:!0,fourceAutoplay:!1,autoplayControls:!0,appendAutoplayControlsTo:".lg-toolbar"},c=function(c){return this.core=a(c).data("lightGallery"),this.$el=a(c),!(this.core.$items.length<2)&&(this.core.s=a.extend({},b,this.core.s),this.interval=!1,this.fromAuto=!0,this.canceledOnTouch=!1,this.fourceAutoplayTemp=this.core.s.fourceAutoplay,this.core.doCss()||(this.core.s.progressBar=!1),this.init(),this)};c.prototype.init=function(){var a=this;a.core.s.autoplayControls&&a.controls(),a.core.s.progressBar&&a.core.$outer.find(".lg").append('<div class="lg-progress-bar"><div class="lg-progress"></div></div>'),a.progress(),a.core.s.autoplay&&a.$el.one("onSlideItemLoad.lg.tm",function(){a.startlAuto()}),a.$el.on("onDragstart.lg.tm touchstart.lg.tm",function(){a.interval&&(a.cancelAuto(),a.canceledOnTouch=!0)}),a.$el.on("onDragend.lg.tm touchend.lg.tm onSlideClick.lg.tm",function(){!a.interval&&a.canceledOnTouch&&(a.startlAuto(),a.canceledOnTouch=!1)})},c.prototype.progress=function(){var a,b,c=this;c.$el.on("onBeforeSlide.lg.tm",function(){c.core.s.progressBar&&c.fromAuto&&(a=c.core.$outer.find(".lg-progress-bar"),b=c.core.$outer.find(".lg-progress"),c.interval&&(b.removeAttr("style"),a.removeClass("lg-start"),setTimeout(function(){b.css("transition","width "+(c.core.s.speed+c.core.s.pause)+"ms ease 0s"),a.addClass("lg-start")},20))),c.fromAuto||c.core.s.fourceAutoplay||c.cancelAuto(),c.fromAuto=!1})},c.prototype.controls=function(){var b=this;a(this.core.s.appendAutoplayControlsTo).append('<span class="lg-autoplay-button lg-icon"></span>'),b.core.$outer.find(".lg-autoplay-button").on("click.lg",function(){a(b.core.$outer).hasClass("lg-show-autoplay")?(b.cancelAuto(),b.core.s.fourceAutoplay=!1):b.interval||(b.startlAuto(),b.core.s.fourceAutoplay=b.fourceAutoplayTemp)})},c.prototype.startlAuto=function(){var a=this;a.core.$outer.find(".lg-progress").css("transition","width "+(a.core.s.speed+a.core.s.pause)+"ms ease 0s"),a.core.$outer.addClass("lg-show-autoplay"),a.core.$outer.find(".lg-progress-bar").addClass("lg-start"),a.interval=setInterval(function(){a.core.index+1<a.core.$items.length?a.core.index++:a.core.index=0,a.fromAuto=!0,a.core.slide(a.core.index,!1,!1,"next")},a.core.s.speed+a.core.s.pause)},c.prototype.cancelAuto=function(){clearInterval(this.interval),this.interval=!1,this.core.$outer.find(".lg-progress").removeAttr("style"),this.core.$outer.removeClass("lg-show-autoplay"),this.core.$outer.find(".lg-progress-bar").removeClass("lg-start")},c.prototype.destroy=function(){this.cancelAuto(),this.core.$outer.find(".lg-progress-bar").remove()},a.fn.lightGallery.modules.autoplay=c}()}),function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(0,function(a){!function(){"use strict";var b={fullScreen:!0},c=function(c){return this.core=a(c).data("lightGallery"),this.$el=a(c),this.core.s=a.extend({},b,this.core.s),this.init(),this};c.prototype.init=function(){var a="";if(this.core.s.fullScreen){if(!(document.fullscreenEnabled||document.webkitFullscreenEnabled||document.mozFullScreenEnabled||document.msFullscreenEnabled))return;a='<span class="lg-fullscreen lg-icon"></span>',this.core.$outer.find(".lg-toolbar").append(a),this.fullScreen()}},c.prototype.requestFullscreen=function(){var a=document.documentElement;a.requestFullscreen?a.requestFullscreen():a.msRequestFullscreen?a.msRequestFullscreen():a.mozRequestFullScreen?a.mozRequestFullScreen():a.webkitRequestFullscreen&&a.webkitRequestFullscreen()},c.prototype.exitFullscreen=function(){document.exitFullscreen?document.exitFullscreen():document.msExitFullscreen?document.msExitFullscreen():document.mozCancelFullScreen?document.mozCancelFullScreen():document.webkitExitFullscreen&&document.webkitExitFullscreen()},c.prototype.fullScreen=function(){var b=this;a(document).on("fullscreenchange.lg webkitfullscreenchange.lg mozfullscreenchange.lg MSFullscreenChange.lg",function(){b.core.$outer.toggleClass("lg-fullscreen-on")}),this.core.$outer.find(".lg-fullscreen").on("click.lg",function(){document.fullscreenElement||document.mozFullScreenElement||document.webkitFullscreenElement||document.msFullscreenElement?b.exitFullscreen():b.requestFullscreen()})},c.prototype.destroy=function(){this.exitFullscreen(),a(document).off("fullscreenchange.lg webkitfullscreenchange.lg mozfullscreenchange.lg MSFullscreenChange.lg")},a.fn.lightGallery.modules.fullscreen=c}()}),function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(0,function(a){!function(){"use strict";var b={pager:!1},c=function(c){return this.core=a(c).data("lightGallery"),this.$el=a(c),this.core.s=a.extend({},b,this.core.s),this.core.s.pager&&this.core.$items.length>1&&this.init(),this};c.prototype.init=function(){var b,c,d,e=this,f="";if(e.core.$outer.find(".lg").append('<div class="lg-pager-outer"></div>'),e.core.s.dynamic)for(var g=0;g<e.core.s.dynamicEl.length;g++)f+='<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="'+e.core.s.dynamicEl[g].thumb+'" /></div></span>';else e.core.$items.each(function(){e.core.s.exThumbImage?f+='<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="'+a(this).attr(e.core.s.exThumbImage)+'" /></div></span>':f+='<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="'+a(this).find("img").attr("src")+'" /></div></span>'});c=e.core.$outer.find(".lg-pager-outer"),c.html(f),b=e.core.$outer.find(".lg-pager-cont"),b.on("click.lg touchend.lg",function(){var b=a(this);e.core.index=b.index(),e.core.slide(e.core.index,!1,!0,!1)}),c.on("mouseover.lg",function(){clearTimeout(d),c.addClass("lg-pager-hover")}),c.on("mouseout.lg",function(){d=setTimeout(function(){c.removeClass("lg-pager-hover")})}),e.core.$el.on("onBeforeSlide.lg.tm",function(a,c,d){b.removeClass("lg-pager-active"),b.eq(d).addClass("lg-pager-active")})},c.prototype.destroy=function(){},a.fn.lightGallery.modules.pager=c}()}),function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(0,function(a){!function(){"use strict";var b={thumbnail:!0,animateThumb:!0,currentPagerPosition:"middle",thumbWidth:100,thumbHeight:"80px",thumbContHeight:100,thumbMargin:5,exThumbImage:!1,showThumbByDefault:!0,toogleThumb:!0,pullCaptionUp:!0,enableThumbDrag:!0,enableThumbSwipe:!0,swipeThreshold:50,loadYoutubeThumbnail:!0,youtubeThumbSize:1,loadVimeoThumbnail:!0,vimeoThumbSize:"thumbnail_small",loadDailymotionThumbnail:!0},c=function(c){return this.core=a(c).data("lightGallery"),this.core.s=a.extend({},b,this.core.s),this.$el=a(c),this.$thumbOuter=null,this.thumbOuterWidth=0,this.thumbTotalWidth=this.core.$items.length*(this.core.s.thumbWidth+this.core.s.thumbMargin),this.thumbIndex=this.core.index,this.core.s.animateThumb&&(this.core.s.thumbHeight="100%"),this.left=0,this.init(),this};c.prototype.init=function(){var a=this;this.core.s.thumbnail&&this.core.$items.length>1&&(this.core.s.showThumbByDefault&&setTimeout(function(){a.core.$outer.addClass("lg-thumb-open")},700),this.core.s.pullCaptionUp&&this.core.$outer.addClass("lg-pull-caption-up"),this.build(),this.core.s.animateThumb&&this.core.doCss()?(this.core.s.enableThumbDrag&&this.enableThumbDrag(),this.core.s.enableThumbSwipe&&this.enableThumbSwipe(),this.thumbClickable=!1):this.thumbClickable=!0,this.toogle(),this.thumbkeyPress())},c.prototype.build=function(){function b(a,b,c){var g,h=d.core.isVideo(a,c)||{},i="";h.youtube||h.vimeo||h.dailymotion?h.youtube?g=d.core.s.loadYoutubeThumbnail?"//img.youtube.com/vi/"+h.youtube[1]+"/"+d.core.s.youtubeThumbSize+".jpg":b:h.vimeo?d.core.s.loadVimeoThumbnail?(g="//i.vimeocdn.com/video/error_"+f+".jpg",i=h.vimeo[1]):g=b:h.dailymotion&&(g=d.core.s.loadDailymotionThumbnail?"//www.dailymotion.com/thumbnail/video/"+h.dailymotion[1]:b):g=b,e+='<div data-vimeo-id="'+i+'" class="lg-thumb-item" style="width:'+d.core.s.thumbWidth+"px; height: "+d.core.s.thumbHeight+"; margin-right: "+d.core.s.thumbMargin+'px"><img src="'+g+'" /></div>',i=""}var c,d=this,e="",f="",g='<div class="lg-thumb-outer"><div class="lg-thumb lg-group"></div></div>';switch(this.core.s.vimeoThumbSize){case"thumbnail_large":f="640";break;case"thumbnail_medium":f="200x150";break;case"thumbnail_small":f="100x75"}if(d.core.$outer.addClass("lg-has-thumb"),d.core.$outer.find(".lg").append(g),d.$thumbOuter=d.core.$outer.find(".lg-thumb-outer"),d.thumbOuterWidth=d.$thumbOuter.width(),d.core.s.animateThumb&&d.core.$outer.find(".lg-thumb").css({width:d.thumbTotalWidth+"px",position:"relative"}),this.core.s.animateThumb&&d.$thumbOuter.css("height",d.core.s.thumbContHeight+"px"),d.core.s.dynamic)for(var h=0;h<d.core.s.dynamicEl.length;h++)b(d.core.s.dynamicEl[h].src,d.core.s.dynamicEl[h].thumb,h);else d.core.$items.each(function(c){d.core.s.exThumbImage?b(a(this).attr("href")||a(this).attr("data-src"),a(this).attr(d.core.s.exThumbImage),c):b(a(this).attr("href")||a(this).attr("data-src"),a(this).find("img").attr("src"),c)});d.core.$outer.find(".lg-thumb").html(e),c=d.core.$outer.find(".lg-thumb-item"),c.each(function(){var b=a(this),c=b.attr("data-vimeo-id");c&&a.getJSON("//www.vimeo.com/api/v2/video/"+c+".json?callback=?",{format:"json"},function(a){b.find("img").attr("src",a[0][d.core.s.vimeoThumbSize])})}),c.eq(d.core.index).addClass("active"),d.core.$el.on("onBeforeSlide.lg.tm",function(){c.removeClass("active"),c.eq(d.core.index).addClass("active")}),c.on("click.lg touchend.lg",function(){var b=a(this);setTimeout(function(){(d.thumbClickable&&!d.core.lgBusy||!d.core.doCss())&&(d.core.index=b.index(),d.core.slide(d.core.index,!1,!0,!1))},50)}),d.core.$el.on("onBeforeSlide.lg.tm",function(){d.animateThumb(d.core.index)}),a(window).on("resize.lg.thumb orientationchange.lg.thumb",function(){setTimeout(function(){d.animateThumb(d.core.index),d.thumbOuterWidth=d.$thumbOuter.width()},200)})},c.prototype.setTranslate=function(a){this.core.$outer.find(".lg-thumb").css({transform:"translate3d(-"+a+"px, 0px, 0px)"})},c.prototype.animateThumb=function(a){var b=this.core.$outer.find(".lg-thumb");if(this.core.s.animateThumb){var c;switch(this.core.s.currentPagerPosition){case"left":c=0;break;case"middle":c=this.thumbOuterWidth/2-this.core.s.thumbWidth/2;break;case"right":c=this.thumbOuterWidth-this.core.s.thumbWidth}this.left=(this.core.s.thumbWidth+this.core.s.thumbMargin)*a-1-c,this.left>this.thumbTotalWidth-this.thumbOuterWidth&&(this.left=this.thumbTotalWidth-this.thumbOuterWidth),this.left<0&&(this.left=0),this.core.lGalleryOn?(b.hasClass("on")||this.core.$outer.find(".lg-thumb").css("transition-duration",this.core.s.speed+"ms"),this.core.doCss()||b.animate({left:-this.left+"px"},this.core.s.speed)):this.core.doCss()||b.css("left",-this.left+"px"),this.setTranslate(this.left)}},c.prototype.enableThumbDrag=function(){var b=this,c=0,d=0,e=!1,f=!1,g=0;b.$thumbOuter.addClass("lg-grab"),b.core.$outer.find(".lg-thumb").on("mousedown.lg.thumb",function(a){b.thumbTotalWidth>b.thumbOuterWidth&&(a.preventDefault(),c=a.pageX,e=!0,b.core.$outer.scrollLeft+=1,b.core.$outer.scrollLeft-=1,b.thumbClickable=!1,b.$thumbOuter.removeClass("lg-grab").addClass("lg-grabbing"))}),a(window).on("mousemove.lg.thumb",function(a){e&&(g=b.left,f=!0,d=a.pageX,b.$thumbOuter.addClass("lg-dragging"),g-=d-c,g>b.thumbTotalWidth-b.thumbOuterWidth&&(g=b.thumbTotalWidth-b.thumbOuterWidth),g<0&&(g=0),b.setTranslate(g))}),a(window).on("mouseup.lg.thumb",function(){f?(f=!1,b.$thumbOuter.removeClass("lg-dragging"),b.left=g,Math.abs(d-c)<b.core.s.swipeThreshold&&(b.thumbClickable=!0)):b.thumbClickable=!0,e&&(e=!1,b.$thumbOuter.removeClass("lg-grabbing").addClass("lg-grab"))})},c.prototype.enableThumbSwipe=function(){var a=this,b=0,c=0,d=!1,e=0;a.core.$outer.find(".lg-thumb").on("touchstart.lg",function(c){a.thumbTotalWidth>a.thumbOuterWidth&&(c.preventDefault(),b=c.originalEvent.targetTouches[0].pageX,a.thumbClickable=!1)}),a.core.$outer.find(".lg-thumb").on("touchmove.lg",function(f){a.thumbTotalWidth>a.thumbOuterWidth&&(f.preventDefault(),c=f.originalEvent.targetTouches[0].pageX,d=!0,a.$thumbOuter.addClass("lg-dragging"),e=a.left,e-=c-b,e>a.thumbTotalWidth-a.thumbOuterWidth&&(e=a.thumbTotalWidth-a.thumbOuterWidth),e<0&&(e=0),a.setTranslate(e))}),a.core.$outer.find(".lg-thumb").on("touchend.lg",function(){a.thumbTotalWidth>a.thumbOuterWidth&&d?(d=!1,a.$thumbOuter.removeClass("lg-dragging"),Math.abs(c-b)<a.core.s.swipeThreshold&&(a.thumbClickable=!0),a.left=e):a.thumbClickable=!0})},c.prototype.toogle=function(){var a=this;a.core.s.toogleThumb&&(a.core.$outer.addClass("lg-can-toggle"),a.$thumbOuter.append('<span class="lg-toogle-thumb lg-icon"></span>'),a.core.$outer.find(".lg-toogle-thumb").on("click.lg",function(){a.core.$outer.toggleClass("lg-thumb-open")}))},c.prototype.thumbkeyPress=function(){var b=this;a(window).on("keydown.lg.thumb",function(a){38===a.keyCode?(a.preventDefault(),b.core.$outer.addClass("lg-thumb-open")):40===a.keyCode&&(a.preventDefault(),b.core.$outer.removeClass("lg-thumb-open"))})},c.prototype.destroy=function(){this.core.s.thumbnail&&this.core.$items.length>1&&(a(window).off("resize.lg.thumb orientationchange.lg.thumb keydown.lg.thumb"),

this.$thumbOuter.remove(),this.core.$outer.removeClass("lg-has-thumb"))},a.fn.lightGallery.modules.Thumbnail=c}()}),function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof module&&module.exports?module.exports=b(require("jquery")):b(a.jQuery)}(this,function(a){!function(){"use strict";function b(a,b,c,d){var e=this;if(e.core.$slide.eq(b).find(".lg-video").append(e.loadVideo(c,"lg-object",!0,b,d)),d)if(e.core.s.videojs)try{videojs(e.core.$slide.eq(b).find(".lg-html5").get(0),e.core.s.videojsOptions,function(){!e.videoLoaded&&e.core.s.autoplayFirstVideo&&this.play()})}catch(a){console.error("Make sure you have included videojs")}else!e.videoLoaded&&e.core.s.autoplayFirstVideo&&e.core.$slide.eq(b).find(".lg-html5").get(0).play()}function c(a,b){var c=this.core.$slide.eq(b).find(".lg-video-cont");c.hasClass("lg-has-iframe")||(c.css("max-width",this.core.s.videoMaxWidth),this.videoLoaded=!0)}function d(b,c,d){var e=this,f=e.core.$slide.eq(c),g=f.find(".lg-youtube").get(0),h=f.find(".lg-vimeo").get(0),i=f.find(".lg-dailymotion").get(0),j=f.find(".lg-vk").get(0),k=f.find(".lg-html5").get(0);if(g)g.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}',"*");else if(h)try{$f(h).api("pause")}catch(a){console.error("Make sure you have included froogaloop2 js")}else if(i)i.contentWindow.postMessage("pause","*");else if(k)if(e.core.s.videojs)try{videojs(k).pause()}catch(a){console.error("Make sure you have included videojs")}else k.pause();j&&a(j).attr("src",a(j).attr("src").replace("&autoplay","&noplay"));var l;l=e.core.s.dynamic?e.core.s.dynamicEl[d].src:e.core.$items.eq(d).attr("href")||e.core.$items.eq(d).attr("data-src");var m=e.core.isVideo(l,d)||{};(m.youtube||m.vimeo||m.dailymotion||m.vk)&&e.core.$outer.addClass("lg-hide-download")}var e={videoMaxWidth:"855px",autoplayFirstVideo:!0,youtubePlayerParams:!1,vimeoPlayerParams:!1,dailymotionPlayerParams:!1,vkPlayerParams:!1,videojs:!1,videojsOptions:{}},f=function(b){return this.core=a(b).data("lightGallery"),this.$el=a(b),this.core.s=a.extend({},e,this.core.s),this.videoLoaded=!1,this.init(),this};f.prototype.init=function(){var e=this;e.core.$el.on("hasVideo.lg.tm",b.bind(this)),e.core.$el.on("onAferAppendSlide.lg.tm",c.bind(this)),e.core.doCss()&&e.core.$items.length>1&&(e.core.s.enableSwipe||e.core.s.enableDrag)?e.core.$el.on("onSlideClick.lg.tm",function(){var a=e.core.$slide.eq(e.core.index);e.loadVideoOnclick(a)}):e.core.$slide.on("click.lg",function(){e.loadVideoOnclick(a(this))}),e.core.$el.on("onBeforeSlide.lg.tm",d.bind(this)),e.core.$el.on("onAfterSlide.lg.tm",function(a,b){e.core.$slide.eq(b).removeClass("lg-video-playing")}),e.core.s.autoplayFirstVideo&&e.core.$el.on("onAferAppendSlide.lg.tm",function(a,b){if(!e.core.lGalleryOn){var c=e.core.$slide.eq(b);setTimeout(function(){e.loadVideoOnclick(c)},100)}})},f.prototype.loadVideo=function(b,c,d,e,f){var g="",h=1,i="",j=this.core.isVideo(b,e)||{};if(d&&(h=this.videoLoaded?0:this.core.s.autoplayFirstVideo?1:0),j.youtube)i="?wmode=opaque&autoplay="+h+"&enablejsapi=1",this.core.s.youtubePlayerParams&&(i=i+"&"+a.param(this.core.s.youtubePlayerParams)),g='<iframe class="lg-video-object lg-youtube '+c+'" width="560" height="315" src="//www.youtube.com/embed/'+j.youtube[1]+i+'" frameborder="0" allowfullscreen></iframe>';else if(j.vimeo)i="?autoplay="+h+"&api=1",this.core.s.vimeoPlayerParams&&(i=i+"&"+a.param(this.core.s.vimeoPlayerParams)),g='<iframe class="lg-video-object lg-vimeo '+c+'" width="560" height="315"  src="//player.vimeo.com/video/'+j.vimeo[1]+i+'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';else if(j.dailymotion)i="?wmode=opaque&autoplay="+h+"&api=postMessage",this.core.s.dailymotionPlayerParams&&(i=i+"&"+a.param(this.core.s.dailymotionPlayerParams)),g='<iframe class="lg-video-object lg-dailymotion '+c+'" width="560" height="315" src="//www.dailymotion.com/embed/video/'+j.dailymotion[1]+i+'" frameborder="0" allowfullscreen></iframe>';else if(j.html5){var k=f.substring(0,1);"."!==k&&"#"!==k||(f=a(f).html()),g=f}else j.vk&&(i="&autoplay="+h,this.core.s.vkPlayerParams&&(i=i+"&"+a.param(this.core.s.vkPlayerParams)),g='<iframe class="lg-video-object lg-vk '+c+'" width="560" height="315" src="//vk.com/video_ext.php?'+j.vk[1]+i+'" frameborder="0" allowfullscreen></iframe>');return g},f.prototype.loadVideoOnclick=function(a){var b=this;if(a.find(".lg-object").hasClass("lg-has-poster")&&a.find(".lg-object").is(":visible"))if(a.hasClass("lg-has-video")){var c=a.find(".lg-youtube").get(0),d=a.find(".lg-vimeo").get(0),e=a.find(".lg-dailymotion").get(0),f=a.find(".lg-html5").get(0);if(c)c.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}',"*");else if(d)try{$f(d).api("play")}catch(a){console.error("Make sure you have included froogaloop2 js")}else if(e)e.contentWindow.postMessage("play","*");else if(f)if(b.core.s.videojs)try{videojs(f).play()}catch(a){console.error("Make sure you have included videojs")}else f.play();a.addClass("lg-video-playing")}else{a.addClass("lg-video-playing lg-has-video");var g,h,i=function(c,d){if(a.find(".lg-video").append(b.loadVideo(c,"",!1,b.core.index,d)),d)if(b.core.s.videojs)try{videojs(b.core.$slide.eq(b.core.index).find(".lg-html5").get(0),b.core.s.videojsOptions,function(){this.play()})}catch(a){console.error("Make sure you have included videojs")}else b.core.$slide.eq(b.core.index).find(".lg-html5").get(0).play()};b.core.s.dynamic?(g=b.core.s.dynamicEl[b.core.index].src,h=b.core.s.dynamicEl[b.core.index].html,i(g,h)):(g=b.core.$items.eq(b.core.index).attr("href")||b.core.$items.eq(b.core.index).attr("data-src"),h=b.core.$items.eq(b.core.index).attr("data-html"),i(g,h));var j=a.find(".lg-object");a.find(".lg-video").append(j),a.find(".lg-video-object").hasClass("lg-html5")||(a.removeClass("lg-complete"),a.find(".lg-video-object").on("load.lg error.lg",function(){a.addClass("lg-complete")}))}},f.prototype.destroy=function(){this.videoLoaded=!1},a.fn.lightGallery.modules.video=f}()}),function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(0,function(a){!function(){"use strict";var b=function(){var a=!1,b=navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);return b&&parseInt(b[2],10)<54&&(a=!0),a},c={scale:1,zoom:!0,actualSize:!0,enableZoomAfter:300,useLeftForZoom:b()},d=function(b){return this.core=a(b).data("lightGallery"),this.core.s=a.extend({},c,this.core.s),this.core.s.zoom&&this.core.doCss()&&(this.init(),this.zoomabletimeout=!1,this.pageX=a(window).width()/2,this.pageY=a(window).height()/2+a(window).scrollTop()),this};d.prototype.init=function(){var b=this,c='<span id="lg-zoom-in" class="lg-icon"></span><span id="lg-zoom-out" class="lg-icon"></span>';b.core.s.actualSize&&(c+='<span id="lg-actual-size" class="lg-icon"></span>'),b.core.s.useLeftForZoom?b.core.$outer.addClass("lg-use-left-for-zoom"):b.core.$outer.addClass("lg-use-transition-for-zoom"),this.core.$outer.find(".lg-toolbar").append(c),b.core.$el.on("onSlideItemLoad.lg.tm.zoom",function(c,d,e){var f=b.core.s.enableZoomAfter+e;a("body").hasClass("lg-from-hash")&&e?f=0:a("body").removeClass("lg-from-hash"),b.zoomabletimeout=setTimeout(function(){b.core.$slide.eq(d).addClass("lg-zoomable")},f+30)});var d=1,e=function(c){var d,e,f=b.core.$outer.find(".lg-current .lg-image"),g=(a(window).width()-f.prop("offsetWidth"))/2,h=(a(window).height()-f.prop("offsetHeight"))/2+a(window).scrollTop();d=b.pageX-g,e=b.pageY-h;var i=(c-1)*d,j=(c-1)*e;f.css("transform","scale3d("+c+", "+c+", 1)").attr("data-scale",c),b.core.s.useLeftForZoom?f.parent().css({left:-i+"px",top:-j+"px"}).attr("data-x",i).attr("data-y",j):f.parent().css("transform","translate3d(-"+i+"px, -"+j+"px, 0)").attr("data-x",i).attr("data-y",j)},f=function(){d>1?b.core.$outer.addClass("lg-zoomed"):b.resetZoom(),d<1&&(d=1),e(d)},g=function(c,e,g,h){var i,j=e.prop("offsetWidth");i=b.core.s.dynamic?b.core.s.dynamicEl[g].width||e[0].naturalWidth||j:b.core.$items.eq(g).attr("data-width")||e[0].naturalWidth||j;var k;b.core.$outer.hasClass("lg-zoomed")?d=1:i>j&&(k=i/j,d=k||2),h?(b.pageX=a(window).width()/2,b.pageY=a(window).height()/2+a(window).scrollTop()):(b.pageX=c.pageX||c.originalEvent.targetTouches[0].pageX,b.pageY=c.pageY||c.originalEvent.targetTouches[0].pageY),f(),setTimeout(function(){b.core.$outer.removeClass("lg-grabbing").addClass("lg-grab")},10)},h=!1;b.core.$el.on("onAferAppendSlide.lg.tm.zoom",function(a,c){var d=b.core.$slide.eq(c).find(".lg-image");d.on("dblclick",function(a){g(a,d,c)}),d.on("touchstart",function(a){h?(clearTimeout(h),h=null,g(a,d,c)):h=setTimeout(function(){h=null},300),a.preventDefault()})}),a(window).on("resize.lg.zoom scroll.lg.zoom orientationchange.lg.zoom",function(){b.pageX=a(window).width()/2,b.pageY=a(window).height()/2+a(window).scrollTop(),e(d)}),a("#lg-zoom-out").on("click.lg",function(){b.core.$outer.find(".lg-current .lg-image").length&&(d-=b.core.s.scale,f())}),a("#lg-zoom-in").on("click.lg",function(){b.core.$outer.find(".lg-current .lg-image").length&&(d+=b.core.s.scale,f())}),a("#lg-actual-size").on("click.lg",function(a){g(a,b.core.$slide.eq(b.core.index).find(".lg-image"),b.core.index,!0)}),b.core.$el.on("onBeforeSlide.lg.tm",function(){d=1,b.resetZoom()}),b.zoomDrag(),b.zoomSwipe()},d.prototype.resetZoom=function(){this.core.$outer.removeClass("lg-zoomed"),this.core.$slide.find(".lg-img-wrap").removeAttr("style data-x data-y"),this.core.$slide.find(".lg-image").removeAttr("style data-scale"),this.pageX=a(window).width()/2,this.pageY=a(window).height()/2+a(window).scrollTop()},d.prototype.zoomSwipe=function(){var a=this,b={},c={},d=!1,e=!1,f=!1;a.core.$slide.on("touchstart.lg",function(c){if(a.core.$outer.hasClass("lg-zoomed")){var d=a.core.$slide.eq(a.core.index).find(".lg-object");f=d.prop("offsetHeight")*d.attr("data-scale")>a.core.$outer.find(".lg").height(),e=d.prop("offsetWidth")*d.attr("data-scale")>a.core.$outer.find(".lg").width(),(e||f)&&(c.preventDefault(),b={x:c.originalEvent.targetTouches[0].pageX,y:c.originalEvent.targetTouches[0].pageY})}}),a.core.$slide.on("touchmove.lg",function(g){if(a.core.$outer.hasClass("lg-zoomed")){var h,i,j=a.core.$slide.eq(a.core.index).find(".lg-img-wrap");g.preventDefault(),d=!0,c={x:g.originalEvent.targetTouches[0].pageX,y:g.originalEvent.targetTouches[0].pageY},a.core.$outer.addClass("lg-zoom-dragging"),i=f?-Math.abs(j.attr("data-y"))+(c.y-b.y):-Math.abs(j.attr("data-y")),h=e?-Math.abs(j.attr("data-x"))+(c.x-b.x):-Math.abs(j.attr("data-x")),(Math.abs(c.x-b.x)>15||Math.abs(c.y-b.y)>15)&&(a.core.s.useLeftForZoom?j.css({left:h+"px",top:i+"px"}):j.css("transform","translate3d("+h+"px, "+i+"px, 0)"))}}),a.core.$slide.on("touchend.lg",function(){a.core.$outer.hasClass("lg-zoomed")&&d&&(d=!1,a.core.$outer.removeClass("lg-zoom-dragging"),a.touchendZoom(b,c,e,f))})},d.prototype.zoomDrag=function(){var b=this,c={},d={},e=!1,f=!1,g=!1,h=!1;b.core.$slide.on("mousedown.lg.zoom",function(d){var f=b.core.$slide.eq(b.core.index).find(".lg-object");h=f.prop("offsetHeight")*f.attr("data-scale")>b.core.$outer.find(".lg").height(),g=f.prop("offsetWidth")*f.attr("data-scale")>b.core.$outer.find(".lg").width(),b.core.$outer.hasClass("lg-zoomed")&&a(d.target).hasClass("lg-object")&&(g||h)&&(d.preventDefault(),c={x:d.pageX,y:d.pageY},e=!0,b.core.$outer.scrollLeft+=1,b.core.$outer.scrollLeft-=1,b.core.$outer.removeClass("lg-grab").addClass("lg-grabbing"))}),a(window).on("mousemove.lg.zoom",function(a){if(e){var i,j,k=b.core.$slide.eq(b.core.index).find(".lg-img-wrap");f=!0,d={x:a.pageX,y:a.pageY},b.core.$outer.addClass("lg-zoom-dragging"),j=h?-Math.abs(k.attr("data-y"))+(d.y-c.y):-Math.abs(k.attr("data-y")),i=g?-Math.abs(k.attr("data-x"))+(d.x-c.x):-Math.abs(k.attr("data-x")),b.core.s.useLeftForZoom?k.css({left:i+"px",top:j+"px"}):k.css("transform","translate3d("+i+"px, "+j+"px, 0)")}}),a(window).on("mouseup.lg.zoom",function(a){e&&(e=!1,b.core.$outer.removeClass("lg-zoom-dragging"),!f||c.x===d.x&&c.y===d.y||(d={x:a.pageX,y:a.pageY},b.touchendZoom(c,d,g,h)),f=!1),b.core.$outer.removeClass("lg-grabbing").addClass("lg-grab")})},d.prototype.touchendZoom=function(a,b,c,d){var e=this,f=e.core.$slide.eq(e.core.index).find(".lg-img-wrap"),g=e.core.$slide.eq(e.core.index).find(".lg-object"),h=-Math.abs(f.attr("data-x"))+(b.x-a.x),i=-Math.abs(f.attr("data-y"))+(b.y-a.y),j=(e.core.$outer.find(".lg").height()-g.prop("offsetHeight"))/2,k=Math.abs(g.prop("offsetHeight")*Math.abs(g.attr("data-scale"))-e.core.$outer.find(".lg").height()+j),l=(e.core.$outer.find(".lg").width()-g.prop("offsetWidth"))/2,m=Math.abs(g.prop("offsetWidth")*Math.abs(g.attr("data-scale"))-e.core.$outer.find(".lg").width()+l);(Math.abs(b.x-a.x)>15||Math.abs(b.y-a.y)>15)&&(d&&(i<=-k?i=-k:i>=-j&&(i=-j)),c&&(h<=-m?h=-m:h>=-l&&(h=-l)),d?f.attr("data-y",Math.abs(i)):i=-Math.abs(f.attr("data-y")),c?f.attr("data-x",Math.abs(h)):h=-Math.abs(f.attr("data-x")),e.core.s.useLeftForZoom?f.css({left:h+"px",top:i+"px"}):f.css("transform","translate3d("+h+"px, "+i+"px, 0)"))},d.prototype.destroy=function(){var b=this;b.core.$el.off(".lg.zoom"),a(window).off(".lg.zoom"),b.core.$slide.off(".lg.zoom"),b.core.$el.off(".lg.tm.zoom"),b.resetZoom(),clearTimeout(b.zoomabletimeout),b.zoomabletimeout=!1},a.fn.lightGallery.modules.zoom=d}()}),function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(0,function(a){!function(){"use strict";var b={hash:!0},c=function(c){return this.core=a(c).data("lightGallery"),this.core.s=a.extend({},b,this.core.s),this.core.s.hash&&(this.oldHash=window.location.hash,this.init()),this};c.prototype.init=function(){var b,c=this;c.core.$el.on("onAfterSlide.lg.tm",function(a,b,d){history.replaceState?history.replaceState(null,null,window.location.pathname+window.location.search+"#lg="+c.core.s.galleryId+"&slide="+d):window.location.hash="lg="+c.core.s.galleryId+"&slide="+d}),a(window).on("hashchange.lg.hash",function(){b=window.location.hash;var a=parseInt(b.split("&slide=")[1],10);b.indexOf("lg="+c.core.s.galleryId)>-1?c.core.slide(a,!1,!1):c.core.lGalleryOn&&c.core.destroy()})},c.prototype.destroy=function(){this.core.s.hash&&(this.oldHash&&this.oldHash.indexOf("lg="+this.core.s.galleryId)<0?history.replaceState?history.replaceState(null,null,this.oldHash):window.location.hash=this.oldHash:history.replaceState?history.replaceState(null,document.title,window.location.pathname+window.location.search):window.location.hash="",this.core.$el.off(".lg.hash"))},a.fn.lightGallery.modules.hash=c}()}),function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(0,function(a){!function(){"use strict";var b={share:!0,facebook:!0,facebookDropdownText:"Facebook",twitter:!0,twitterDropdownText:"Twitter",googlePlus:!0,googlePlusDropdownText:"GooglePlus",pinterest:!0,pinterestDropdownText:"Pinterest"},c=function(c){return this.core=a(c).data("lightGallery"),this.core.s=a.extend({},b,this.core.s),this.core.s.share&&this.init(),this};c.prototype.init=function(){var b=this,c='<span id="lg-share" class="lg-icon"><ul class="lg-dropdown" style="position: absolute;">';c+=b.core.s.facebook?'<li><a id="lg-share-facebook" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">'+this.core.s.facebookDropdownText+"</span></a></li>":"",c+=b.core.s.twitter?'<li><a id="lg-share-twitter" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">'+this.core.s.twitterDropdownText+"</span></a></li>":"",c+=b.core.s.googlePlus?'<li><a id="lg-share-googleplus" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">'+this.core.s.googlePlusDropdownText+"</span></a></li>":"",c+=b.core.s.pinterest?'<li><a id="lg-share-pinterest" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">'+this.core.s.pinterestDropdownText+"</span></a></li>":"",c+="</ul></span>",this.core.$outer.find(".lg-toolbar").append(c),this.core.$outer.find(".lg").append('<div id="lg-dropdown-overlay"></div>'),a("#lg-share").on("click.lg",function(){b.core.$outer.toggleClass("lg-dropdown-active")}),a("#lg-dropdown-overlay").on("click.lg",function(){b.core.$outer.removeClass("lg-dropdown-active")}),b.core.$el.on("onAfterSlide.lg.tm",function(c,d,e){setTimeout(function(){a("#lg-share-facebook").attr("href","https://www.facebook.com/sharer/sharer.php?u="+encodeURIComponent(b.getSahreProps(e,"facebookShareUrl")||window.location.href)),a("#lg-share-twitter").attr("href","https://twitter.com/intent/tweet?text="+b.getSahreProps(e,"tweetText")+"&url="+encodeURIComponent(b.getSahreProps(e,"twitterShareUrl")||window.location.href)),a("#lg-share-googleplus").attr("href","https://plus.google.com/share?url="+encodeURIComponent(b.getSahreProps(e,"googleplusShareUrl")||window.location.href)),a("#lg-share-pinterest").attr("href","http://www.pinterest.com/pin/create/button/?url="+encodeURIComponent(b.getSahreProps(e,"pinterestShareUrl")||window.location.href)+"&media="+encodeURIComponent(b.getSahreProps(e,"src"))+"&description="+b.getSahreProps(e,"pinterestText"))},100)})},c.prototype.getSahreProps=function(a,b){var c="";if(this.core.s.dynamic)c=this.core.s.dynamicEl[a][b];else{var d=this.core.$items.eq(a).attr("href"),e=this.core.$items.eq(a).data(b);c="src"===b?d||e:e}return c},c.prototype.destroy=function(){},a.fn.lightGallery.modules.share=c}()});



/* ############ */



$(document).ready(function(){

    $('#lightgallery').lightGallery();

});

/*Light Gallery Ends*/