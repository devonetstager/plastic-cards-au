<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePagesTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('pages', function (Blueprint $table) {

            $table->boolean('page_type')->after('page_sort_order')->comment('0-Pages, 1-Gallery Page')->default(0);
           

           // to change column datatype or change it to `nullable` 
           // or set default values , this is just example

          

        });
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
