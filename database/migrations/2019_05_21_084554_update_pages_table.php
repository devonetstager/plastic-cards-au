<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {

            $table->string('page_heading')->after('id');
            $table->text('page_description')->after('id');
            $table->string('page_meta_title')->after('id');
            $table->string('page_meta_keywords')->after('id');
            $table->text('page_meta_description')->after('id');
            $table->boolean('page_publish_status')->after('id')->default(1);

           // to change column datatype or change it to `nullable` 
           // or set default values , this is just example

          

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
