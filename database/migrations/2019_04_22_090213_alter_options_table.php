<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('options', function (Blueprint $table) {

            $table->string('facebook')->after('id');
			$table->string('twitter')->after('id');
			$table->string('instagram')->after('id');
			$table->string('googleplus')->after('id');
			$table->string('linkedin')->after('id');
			$table->string('youtube')->after('id');

           // to change column datatype or change it to `nullable` 
           // or set default values , this is just example

          

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
