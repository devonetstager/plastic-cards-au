-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2020 at 11:19 PM
-- Server version: 5.6.46-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `plasticardau`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_submissions`
--

CREATE TABLE `contact_submissions` (
  `con_id` int(11) NOT NULL,
  `con_firstname` varchar(100) DEFAULT NULL,
  `con_lastname` varchar(100) DEFAULT NULL,
  `con_email` varchar(500) DEFAULT NULL,
  `con_inquiry_type` varchar(100) DEFAULT NULL,
  `con_inquiry` text,
  `con_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_submissions`
--

INSERT INTO `contact_submissions` (`con_id`, `con_firstname`, `con_lastname`, `con_email`, `con_inquiry_type`, `con_inquiry`, `con_date`) VALUES
(1, 'Clixpert Test', 'test', 'clixperttest@gmail.com', 'Inquiry', 'Testing, kindly ignore this.', '2019-06-28 05:21:11'),
(2, 'Alex', 'Test', 'amaniago@clixpert.com.au', 'Inquiry', 'Test', '2019-06-28 06:09:45'),
(3, 'christine', 'manning', 'christinemanning83@gmail.com', 'General Comments', 'hi 532258!', '2019-06-30 17:08:52'),
(4, '55manda4', 'le!', 'srtee@google.com', 'Inquiry', 'quote please', '2019-06-30 17:13:10'),
(5, 'Alex Test', 'Test', 'amaniago@clixpert.com.au', NULL, 'test', '2019-06-30 22:59:03'),
(6, 'Alex', 'test', 'amaniago@clixpert.com.au', 'Inquiry', 'test', '2019-07-02 23:38:09'),
(7, 'Fariz', 'Bey', 'fbey@clixpert.com.au', 'Inquiry', 'this is a test please ignore', '2019-07-02 23:49:14'),
(8, 'Alex Test', 'Test 2', 'amaniago@clixpert.com.au', 'Sales', 'test', '2019-07-03 23:10:07'),
(9, 'kathy', 'bertolami', 'bertolami@iinet.net.au', 'Sales', 'Do your cards integrate with pos systems? Namely Square?\r\nI would like a gift card designed and would like to know what your turn around time is\r\nMany Thanks\r\nKathy', '2019-07-29 22:34:38'),
(10, 'paul', 'sweeney', 'sweeneypaul4@bigpond.com', 'Sales', 'Hi - Just wondering  can we just purchase generic cards to use for our members  - we want to have the ability to add printed name and bar codes (ourselves or at a printers ) and then scan with our price reader ( on our till) to give members points, discounts etc... Is this possible? Many thanks Paul', '2019-07-31 20:29:48'),
(11, 'Tim', 'Beattie', 'timbeattie1@live.com', 'Inquiry', 'Are you able to print individual membership cards (e.g. with name and membership number) on an annual basis for a society with around 400 members and also mail the cards to the members once printed?', '2019-10-06 20:55:51'),
(12, 'Susan', 'HULL', 'headoffice@buildpro.com.au', 'General Comments', 'Can you tell me if your card are UV protected?\r\nRequire approx 1000, two colour print. (Black and Orange).', '2019-10-21 15:56:42'),
(13, 'Pete', 'Struik', 'pete@bigscreencinemas.com.au', 'Sales', 'I am looking for options regarding Gift Cards,\r\nOur business currently uses Gift Vouchers and we are looking into the possibility of producing something more modern and professional like the gift cards you can get from Bunnings, JB hifi and  Coles for example.  \r\nWe are ideally looking for something that can be used with Paywave,\r\nThat can be loaded with a value when purchased would be ideal,\r\nAdditional features could include the ability to add money online to a previously purchased card.\r\nPlease Contact me if this is something you can provide or please direct me to a company that might be able to facilitate such a thing if you cannot.\r\nKind Regards,\r\nPete Struik\r\npete@bigscreencinemas.com.au', '2019-10-21 23:35:16'),
(14, 'Ruwani', 'Peiris', 'ruwani_p@hotmail.com', 'Inquiry', 'Hi, I was wondering whether your company offers a plastic cards recycling service? Thank you :)', '2019-10-22 02:46:31'),
(15, 'Alex Test 3', 'Test', 'amaniago@clixpert.com.au', 'Other', 'Test Please ignore.', '2019-11-05 15:51:18'),
(16, 'Alex Test', 'Test', 'test@gmail.com', 'Inquiry', 'test', '2019-11-05 22:24:47'),
(17, 'Alex', 'Test', 'amaniago@clixpert.com.au', 'Sales', 'test', '2019-11-06 15:10:30'),
(18, 'Majid', 'sadeghi', 'sales@ariadcard.com.au', 'Sales', 'Dear Sir/Madam\r\nHope this letter finds you well,  I would like to introduce  ARIADCARD company.\r\nARIADCARD is the leading plastic card producer in Australia. If you are thinking of purchasing your necessary plastic card including Gift cards , VIP cards , Customer club cards, Loyalty cards , Membership cards ,visit cards, ....  please visit our website (www.ariadcard.com.au)\r\nand send us your inquiry via Email (sales@ariadcard.com.au)\r\nWe produce 10 pcs untill over 10,000 PVC cards (RFID /smart/Magnetic/,... cards) every business day of the year to support\r\na broad range of private, public and government entities.\r\nOur customer service team is here to answer all your questions regarding your plastic card needs.                                                      We will be glad you following and give new experience with Ariadcard.                                                                                                                   Even 10 pcs -1000 pcs delivery  one day****Best quality & Best Price *****                                                                                                                   Thanks                                                                                                                             Best regards', '2019-11-07 03:02:57'),
(19, 'Ellen', 'Barrett', 'ellen.barrett@syd.catholic.edu.au', NULL, 'I have an entry on our credit card for $660.86 could you please send me the invoice for this.  Kind regards Ellen Barrett St Gertrude\'s Catholic Primary School 1-11 Justin Street Smithfield. 029609 4144', '2019-12-10 14:50:38'),
(20, 'Maintenance Check', 'Check', 'amaniago@clixpert.com.au', 'Sales', 'Please ignore', '2020-02-12 17:20:33'),
(21, 'stephanie', 'raphael', 'stephmelanie@gmail.com', 'General Comments', 'test', '2020-02-12 17:27:45'),
(22, 'christine', 'manning', 'christinemanning83@gmail.com', 'Sales', 'test', '2020-02-12 17:28:01'),
(23, 'test', 'test only', 'clixpertmaintanance@gmail.com', 'Other', 'test only', '2020-02-12 21:15:23');

-- --------------------------------------------------------

--
-- Table structure for table `cpau_cmsv_delete`
--

CREATE TABLE `cpau_cmsv_delete` (
  `cms_id` bigint(20) UNSIGNED NOT NULL,
  `cms_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cms_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cms_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `gallery_id` int(11) NOT NULL,
  `gallery_page_id` int(11) DEFAULT NULL,
  `gallery_original_name` varchar(500) DEFAULT NULL,
  `gallery_file` varchar(500) DEFAULT NULL,
  `gallery_image_order` int(11) NOT NULL DEFAULT '0',
  `gallery_trash_status` tinyint(1) DEFAULT '0',
  `gallery_created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`gallery_id`, `gallery_page_id`, `gallery_original_name`, `gallery_file`, `gallery_image_order`, `gallery_trash_status`, `gallery_created_date`) VALUES
(17, 9, '3.jpg', 'gallery_155878855986.jpg', 0, 0, '2019-05-25 12:49:19'),
(18, 9, '4.jpg', 'gallery_155878855914.jpg', 0, 0, '2019-05-25 12:49:19'),
(19, 9, '2.jpg', 'gallery_155878855931.jpg', 0, 0, '2019-05-25 12:49:19'),
(20, 9, '1.jpg', 'gallery_155878855928.jpg', 0, 0, '2019-05-25 12:49:19'),
(21, 9, '18.jpg', 'gallery_155878855941.jpg', 0, 0, '2019-05-25 12:49:19'),
(22, 9, '5.jpg', 'gallery_155878855974.jpg', 0, 0, '2019-05-25 12:49:19'),
(23, 9, '6.jpg', 'gallery_155878856138.jpg', 0, 0, '2019-05-25 12:49:21'),
(24, 9, '10.jpg', 'gallery_155878856124.jpg', 0, 0, '2019-05-25 12:49:21'),
(25, 9, '9.jpg', 'gallery_155878856171.jpg', 0, 0, '2019-05-25 12:49:21'),
(26, 9, '8.jpg', 'gallery_155878856145.jpg', 0, 0, '2019-05-25 12:49:21'),
(27, 9, '11.jpg', 'gallery_155878856163.jpg', 0, 0, '2019-05-25 12:49:21'),
(28, 9, '7.jpg', 'gallery_155878856115.jpg', 0, 0, '2019-05-25 12:49:21'),
(29, 9, '14.jpg', 'gallery_155878856275.jpg', 0, 0, '2019-05-25 12:49:22'),
(30, 9, '16.jpg', 'gallery_155878856215.jpg', 0, 0, '2019-05-25 12:49:22'),
(31, 9, '12.jpg', 'gallery_155878856251.jpg', 0, 0, '2019-05-25 12:49:22'),
(32, 9, '13.jpg', 'gallery_155878856277.jpg', 0, 0, '2019-05-25 12:49:22'),
(33, 9, '15.jpg', 'gallery_155878856277.jpg', 0, 0, '2019-05-25 12:49:22'),
(34, 9, '17.jpg', 'gallery_155878856282.jpg', 0, 0, '2019-05-25 12:49:22'),
(57, 11, 'access2.jpg', 'gallery_155879969593.jpg', 0, 0, '2019-05-25 15:54:55'),
(58, 11, 'memcard14.jpg', 'gallery_155879969586.jpg', 0, 0, '2019-05-25 15:54:55'),
(59, 11, 'memcard5.jpg', 'gallery_155879969556.jpg', 0, 0, '2019-05-25 15:54:55'),
(60, 11, 'memcard11.jpg', 'gallery_155879969581.jpg', 0, 0, '2019-05-25 15:54:55'),
(61, 11, 'adv17.jpg', 'gallery_155879969544.jpg', 0, 0, '2019-05-25 15:54:55'),
(62, 11, 'adv14-front.jpg', 'gallery_155879969671.jpg', 0, 0, '2019-05-25 15:54:56'),
(63, 11, 'loyal11.jpg', 'gallery_155879969618.jpg', 0, 0, '2019-05-25 15:54:56'),
(64, 11, 'gc5.jpg', 'gallery_155879969654.jpg', 0, 0, '2019-05-25 15:54:56'),
(65, 11, 'disc9.jpg', 'gallery_155879969635.jpg', 0, 0, '2019-05-25 15:54:56'),
(66, 12, 'access2.jpg', 'gallery_155879982079.jpg', 0, 0, '2019-05-25 15:57:00'),
(67, 12, 'loyal5.jpg', 'gallery_155879982068.jpg', 0, 0, '2019-05-25 15:57:00'),
(68, 12, 'loyal7.jpg', 'gallery_155879982062.jpg', 0, 0, '2019-05-25 15:57:00'),
(69, 12, 'loyal6.jpg', 'gallery_155879982080.jpg', 0, 0, '2019-05-25 15:57:00'),
(70, 12, 'memcard5.jpg', 'gallery_155879982027.jpg', 0, 0, '2019-05-25 15:57:00'),
(71, 12, 'adv14-front.jpg', 'gallery_155879982093.jpg', 0, 0, '2019-05-25 15:57:00'),
(72, 12, 'adv17.jpg', 'gallery_155879982099.jpg', 0, 0, '2019-05-25 15:57:00'),
(73, 12, 'loyal1.jpg', 'gallery_155879982170.jpg', 0, 0, '2019-05-25 15:57:01'),
(74, 12, 'disc7.jpg', 'gallery_155879982134.jpg', 0, 0, '2019-05-25 15:57:01'),
(75, 13, 'memcard11.jpg', 'gallery_155879997913.jpg', 0, 0, '2019-05-25 15:59:39'),
(76, 13, 'adv14-front.jpg', 'gallery_155879997919.jpg', 0, 0, '2019-05-25 15:59:39'),
(77, 13, 'disc9.jpg', 'gallery_155879997955.jpg', 0, 0, '2019-05-25 15:59:39'),
(78, 13, 'disc7.jpg', 'gallery_155879997935.jpg', 0, 0, '2019-05-25 15:59:39'),
(79, 13, 'memcard5.jpg', 'gallery_155879997971.jpg', 0, 0, '2019-05-25 15:59:39'),
(80, 13, 'memcard14.jpg', 'gallery_155879997993.jpg', 0, 0, '2019-05-25 15:59:39'),
(81, 13, 'gc4-front.jpg', 'gallery_155879998064.jpg', 0, 0, '2019-05-25 15:59:40'),
(82, 13, 'gc5.jpg', 'gallery_155879998078.jpg', 0, 0, '2019-05-25 15:59:40'),
(83, 13, 'loyal7.jpg', 'gallery_155879998032.jpg', 0, 0, '2019-05-25 15:59:40'),
(84, 14, 'custom8.jpg', 'gallery_155880005898.jpg', 0, 0, '2019-05-25 16:00:58'),
(85, 14, 'custom7.jpg', 'gallery_155880005832.jpg', 0, 0, '2019-05-25 16:00:58'),
(86, 14, 'custom1.jpg', 'gallery_155880005852.jpg', 0, 0, '2019-05-25 16:00:58'),
(87, 14, 'custom2.jpg', 'gallery_155880005929.jpg', 0, 0, '2019-05-25 16:00:59'),
(88, 14, 'custom5.jpg', 'gallery_155880005930.jpg', 0, 0, '2019-05-25 16:00:59'),
(90, 14, 'custom6.jpg', 'gallery_155880005990.jpg', 0, 0, '2019-05-25 16:00:59'),
(106, 15, 'mailer1a.jpg', 'gallery_155911869059.jpg', 0, 0, '2019-05-29 08:31:30'),
(107, 15, 'mailer2a.jpg', 'gallery_155911891757.jpg', 0, 0, '2019-05-29 08:35:17'),
(108, 15, 'mail5.jpg', 'gallery_155911891786.jpg', 0, 0, '2019-05-29 08:35:17'),
(109, 15, 'mail4.jpg', 'gallery_155911891787.jpg', 0, 0, '2019-05-29 08:35:17'),
(176, 14, 'IMG__5.jpg', 'img-5.jpg', 0, 0, '2019-06-28 10:06:25'),
(177, 15, 'MAIL__5B.jpg', 'mail-5b.jpg', 0, 0, '2019-06-28 10:07:17'),
(178, 8, 'plastic-cards-australia-credit-card-size-cards-16.jpg', 'plastic-cards-australia-credit-card-size-cards-16.jpg', 0, 0, '2019-07-05 22:49:18'),
(179, 8, 'plastic-cards-australia-credit-card-size-cards-1.jpg', 'plastic-cards-australia-credit-card-size-cards-1.jpg', 0, 0, '2019-07-05 22:49:18'),
(180, 8, 'plastic-cards-australia-credit-card-size-cards-17.jpg', 'plastic-cards-australia-credit-card-size-cards-17.jpg', 0, 0, '2019-07-05 22:49:18'),
(181, 8, 'plastic-cards-australia-credit-card-size-cards-15.jpg', 'plastic-cards-australia-credit-card-size-cards-15.jpg', 0, 0, '2019-07-05 22:49:18'),
(182, 8, 'plastic-cards-australia-credit-card-size-cards-2.jpg', 'plastic-cards-australia-credit-card-size-cards-2.jpg', 0, 0, '2019-07-05 22:49:18'),
(183, 8, 'plastic-cards-australia-credit-card-size-cards-3.jpg', 'plastic-cards-australia-credit-card-size-cards-3.jpg', 0, 0, '2019-07-05 22:49:18'),
(184, 8, 'plastic-cards-australia-credit-card-size-cards-5.jpg', 'plastic-cards-australia-credit-card-size-cards-5.jpg', 0, 0, '2019-07-05 22:49:19'),
(185, 8, 'plastic-cards-australia-credit-card-size-cards-6.jpg', 'plastic-cards-australia-credit-card-size-cards-6.jpg', 0, 0, '2019-07-05 22:49:19'),
(186, 8, 'plastic-cards-australia-credit-card-size-cards-10 _1_.jpg', 'plastic-cards-australia-credit-card-size-cards-10-1.jpg', 0, 0, '2019-07-05 22:49:20'),
(187, 8, 'plastic-cards-australia-credit-card-size-cards-14.jpg', 'plastic-cards-australia-credit-card-size-cards-14.jpg', 0, 0, '2019-07-05 22:49:20'),
(188, 8, 'plastic-cards-australia-credit-card-size-cards-11.jpg', 'plastic-cards-australia-credit-card-size-cards-11.jpg', 0, 0, '2019-07-05 22:49:20'),
(189, 8, 'plastic-cards-australia-credit-card-size-cards-7.jpg', 'plastic-cards-australia-credit-card-size-cards-7.jpg', 0, 0, '2019-07-05 22:49:20'),
(190, 8, 'plastic-cards-australia-credit-card-size-cards-4.jpg', 'plastic-cards-australia-credit-card-size-cards-4.jpg', 0, 0, '2019-07-05 22:49:20'),
(191, 8, 'plastic-cards-australia-credit-card-size-cards-12.jpg', 'plastic-cards-australia-credit-card-size-cards-12.jpg', 0, 0, '2019-07-05 22:49:20'),
(192, 8, 'plastic-cards-australia-credit-card-size-cards-9.jpg', 'plastic-cards-australia-credit-card-size-cards-9.jpg', 0, 0, '2019-07-05 22:49:20'),
(193, 8, 'plastic-cards-australia-credit-card-size-cards-8.jpg', 'plastic-cards-australia-credit-card-size-cards-8.jpg', 0, 0, '2019-07-05 22:49:20'),
(194, 8, 'plastic-cards-australia-credit-card-size-cards-13.jpg', 'plastic-cards-australia-credit-card-size-cards-13.jpg', 0, 0, '2019-07-05 22:49:20'),
(195, 8, 'plastic-cards-australia-credit-card-size-cards-18.jpg', 'plastic-cards-australia-credit-card-size-cards-18.jpg', 0, 0, '2019-07-05 22:49:45'),
(196, 24, 'plastic-cards-australia-business-card-6.jpg', 'plastic-cards-australia-business-card-6.jpg', 0, 0, '2019-07-05 22:56:17'),
(197, 24, 'plastic-cards-australia-business-card-3.jpg', 'plastic-cards-australia-business-card-3.jpg', 0, 0, '2019-07-05 22:56:20'),
(198, 24, 'plastic-cards-australia-business-card-9.jpg', 'plastic-cards-australia-business-card-9.jpg', 0, 0, '2019-07-05 22:56:20'),
(199, 24, 'plastic-cards-australia-business-card-5.jpg', 'plastic-cards-australia-business-card-5.jpg', 0, 0, '2019-07-05 22:56:20'),
(200, 24, 'plastic-cards-australia-business-card-8.jpg', 'plastic-cards-australia-business-card-8.jpg', 0, 0, '2019-07-05 22:56:20'),
(201, 24, 'plastic-cards-australia-business-card-2.jpg', 'plastic-cards-australia-business-card-2.jpg', 0, 0, '2019-07-05 22:56:20'),
(202, 24, 'plastic-cards-australia-business-card-7.jpg', 'plastic-cards-australia-business-card-7.jpg', 0, 0, '2019-07-05 22:56:20'),
(203, 24, 'plastic-cards-australia-business-card-1.jpg', 'plastic-cards-australia-business-card-1.jpg', 0, 0, '2019-07-05 22:56:20'),
(204, 24, 'plastic-cards-australia-business-card-4.jpg', 'plastic-cards-australia-business-card-4.jpg', 0, 0, '2019-07-05 22:56:20'),
(205, 10, 'plastic-cards-australia-combo-tag-2.jpg', 'plastic-cards-australia-combo-tag-2.jpg', 0, 0, '2019-07-05 23:03:46'),
(206, 10, 'plastic-cards-australia-combo-tag-4.jpg', 'plastic-cards-australia-combo-tag-4.jpg', 0, 0, '2019-07-05 23:03:46'),
(207, 10, 'plastic-cards-australia-combo-tag-3.jpg', 'plastic-cards-australia-combo-tag-3.jpg', 0, 0, '2019-07-05 23:03:46'),
(208, 10, 'plastic-cards-australia-combo-tag-6.jpg', 'plastic-cards-australia-combo-tag-6.jpg', 0, 0, '2019-07-05 23:03:47'),
(209, 10, 'plastic-cards-australia-combo-tag-5.jpg', 'plastic-cards-australia-combo-tag-5.jpg', 0, 0, '2019-07-05 23:03:47'),
(210, 10, 'plastic-cards-australia-combo-tag-11.jpg', 'plastic-cards-australia-combo-tag-11.jpg', 0, 0, '2019-07-05 23:03:47'),
(211, 10, 'plastic-cards-australia-combo-tag-7.jpg', 'plastic-cards-australia-combo-tag-7.jpg', 0, 0, '2019-07-05 23:03:48'),
(212, 10, 'plastic-cards-australia-combo-tag-13.jpg', 'plastic-cards-australia-combo-tag-13.jpg', 0, 0, '2019-07-05 23:03:48'),
(213, 10, 'plastic-cards-australia-combo-tag-12.jpg', 'plastic-cards-australia-combo-tag-12.jpg', 0, 0, '2019-07-05 23:03:48'),
(214, 10, 'plastic-cards-australia-combo-tag-14.jpg', 'plastic-cards-australia-combo-tag-14.jpg', 0, 0, '2019-07-05 23:03:48'),
(215, 10, 'plastic-cards-australia-combo-tag-9.jpg', 'plastic-cards-australia-combo-tag-9.jpg', 0, 0, '2019-07-05 23:03:48'),
(216, 10, 'plastic-cards-australia-combo-tag-8.jpg', 'plastic-cards-australia-combo-tag-8.jpg', 0, 0, '2019-07-05 23:03:48'),
(217, 10, 'plastic-cards-australia-combo-tag-1.jpg', 'plastic-cards-australia-combo-tag-1.jpg', 0, 0, '2019-07-05 23:03:48'),
(218, 10, 'plastic-cards-australia-combo-tag-10.jpg', 'plastic-cards-australia-combo-tag-10.jpg', 0, 0, '2019-07-05 23:03:48'),
(219, 10, 'plastic-cards-australia-combo-tag-15.jpg', 'plastic-cards-australia-combo-tag-15.jpg', 0, 0, '2019-07-05 23:04:01');

-- --------------------------------------------------------

--
-- Table structure for table `homepage`
--

CREATE TABLE `homepage` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `home_banner_title` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_banner_image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_banner_features` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_heading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_description` text COLLATE utf8mb4_unicode_ci,
  `home_section2_title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_section2_image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_section2_description` text COLLATE utf8mb4_unicode_ci,
  `home_testimonial_title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_testimonial_description` text COLLATE utf8mb4_unicode_ci,
  `meta_title` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homepage`
--

INSERT INTO `homepage` (`id`, `home_banner_title`, `home_banner_image`, `home_banner_features`, `home_heading`, `home_description`, `home_section2_title`, `home_section2_image`, `home_section2_description`, `home_testimonial_title`, `home_testimonial_description`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Your Plastic<br />Card Solution', '5cebc626b65621558955558.jpg', '6 Month Warranty,Free Express Delivery,Fast Turnaround,Impressive Service', 'Welcome To Custom Plastic Cards', '<p>Custom Plastic Cards has been a leading plastic card company since 2002. Custom Plastic Cards provides the highest quality plastic cards in the industry, including membership cards, ID cards, gift cards, plastic business cards and the full range of environmentally friendly cards at low prices. Our client base includes individuals and small businesses to high profile private, government and educational institutions. We can print 100 to 3,000,000 cards. The quality and appearance of our cards are sure to have a lasting impact on your customers.</p>', 'Types Of Plastic Cards', 'plastic-cards-australia-type-of-plastic-cards.png', '<p>We print the following types of plastic cards: Memberships, Identification, Gift, Discount, Loyalty, Access, Key Tags, Combo cards, Fundraising, Business, Advertising, Novelty, Mailers and More. We also have Environmentally friendly cards made from Teslin, BioPVC or Recycled PVC.</p>', 'See Why Our Clients Love Us', '<p>We received the cards today and are <span>&nbsp;extremely pleased&nbsp;</span> with the <span>&nbsp;quality</span> and <span>&nbsp;fast turnaround&nbsp;</span> of your services. We will continue to use your services for our future needs. I anticipate placing another order within the next 2 weeks. Thank you!</p>', 'Plastic business cards, plastic membership cards, discount cards &amp; membership cards printing: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'For Blank plastic cards or plastic cards printing in Australia, contact Custom Plastic Cards, the leading plastic card manufacturer in Australia. We produce the highest quality blank plastic cards, including plastic cards printing, plastic key tags, discount cards, membership cards, identification cards, access cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `homepage_boxes`
--

CREATE TABLE `homepage_boxes` (
  `hbox_id` int(11) NOT NULL,
  `hbox_title` varchar(5000) DEFAULT NULL,
  `hbox_image` varchar(1000) DEFAULT NULL,
  `hbox_order` int(11) DEFAULT NULL,
  `hbox_created_at` datetime DEFAULT NULL,
  `hbox_updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homepage_boxes`
--

INSERT INTO `homepage_boxes` (`hbox_id`, `hbox_title`, `hbox_image`, `hbox_order`, `hbox_created_at`, `hbox_updated_at`) VALUES
(1, 'Environmentally<br />Friendly Cards', 'plastic-cards-australia-environmentally-friendly-cards.jpg', 1, NULL, NULL),
(2, 'High Quality<br />Cards', 'plastic-cards-australia-high-quality-cards-1.jpg', 2, NULL, NULL),
(3, 'On Time<br />Delivery', 'plastic-cards-australia-on-time-delivery.jpg', 3, NULL, NULL),
(4, '5 Star<br />Service', 'plastic-cards-australia-5-star-service.png', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `homepage_clients`
--

CREATE TABLE `homepage_clients` (
  `clients_id` int(11) NOT NULL,
  `clients_title` varchar(5000) DEFAULT NULL,
  `clients_image` varchar(1000) DEFAULT NULL,
  `clients_order` int(11) DEFAULT NULL,
  `clients_created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `clients_updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homepage_clients`
--

INSERT INTO `homepage_clients` (`clients_id`, `clients_title`, `clients_image`, `clients_order`, `clients_created_at`, `clients_updated_at`) VALUES
(2, 'ANZ', 'plastic-cards-australia-anz.png', 1, '2019-05-24 10:28:25', '2019-05-29 04:32:02'),
(4, 'AGDD', '5cee0a0fbc1911559104015.png', 3, '2019-05-25 21:40:37', '2019-05-29 04:32:02'),
(6, 'Europcar', '5cee0a2843c361559104040.png', 5, '2019-05-25 21:41:07', '2019-05-29 04:32:02'),
(7, 'The University of Melbourne', '5cee09de8e7251559103966.png', 6, '2019-05-25 21:41:34', '2019-05-29 04:32:02'),
(8, 'Brisbane City', '5cee0ad4191991559104212.png', 4, '2019-05-29 12:30:12', '2019-05-29 04:32:02'),
(9, 'BMW', '5cee0b11991401559104273.png', 2, '2019-05-29 12:31:13', '2019-05-29 04:32:02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_22_063916_create_cms_table', 1),
(4, '2019_04_22_083335_create_options_table', 2),
(5, '2019_04_22_084347_alter_cms_table', 2),
(6, '2019_04_22_090213_alter_options_table', 3),
(7, '2019_04_22_091429_alter_options_table', 4),
(8, '2019_04_22_114853_alter_options_table_3', 5),
(9, '2019_05_21_081409_create_pages_table', 5),
(10, '2019_05_21_084554_update_pages_table', 6),
(11, '2019_05_21_093342_update_pages_table_2', 7),
(12, '2019_05_21_095829_update_pages_table_3', 8);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `googleplus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phonenumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `youtube`, `linkedin`, `googleplus`, `instagram`, `twitter`, `facebook`, `address`, `email`, `phonenumber`, `mobile`, `created_at`, `updated_at`) VALUES
(1, 'sss', 'lnk', NULL, 'sAS', NULL, NULL, 'Level 2 Riverside Quay<br>\r\n1 Southbank Boulevard<br>\r\nSouthbank, Victoria  3006', 'info@plastic-cards.com.au', '1300 651 544', '32 4234', NULL, '2019-07-01 12:58:51');

-- --------------------------------------------------------

--
-- Table structure for table `ordersamples`
--

CREATE TABLE `ordersamples` (
  `ord_id` int(11) NOT NULL,
  `ord_firstname` varchar(100) DEFAULT NULL,
  `ord_lastname` varchar(100) DEFAULT NULL,
  `ord_company` varchar(100) DEFAULT NULL,
  `ord_address1` varchar(5000) DEFAULT NULL,
  `ord_address2` varchar(5000) DEFAULT NULL,
  `ord_city` varchar(200) DEFAULT NULL,
  `ord_state` varchar(200) DEFAULT NULL,
  `ord_zip` varchar(20) DEFAULT NULL,
  `ord_country` varchar(50) DEFAULT NULL,
  `ord_email` varchar(50) DEFAULT NULL,
  `ord_phone` varchar(50) DEFAULT NULL,
  `ord_comments` text,
  `ord_how_findus` varchar(200) DEFAULT NULL,
  `ord_created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordersamples`
--

INSERT INTO `ordersamples` (`ord_id`, `ord_firstname`, `ord_lastname`, `ord_company`, `ord_address1`, `ord_address2`, `ord_city`, `ord_state`, `ord_zip`, `ord_country`, `ord_email`, `ord_phone`, `ord_comments`, `ord_how_findus`, `ord_created_at`) VALUES
(1, 'Alex Test', 'Test', 'Test Company', '1 Test St', NULL, 'Sydney', 'NSW', '2000', 'Australia', 'amaniago@clixpert.com.au', '0999888777', 'test', 'Google', '2019-06-28 06:15:31'),
(2, 'christine', 'manning', 'cccafe', 'unit 1, 63 berwickroad', NULL, 'Campbellfield', 'Victoria', '3061', 'Australia', 'christinemanning83@gmail.com', '4196835920', 'test', 'Google', '2019-06-30 18:31:23'),
(3, 'Alex Test', 'Test 2', 'Clixpert test', '1 Test Rd', NULL, 'Sydney', 'NSW', '2000', 'Australia', 'amaniago@clixpert.com.au', '0999888777', 'test', 'Email', '2019-07-02 23:45:05'),
(4, 'Alex Test 3', 'test', 'test company', '1 test street', '1 test', 'Sydney', 'NSW', '2000', 'Australia', 'amaniago@clixpert.com.au', '999888777', 'test', 'Google', '2019-07-03 23:10:57'),
(5, 'Jasminka', 'House', 'Fifi & Boots Custom Decor', '14 Macleay Circuit', NULL, 'Upper Coomera', 'Qld', '4209', 'Australia', 'jazzihouse@hotmail.com', '0400553503', 'Hi, I am after blank pvc/poly white business cards that I can sublimate on, do you supply blanks at all?\r\nThank you.\r\nRegards,\r\nJasminka', 'Google', '2019-07-10 05:03:15'),
(6, 'Benita', 'Chantharath', 'West HQ', '55 Sherbrooke Street', NULL, 'Rooty Hill', 'NSW', '2766', 'Australia', 'bchantharath@westhq.com.au', '96255500', 'Membership cards, ideally with more premium looks and finishes', 'Other', '2019-07-17 21:06:07'),
(7, 'Craig', 'Curtis', 'Leisure Concepts', 'C18, Level 2 Business Centre', '100 Overton Road', 'Williams Landing', 'Victoria', '3027', 'Australia', 'craig@leisureconcepts.com.au', '1300911441', 'Hi, could you please send me a sample of each type of card - we are wanting something high quality that we can use to send to clients who have purchased from us to invite them to be part of a loyalty program - was thinking something like a Qantas or Virgin FF card or similar that we design', 'Google', '2019-07-17 21:23:14'),
(8, 'Jeff', 'Lyons', 'St. Mary\'s', '135 Nobbs Street', NULL, 'Berserker (North Rockhampton)', 'Queensland', '4701', 'Australia', 'Jeffrey_Lyons@rok.catholic.edu.au', NULL, 'Large variety batch of Sample Cards.....Color Text........Color Text and Photo......Color 2 sided cards.....Color cards with 3D/Holographic images....in fact, all the different types, please!!. \r\n\r\nThank You.\r\n\r\nIT Manager\r\nSt. Mary\'s Catholic Primary School\r\n135 Nobbs Street\r\nNorth Rockhampton\r\nQueensland 4701\r\n\r\nThank you.', 'Yahoo', '2019-07-23 18:16:47'),
(9, 'Lucy', 'Rowe', 'NSW Health', '14 Douglas St', NULL, 'Stanmore', 'NSW', '2048', 'Australia', 'lucy.rowe@health.nsw.gov.au', '0416218828', 'Thin plastic card (ID size) to be printed with educational resources.  Must be thin and flimsy but plastic for infection control reasons.', 'Google', '2019-07-28 16:13:00'),
(10, 'tes', 'test', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-05 17:17:43'),
(11, 'a', 's', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-05 17:43:25'),
(12, 'Fariz', 'Test', 'Clixpert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-05 18:31:11'),
(13, 'Fariz', 'Test', 'Clixpert', NULL, NULL, NULL, NULL, NULL, NULL, 'fbey@clixpert.com.au', NULL, NULL, NULL, '2019-09-05 18:32:08'),
(14, 'Alex Test', 'Test', 'Test', NULL, NULL, NULL, NULL, NULL, NULL, 'test', NULL, NULL, NULL, '2019-09-05 18:41:42'),
(15, 'Alex Test', 'Test', 'Test', NULL, NULL, NULL, NULL, NULL, NULL, 'amaniago@clixpert.com.au', NULL, NULL, NULL, '2019-09-05 18:43:13'),
(16, 'Alex Test', 'Test 2', 'Test Company', NULL, NULL, NULL, NULL, NULL, NULL, 'amaniago@clixpert', NULL, NULL, NULL, '2019-09-05 18:49:14'),
(17, 'Alex Test', 'Test 2', 'Test Company', NULL, NULL, NULL, NULL, NULL, NULL, 'amaniago@clixpert', NULL, NULL, NULL, '2019-09-05 18:49:35'),
(18, 'Alex Test', 'Test 2', 'Test Company', NULL, NULL, NULL, NULL, NULL, NULL, 'amaniago@clixpert.com.au', NULL, NULL, NULL, '2019-09-05 18:50:03'),
(19, 'Kassy', 'Stabb', 'Hip Pocket WorkWear & Safety (Ballarat)', '1265 - 1267 Howitt Street', NULL, 'Wendouree', 'VIC', '3355', 'Australia', 'kassy@hippocketballarat.com.au', '53395446', NULL, 'Google', '2019-09-09 16:52:19'),
(20, 'Carl', 'Knox-Robinson', 'Mr', '92a Peninsula Road', NULL, 'Maylands', 'Western Australia', '6051', 'Australia', 'carl@spanse.info', NULL, 'keyrings, membership card and keyring/membership card combo', 'Google', '2019-10-21 16:07:46'),
(21, 'Alex Test', 'Test', 'Test Company', '1 Test Rd', NULL, 'Sydney', 'NSW', '2000', 'Australia', 'amaniago@clixpert.com.au', '0999333444', 'test please ignore', 'Google', '2019-11-05 15:52:48'),
(22, 'Alex Test', 'Test', 'Test Company', '1 Test Rd', NULL, 'Sydney', 'NSW', '2000', 'Australia', 'test@gmail.com', '0999444556', 'test', 'Google', '2019-11-05 22:27:04'),
(23, 'Alex', 'Test', 'test company', '1 Test', NULL, 'Sydney', 'NSW', '2766', 'Australia', 'amaniago@clixpert.com.au', '0999888777', 'test', 'Google', '2019-11-06 15:13:28'),
(24, 'Chris', 'Gray', 'GrowthOps', '16/9 Beaconsfield St', 'Fyshwick ACT', 'Canberra', 'ACT', '2609', 'Australia', 'chris.gray@growthops.com.au', '0482023541', 'CORN CARDS OR PLA\r\nTESLIN CARDS\r\nRECYCLED CARDS', 'Google', '2020-01-28 22:00:24'),
(25, 'Gary', 'Collis', 'Planet Amusements', '9 Lundy Cove', NULL, 'KIARA', 'WA', '6054', 'Australia', 'info@planetamusements.com.au', '+61424363962', 'Gift Cards\r\nDiscount cards', 'Friends', '2020-02-01 05:51:36');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_heading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_description` text COLLATE utf8mb4_unicode_ci,
  `page_image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `page_sort_order` int(11) NOT NULL,
  `page_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Pages, 1-Gallery Page',
  `page_parent` int(11) NOT NULL,
  `page_publish_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_heading`, `page_description`, `page_image`, `meta_title`, `meta_keywords`, `meta_description`, `page_sort_order`, `page_type`, `page_parent`, `page_publish_status`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<p>Custom Plastic Cards is a leading plastic cards manufacturer in Australia, owned by Sport Software which is a multi-national company with offices in USA, Europe and Australia. The company has been established for almost 10 years and is focused on designing and producing high quality plastic cards for our valued customers at low prices with short lead times and strict turnaround. We also supply a variety of scanners and swipe card machines.</p><h2>We work with you to produce a design that will impress you.</h2><p><strong>Your choice of colors, from black to multiple colors.</strong></p><ul class=\"listing\"><li>A professional and very impressive finish</li><li>Custom Plastic Cards presentation can include:<ul><li>Photo ID</li><li>Company logos</li><li>Your choice of text, styles and fonts</li></ul></li><li>Signature panel</li><li>Barcode</li><li>Magnetic Stripe (High coarse or Low coarse)</li><li>30, 20 or 10 mm card thickness (30 mm is the standard credit card thickness)</li><li>PVC or composite cards with laminated polish or matte finish</li><li>Print on one or both sides</li><li>Fast turnaround is our specialty</li></ul>', NULL, 'Blank plastic cards, plastic cards printing in Australia: Custom Plastic Cards, Plastic Card manufacturer', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'For Blank plastic cards or plastic cards printing in Australia, contact Custom Plastic Cards, the leading plastic card manufacturer in Australia. We produce the highest quality blank plastic cards, including plastic cards printing, plastic key tags, discount cards, membership cards, identification cards, access cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 1, 0, 0, 1, NULL, '2019-05-29 21:30:54'),
(2, 'Contact Us', NULL, NULL, 'Contact Custom Plastic Cards, Herne Hill, Victoria, Australia: suppliers of plastic cards', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Contact Custom Plastic Cards, based in Herne Hill, Victoria, Australia. We are suppliers of plastic cards, ID cards, membership cards and more. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, identification cards, access cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 2, 0, 1, 1, NULL, '2019-05-29 21:32:10'),
(3, 'Testimonials', '<p><strong>Client base includes:</strong> Australian Department of Defence, Brisbane City Council, Sanity records, Europcar, ANZ Bank, BMW, Melbourne University and many more prominent organizations, small businesses and individuals.</p>', NULL, 'Plastic card suppliers, membership card printers, loyalty card manufacturers: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'For plastic card suppliers, membership card printers or loyalty card manufacturers in Australia, contact Custom Plastic Cards. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 3, 0, 1, 1, NULL, '2019-05-29 21:32:59'),
(4, 'Generous Warranty', '<h2>Our Generous Warranty Policy</h2><p>The goal of <strong>Custom Plastic Cards (CPC)</strong> is <strong>100%</strong> customer satisfaction in regard to the quality of our product and customer service. We work hard to give you the best value for money in the industry and that is reflected in the quality and experience of our professional staff. Our pride in our work is reflected in the <strong>30</strong> day warranty, following the delivery of the cards (goods). Our warranty covers the print quality and print content of our plastic cards and covers defects and misprints in this regard.</p><p><strong>CPC</strong> does not guarantee exact color matches unless specific PMS numbers are issued and <strong>CPC</strong> confirms the color matching in writing (usually via email). Warranty is limited to the replacement of defective goods and the approval of <strong>CPC</strong> is required prior to the return of goods. Printing errors on proofs approved by the customer are the customer&#39;s responsibility. <strong>CPC</strong> does not provide a warranty for any damage caused during shipping. We guarantee to ship and invoice &iuml;&iquest;&frac12;10% of the quantity ordered by all customers in regard to plastic cards.</p><p>Prior to the return of our goods, the customer must contact CPC within the warranty period, shall <strong>CPC</strong> decide there are reasonable grounds for a return, <strong>CPC</strong> will replace the goods at <strong>CPC</strong>&#39;s expense. No refund is offered for the shipping on returns in regard to the original order (the order upon which the return is based). If <strong>CPC</strong> decides they cannot meet your requirements for the replacement of the returned goods, <strong>CPC</strong> will refund the cost of the cards and any card set up charges, there will be <strong>no re-stocking</strong> fee applied although no refund is offered for shipping. Refunds may be reduced if the goods are not returned in the original conditions and if the correct card quantity is not returned. You must obtain a return number from us via email prior to returning the goods otherwise the goods will not be accepted.</p>', NULL, 'Blank plastic cards, plastic cards printing in Australia: Custom Plastic Cards, Plastic Card manufacturer', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'For Blank plastic cards or plastic cards printing in Australia, contact Custom Plastic Cards, the leading plastic card manufacturer in Australia. We produce the highest quality blank plastic cards, including plastic cards printing, plastic key tags, discount cards, membership cards, identification cards, access cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 4, 0, 1, 1, NULL, '2019-05-29 21:34:56'),
(5, 'Services', '<p>Our goal at Custom Plastic Cards is 100% customer satisfaction by providing great customer service and a high quality product. Our friendly sales and support staff work closely with you and give expert guidance to ensure that we exceed your expectations. We use the traditional highest quality PVC cards and environmentally friendly cards including PLA or corn cards, teslin and recycled PVC cards. We have a large range of card types from credit card size cards, plastic key tags and credit card-key tag combinations or alternatively we can produce a plastic card of virtually any shape or size to suit your specific requirements, such as loyalty cards or club membership cards.</p><p>Custom Plastic Cards offers CMYK/full color printing and spot/pantone color matching and your choice of numerous card features including sequential and non-sequential numbering, bar coding, magnetic striping, scratch offs with PIN/serial numbers, embossing (raised printing) with gold or silver tipping, signature panels, metallic colored printing, foil stamping, holograms and more. We also offer well qualified design services.</p>', NULL, 'Plastic Loyalty cards, club membership cards, key tags, ID cards manufacturing and supply: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'For plastic loyalty cards, club membership cards, key tags, ID cards manufacturing and supply in Australia, contact Custom Plastic Cards. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 5, 0, 0, 1, NULL, '2019-05-29 21:35:56'),
(6, 'Plastic card Quotation', '<p>Your quote will usually arrive within minutes or call <a href=\"tel:1300%20651%20544\" title=\"1300 651 544\">1300 651 544</a> for a quote over the phone with our professional sales staff.</p>', NULL, 'Pricing for printing plastic cards, club membership cards, ID cards, security cards: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Request an online quote and pricing for printing plastic cards, club membership cards, ID cards, security cards in Australia. Custom Plastic Cards are manufacturers of the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 6, 0, 0, 1, NULL, '2019-05-29 21:38:02'),
(7, 'Gallery', NULL, NULL, 'Plastic membership cards, plastic business cards and plastic key tags: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'View examples of plastic membership cards, plastic business cards and plastic key tags, combo cards and more in Australia, from Custom Plastic Cards. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 7, 0, 0, 1, NULL, '2019-05-29 21:39:29'),
(8, 'Credit card size cards', '<p>Our credit card size cards have a vibrant, eye-catching appearance to maximize the impact on your customers. Credit cards size cards are our most popular card type and utilize high quality, long lasting PVC plastic. We also offer a selection of <a href=\"envirocards.html\">environmentally friendly cards</a> cards including certified PLA corn cards, teslin and recycled PVC material. There are numerous features available including full color and spot/pantone color printing, numbering, bar coding, magnetic striping, scratch offs, embossing, signature panels, holograms, smart cards and more.</p>', '5ce9060decb0b1558775309.jpg', 'Plastic cards printing, plastic Plastic Business Cards, plastic card suppliers: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic Plastic Business Cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'For plastic cards printing in Australia, contact Custom Plastic Cards for a wide range of plastic cards, plastic Plastic Business Cards and more. As leading plastic card suppliers, we produce the highest quality plastic cards, including plastic Plastic Business Cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 8, 1, 7, 1, NULL, '2019-07-06 05:49:53'),
(9, 'Key Tags', '<p>The popularity of key tags continues to grow with their ability to accommodate numerous applications including membership, loyalty, security, promotional programs and more. The attractive and eye-catching appearance of our key tags will impress your customers. We use teslin plastic for our key tags, it is the perfect key tag material, teslin is both <a href=\"envirocards.html\">environmental friendly</a>and ductile, it&#39;s robust nature eliminates snapping, cracking and print fading which are commonly associated with key tags.</p><p>We can also customize the size and shape of your key tag to suit your specific requirements. Numerous features can be added to the key tags including bar codes, numbers, signature panels and more.</p>', '5ce939d79ab101558788567.jpg', 'Plastic key tags, corporate branded keytags, plastic card suppliers: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic Plastic Business Cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'View examples of plastic key tags, corporate branded keytags and more from the plastic card suppliers in Australia, Custom Plastic Cards. We produce the highest quality plastic cards, including plastic Plastic Business Cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 9, 1, 7, 1, NULL, '2019-05-29 22:12:14'),
(10, 'Combo Tag', '<p>Our combo cards have an eye-catching and a dynamic appearance that will maximize the impact on your clients for your next gift, discount and loyalty project. We use the long lasting and <a href=\"envirocards.html\">environmental friendly</a> teslin plastic for our combos. Combos consist of numerous configurations with 1 credit card size card and 1 to 3 detachable key tags. We can also customize the size and configuration of the combo sets to suit your specific requirements. Numerous features can be added to the combos including bar codes, numbers, signature panels and more.</p><p>We can also customize the size and shape of your key tag to suit your specific requirements. Numerous features can be added to the key tags including bar codes, numbers, signature panels and more.</p>', '5ce93ae3529f11558788835.jpg', 'Plastic combo cards, discount cards, loyalty cards, plastic card suppliers: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic Plastic Business Cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'View examples of plastic combo cards, such as discount cards, loyalty cards and more from the plastic card suppliers in Australia, Custom Plastic Cards. We produce the highest quality plastic cards, including plastic Plastic Business Cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 10, 1, 7, 1, '2019-05-25 02:58:02', '2019-07-06 06:04:05'),
(11, 'ID Cards', '<p>The use of plastic ID cards - such as photo identification cards or access cards - is becoming increasingly common with the need for security in companies, schools and other organisations, or as part of a club membership card or loyalty card scheme.</p><p>Custom Plastic Cards can design and print a wide range of functional and attractive ID cards that can be used for identification and security access. All of our printed plastic cards come in high quality, long lasting PVC plastic and we can also offer a choice of <a href=\"./envirocards.html\">environmentally friendly cards.</a></p><p>As an established and experienced ID card printers, we can also offer numerous features for added security or identification, such as card numbering, bar coding, magnetic striping, signature panels, holograms, smart cards and more.</p>', '5cee41a93f2a11559118249.jpg', 'Id card printers, plastic identification cards, student ID cards printing: Custom Plastic Cards, Australia', 'plastic id cards printing, manufacturing identification cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic Plastic Business Cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'As ID card printers in Australia, contact Custom Plastic Cards. We can design and print a wide range of plastic identification cards, such as student ID cards, employee ID cards and other photo id cards. As leading plastic ID card suppliers, we produce the highest quality plastic cards, including access cards and plastic security cards, membership cards, security cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic ID card printing and supply now.', 11, 1, 7, 1, '2019-05-25 03:00:30', '2019-06-20 04:35:48'),
(12, 'Plastic Discount Cards', '<p>Plastic Discount cards are the modern and attractive way to represent your shop or brand in style. Any business that doesn&iuml;&iquest;&frac12;t provide these could be left behind nowadays, so impress your customers with a well-designed, high-quality and highly durable discount card to encourage their loyalty.</p><p>Custom Plastic Cards offers CMYK/full colour printing and spot/pantone colour matching as well as qualified design services. You have the choice of numerous card textures and features such as signature panels, magnetic stripes, holograms, smart cards and more, in virtually any shape or size, for an extremely impressive representation of your shop or brand.</p><p>Increase your opportunity to secure your customers&iuml;&iquest;&frac12; loyalty and gain a competitive advantage with a superior plastic discount card from Custom Plastic Cards, the industry experts.</p>', '5cee41e4d0fc31559118308.jpg', 'Discount card printers, plastic discount cards, discount cards printing: Custom Plastic Cards, Australia', 'plastic discount card printers, manufacturing discount cards, discount cards printing, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic Plastic Business Cards, discount cards, membership cards, id cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'As discount card printers in Australia, contact Custom Plastic Cards. We can design and print a wide range of plastic discount cards, such as loyal discount cards, promotional discount cards and other discount cards. As leading plastic discount card suppliers, we produce the highest quality plastic cards, including access cards and plastic security cards, membership cards, Plastic Business Cards, discount cards, security cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic ID card printing and supply now.', 12, 1, 7, 1, '2019-05-25 03:00:30', '2019-05-29 22:17:33'),
(13, 'Plastic membership Card', '<p>Plastic membership cards are the attractive and durable way of providing your members with a card that they can show with pride. Custom Plastic Cards can produce a quality and stylish plastic card that will last the test of time, in virtually any shape or size to suit your specific requirements.</p><p>Our friendly sales and support staff work closely with you and give expert guidance to ensure that we exceed both your and your members expectations. A well-designed and high-quality membership card is an extremely effective way to impress your members and encourage them to show it to other prospective members.</p><p>A high-quality club deserves a high-quality membership card, so don&iuml;&iquest;&frac12;t settle for anything other than the design and functional excellence that Custom Plastic Cards provides.</p>', '5cee41c30db511559118275.jpg', 'Plastic membership cards, membership card printers, membership card printing: Custom Plastic Cards, Australia', 'plastic membership card printers, manufacturing plastic membership cards, plastic membership card printing, membership card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic Plastic Business Cards, discount cards, membership cards, id cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'As membership card printers in Australia, contact Custom Plastic Cards. We can design and print a wide range of plastic membership cards, such as club membership cards, sports membership cards and other photo membership cards. As leading plastic membership card suppliers, we produce the highest quality plastic cards, including access cards and plastic security cards, membership cards, discount cards, Plastic Business Cards, security cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic ID card printing and supply now.', 13, 1, 7, 1, '2019-05-25 03:06:43', '2019-05-29 22:18:39'),
(14, 'Custom Cards', '<p>If want that unique card appearance to distinguish you from your competitors, our custom card service could be your best option. We can produce that custom size or shape and design that will stand out from your competitors.</p><p>Just call, email or fax us and we will quote you on exactly what you need. Our friendly sales staff can also discuss your options to ensure that we maximize the impact on your customers. We can use PVC plastic or a range of our <a href=\"\">&nbsp;environmentally friendly cards.</a></p>', '5ce966df832f21558800095.jpg', 'Custom plastic card printers, custom plastic cards, custom card printing: Custom Plastic Cards, Australia', 'custom plastic card printers, manufacturing custom plastic cards, custom plastic card printing, plastic card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic Plastic Business Cards, discount cards, membership cards, id cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'As custom plastic card printers in Australia, contact Custom Plastic Cards. We can design and print a wide range of custom plastic cards, such as plastic membership cards, plastic id cards and other plastic cards in a custom size or shape and design that will stand out from your competitors. As leading plastic membership card suppliers, we produce the highest quality plastic cards, including access cards and plastic security cards, membership cards, discount cards, Plastic Business Cards, security cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic ID card printing and supply now.', 14, 1, 7, 1, '2019-05-25 03:06:43', '2019-05-29 22:19:44'),
(15, 'Mailers', '<p>Our plastic card mailers have produced extraordinary mail out conversion rates. The unique and striking appearance of our plastic mailers commonly produce conversion rates 3 to 10 times the industry standard.</p><p>We use the durable and <a href=\"envirocards.html\">environmental friendly</a> teslin plastic for our mailers. We can also customize the size and configuration of the mailers to maximize your customer conversion. Numerous features can be added to the plastic mailers including bar codes, numbers, signature panels and more.</p>', '5ce967307c32d1558800176.jpg', 'Plastic card mailers, plastic mailer suppliers, plastic card printers: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic Plastic Business Cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'View examples of plastic card mailers from the plastic mailer suppliers and plastic card printers in Australia, Custom Plastic Cards. We produce the highest quality plastic cards, including plastic Plastic Business Cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 15, 1, 7, 1, '2019-05-25 03:06:43', '2019-06-18 00:09:16'),
(16, 'Order Samples', NULL, NULL, 'Sample plastic cards, suppliers of custom plastic cards for business: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic Plastic Business Cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Order samples of plastic cards, club membership cards, plastic key tags or ID cards in Australia from Custom Plastic Cards. We produce the highest quality plastic cards, including plastic Plastic Business Cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 16, 0, 0, 1, '2019-05-25 03:06:43', '2019-05-29 22:22:15'),
(17, 'Specifications for artwork', '<p>Please read through this whole page carefully before submitting art to reduce any delays in getting your order into production.</p><div class=\"fullWdth marbt20\"><table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" class=\"specsTable\"><thead><tr><th>Windows/Mac</th><th>File Format</th><th>DPI</th><th>Mode</th></tr></thead><tbody><tr><td><strong>Illustrator</strong> (outlined)</td><td>.ai</td><td>300+ dpi</td><td>CMYK</td></tr><tr><td><strong>Photoshop</strong> (layers)</td><td>.psd</td><td>300+ dpi</td><td>CMYK</td></tr><tr><td><strong>EPS</strong> (outlined)</td><td>.eps</td><td>300+ dpi</td><td>CMYK</td></tr><tr><td><strong>PDF</strong> (outlined)</td><td>.pdf</td><td>300+ dpi</td><td>CMYK</td></tr><tr><td><strong>Quark</strong> (collected)</td><td>.qdx</td><td><span>300+ dpi</span></td><td><span>CMYK</span></td></tr><tr><td class=\"style46\">InDesign</td><td><span>.indd</span></td><td><span>300+ dpi</span></td><td><span>CMYK</span></td></tr></tbody></table><div class=\"rightDiv\"><p>Please provide artwork as follows:</p><ul class=\"listing\"><li>Design file type in order of preference: Illustrator, eps, psd or PDF</li><li>Resolution of 300 dpi or higher</li><li>Dimension of 3 3/8&quot; x 2 1/8&quot; (add 1/8&quot; bleed)</li><li>Supply artwork as an outline</li></ul></div></div><p><strong>Fonts:</strong> Please include all printer and screen fonts used to create your artwork or convert to outlines.</p><p><strong>Images:</strong> Make sure that you include all of your placed / embedded / imported images when you send your artwork.</p><p><strong>Layers:</strong> Please do not flatten or rasterize your Photoshop files. We need them layered and editable so we can convert the files into outlines to give you high quality prints on your cards.</p><p><strong>Card Options:</strong> Please show all your options on your artwork and state as FPO (for placement only). We will remove all FPO&#39;s barcodes, mag stripes, data, etc.. prior to production.</p><p>Please make sure your artwork is final when presented to us to prevent delay of production.<br>Should you have any questions please feel free to <a href=\"contactus.html\">contact us</a>.</p>', NULL, 'Plastic business cards, printers of credit card size plastic cards: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'For plastic business cards and other credit card size plastic cards in Australia, contact Custom Plastic Cards. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 17, 0, 0, 1, '2019-05-25 03:06:43', '2019-05-29 22:23:09'),
(18, 'Templates', '<p>Here you may download templates to create your own artwork.</p><table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" class=\"tempclass\"><tbody><tr><td>Illustrator Template</td><td><a href=\"./public/frontend/images/Illustrator_template.zip\" title=\"CLICK HERE\">CLICK HERE</a></td></tr><tr><td>Photoshop Template</td><td><a href=\"./public/frontend/images/PSDtemplate.zip\" title=\"CLICK HERE\">CLICK HERE</a></td></tr></tbody></table><p>You may also <a href=\"http://www.adobe.com/downloads/\" target=\"_blank\" title=\"download TRIAL versions of Photoshop and Illustrator\">click here</a> to download TRIAL versions of Photoshop and Illustrator</p>', NULL, 'Printed plastic cards, plastic card design and manufacturing: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'For printed plastic cards, plastic card design and manufacturing in Australia, contact Custom Plastic Cards. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 18, 0, 17, 1, '2019-05-25 03:11:43', '2019-05-30 02:16:53'),
(19, 'Design Services', '<p>For only <strong>$65</strong> our talented designers can design for you a card that will not only promote your event or company but will impress you and your clients.</p><p>All you have to do is give us details of the design you have in mind and we will be happy to get started.</p>', NULL, 'Plastic card design, printing, manufacturing: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'For plastic card design, printing and manufacturing in Australia, contact Custom Plastic Cards. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 19, 0, 17, 1, '2019-05-25 03:11:43', '2019-05-29 22:28:31'),
(20, 'Submit Order', '<h2 class=\"yu\">IMMEDIATE QUOTATION REQUEST</h2><p>If you do not have the resources to produce your card design our talented and experienced designers can produce an attractive design for you. Please describe what you would like on the front and back of the card in the comments box above and upload any images or logos that you would like to use.We will get to work on your proof and have it to you the same day, if not, the next day in most cases.</p>', NULL, 'Pricing for printing plastic cards, club membership cards, ID cards, security cards: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Request an online quote and pricing for printing plastic cards, club membership cards, ID cards, security cards in Australia. Custom Plastic Cards are manufacturers of the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 20, 0, 17, 1, '2019-05-25 03:11:43', '2019-05-29 22:35:58'),
(21, 'FAQs', '<p><strong>Q: How do we place a quotation request?</strong><br>A: Simply <a href=\"./quote.htm\">click here</a> and enter your quotation request, it takes very little time.</p><p><strong>Q: Is there a minimum card order?</strong><br>A: Yes, you can order 100 to 10 million cards.</p><p><strong>Q: Do I get to view a proof of the card before production?</strong><br>A: Yes, you will view a proof of the front and back of your card with the specifications. You must approve this proof before card production commences..</p><p><strong>Q: This is my first card order, do you offer advice regarding my card design and the features that I will require?</strong><br>A: Yes, we work with you to create an impressive card design that has the appearance and contains the features that best suits the theme and objectives that you are looking to express.</p><p><strong>Q: If we are happy with the quotation, how do we get started?</strong><br>A: Simply email or call us on our toll free number and we will get the process started immediately.</p>', NULL, 'Plastic cards, plastic key tags, custom membership cards, ID cards: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Order plastic cards, plastic key tags, custom membership cards, ID cards and more in Australia from Custom Plastic Cards. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 21, 0, 0, 1, '2019-05-25 03:11:43', '2019-06-18 00:03:06'),
(22, 'Thank You', '<p>Thank you very much for your enquiry, a staff member will contact you shortly with the relevant details.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards -      		Low Prices, Fast Turnaround', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Contact Custom Plastic Cards, based in Herne Hill, Victoria, Australia. We are suppliers of plastic cards, ID cards, membership cards and more. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, identification cards, access cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 22, 0, 0, 1, '2019-05-27 05:40:30', '2019-05-29 22:32:28'),
(23, 'Error 404', NULL, NULL, 'Plastic business cards, plastic membership cards, discount cards &amp; membership cards printing: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'As plastic business cards printers we can design and print plastic membership cards, discount cards, and provide all types of membership card printing in Australia. Custom Plastic Cards are a leading plastic cards company, producing the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards - such as teslin cards - at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 23, 0, 0, 1, '2019-05-28 08:10:02', '2019-05-29 22:37:01'),
(24, 'Business Card', '<p>Plastic business cards are the long-lasting, tastefully designed way to promote your business in style and keep it in your customers&iuml;&iquest;&frac12; mind for many years to come. Custom Plastic Cards can design and print a wide range of functional and attractive business cards. Our talented designers can produce a plastic business card design for you with an eye catching appearance to maximize the impact your business has upon your customers.</p><p>Plastic business cards are very popular nowadays and utilize high quality, long lasting PVC plastic or environmentally friendly materials including certified PLA corn cards, teslin and recycled PVC.</p><p>As a leading plastic business card manufacturer in Australia, Custom Plastic Cards provide a professional style and very impressive finish that can include photo ID, company logos and your choice of text, styles and fonts.</p>', 'plastic-cards-australia-business-card.jpg', 'Business card printers, plastic business cards, business cards printing: Custom Plastic Cards, Australia', 'plastic business card printers, manufacturing business cards, business cards printing, business card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, id cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'As business card printers in Australia, contact Custom Plastic Cards. We can design and print a wide range of plastic business cards, such as small business cards, corporate business cards and other plastic business cards. As leading plastic business card suppliers, we produce the highest quality plastic cards, including government business cards and educational institution business cards, membership cards, discount cards, security cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic business card printing and supply now.', 9, 1, 7, 1, '2019-05-29 10:13:04', '2019-07-06 05:56:25');
INSERT INTO `pages` (`id`, `page_heading`, `page_description`, `page_image`, `meta_title`, `meta_keywords`, `meta_description`, `page_sort_order`, `page_type`, `page_parent`, `page_publish_status`, `created_at`, `updated_at`) VALUES
(25, 'Environmentally Friendly Cards', '<p>Environmentally friendly cards are fast becoming the ultimate choice for environmentally conscious organizations and individuals. Environmental cards are produced from renewable resources and recycled material, all of which significantly reduces the usage of the limited natural resources on earth, unlike the traditional petroleum base PVC cards. Our suite of environmentally friendly cards includes PLA or corn cards, teslin plastic and recycled PVC cards.</p><hr><p><img src=\"https://plastic-cards.com.au/public/mediauploads/f9c07f6384804bfee522025c32d78455.jpg\" class=\"fr-fic fr-dii\" title=\"CORN CARDS OR PLA\" alt=\"CORN CARDS OR PLA\"></p><h2>CORN CARDS OR PLA</h2><p>Corn or PLA (poly lactic acid) cards are extracted from corn and are an excellent alternative for environmentally conscious organizations and individuals. Aside from the renewable nature of corn, corn cards require far less energy in production and produce less green house gas than the common petroleum based alternative of PVC. Corn cards are biodegrable and produce no toxins when destroyed during incineration, the ground and atmospheric based environmental advantages of corn cards are significant. The durability, life span and printing of the corn cards is very similar to PVC cards</p><hr><p><img src=\"https://plastic-cards.com.au/public/mediauploads/57a534d10ec945225242613241cd8261.jpg\" class=\"fr-fic fr-dii\" title=\"TESLIN CARDS\" alt=\"TESLIN CARDS\"></p><h2>TESLIN CARDS</h2><p>Teslin plastic cards are a non-petroleum based synthetic paper product. No harmful toxins are produced during the production of the teslin cards, unlike their petroleum based counterparts. Teslin cards are printed off-set with superior color consistency and are less susceptible to breakage and snapping due to their ductile/elastic properties. Some additional environmental features include: Non-toxicity, no ozone depleting constituents, no cellulose-based material (no forest harvesting), incinerates in an atmosphere of excess oxygen to yield water, carbon dioxide, energy and clean ash (from silica filler).</p><hr><p><img src=\"https://plastic-cards.com.au/public/mediauploads/bff85d7ece224000585a608743bb503b.jpg\" class=\"fr-fic fr-dii\" title=\"RECYCLED CARDS\" alt=\"RECYCLED CARDS\"></p><h2>RECYCLED CARDS</h2><p>Petroleum based PVC cards are the traditional material for plastic cards. Our recycled cards are produced from recycled PVC. Recycled PVC reduces the amount of PVC that is disposed and produced which reduces the usage of our limited natural resources.</p>', NULL, 'Environmental plastic cards, teslin cards, corn cards, recycled PVC card suppliers: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic Plastic Business Cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'View examples of environmental plastic cards, such as teslin cards, corn cards (PVA cards) and recycled PVC cards from the plastic card suppliers in Australia, Custom Plastic Cards. We produce the highest quality plastic cards, including plastic Plastic Business Cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 25, 0, 0, 1, '2019-05-30 04:56:33', '2019-07-02 19:14:02'),
(26, 'Sitemap', '<ul class=\"sitemap\"><li class=\"home\"><a href=\"/\" title=\"Home\">Home</a></li><li class=\"menu-item-has-children\"><a href=\"about-us.html\" title=\"About Us\">About Us</a><ul class=\"sub-menu\"><li><a href=\"contactus.html\" title=\"Contact Us\">Contact Us</a></li><li><a href=\"testimonials.html\" title=\"Testimonials\">Testimonials</a></li><li><a href=\"warranty.html\" title=\"Generous Warranty\">Generous Warranty</a></li></ul></li><li><a href=\"services.html\" title=\"Services\">Services</a></li><li><a href=\"quote.htm\" title=\"Quote\">Quote</a></li><li class=\"menu-item-has-children\"><a href=\"gallery.html\" title=\"Gallery\">Gallery</a><ul class=\"sub-menu\"><li><a href=\"ccardsize.html\" title=\"Credit Card Size\">Credit Card Size</a></li><li><a href=\"keytags.html\" title=\"Key Tags\">Key Tags</a></li><li><a href=\"businesscards.html\" title=\"Business Cards\">Business Cards</a></li><li><a href=\"combos.html\" title=\"Combo Cards\">Combo Cards</a></li><li><a href=\"idcards.html\" title=\"ID Cards\">ID Cards</a></li><li><a href=\"discountcards.html\" title=\"Discount Cards\">Discount Cards</a></li><li><a href=\"membershipcards.html\" title=\"Membership Cards\">Membership Cards</a></li><li><a href=\"customcards.html\" title=\"Custom Cards\">Custom Cards</a></li><li><a href=\"mailers.html\" title=\"Mailers\">Mailers</a></li><li><a href=\"ordersamples.html\" title=\"Order Samples\">Order Samples</a></li></ul></li><li class=\"menu-item-has-children\"><a href=\"specs.htm\" title=\"Specs\">Specs</a><ul class=\"sub-menu\"><li><a href=\"templates.html\" title=\"Templates\">Templates</a></li><li><a href=\"designservices.html\" title=\"Design Services\">Design Services</a></li><li><a href=\"submission.asp\" title=\"Submit Order\">Submit Order</a></li></ul></li><li><a href=\"faqs.html\" title=\"FAQ’s\">FAQ&rsquo;s</a></li><li><a href=\"privacy.html\" title=\"Privacy Policy\">Privacy Policy</a></li></ul>', NULL, 'Plastic cards printing, discount cards, plastic loyalty card suppliers: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing ', 'For plastic cards printing, manufacturing of discount cards and plastic loyalty card suppliers in Australia, contact Custom Plastic Cards. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 26, 0, 0, 1, '2019-06-20 11:55:06', '2019-07-02 19:14:42'),
(27, 'Privacy Policy', '<p align=\"justify\">Our organization is committed to the protection of your personal information.<strong>Custom Plastic Cards</strong> will not reveal, disclose, sell, distribute, rent, license, share or pass your personal information on to any third parties without your explicit written consent via email, facsimile or mail. In all cases, your information will be held in strict confidence. The&nbsp;<strong>Custom Plastic Cards</strong> web site has no facility to automatically recognize information regarding the visitor&#39;s domain or email address whilst the visitor is browsing and not deliberately submitting information.</p><p align=\"justify\"><strong>Custom Plastic Cards</strong> will not collect any personal information from you revealing racial or ethnic origin, political opinions, religious or philosophical beliefs, details of health, disability or sexual activity or orientation. Exceptions to this include:</p><p align=\"justify\">* Where you have given express consent to&nbsp;<strong>Custom Plastic Cards</strong> to do so<br>* Where there are reasonable grounds to believe that disclosure is necessary to prevent a threat to life or health<br>* Where the use is authorized by law or reasonably necessary to enforce the law or when the information is necessary for the establishment, exercise or defense of a legal claim</p><p align=\"justify\"><strong>Custom Plastic Cards</strong> may only use your personal information or data for the purpose of responding to your communication, giving support or help related to our product/s, checking the status of the usage of our product/s and direct marketing in relation to promotional activities. Again, your details will not be given to a third party without your consent as described above. You may also notify us at any time if you would like us to destroy your personal information or if you do not wish to receive any further communication from&nbsp;<strong>Custom Plastic Cards</strong>. To do either of the latter two activities go to our Contact Us page (http://www.martialartssoftware.com/order.htm), express your option and include your name and email address.</p><p align=\"center\">If you have any concerns or questions regarding our privacy policy, please contact us toll free at&nbsp;<strong><a href=\"tel:1300651544\" title=\"1 300 651 544\">1 300 651 544</a></strong> or email<br><a href=\"mailto:security@plastic-cards.com.au\">security@</a><a href=\"mailto:security@plastic-cards.com.au\">plastic-cards.com.au</a> <a href=\"mailto:security@member-cards.com\"></a> or use the&nbsp;<a href=\"./contactus.html\">Contact Us</a> page.</p>', NULL, 'Custom Plastic Cards, Australia: manufacturing of plastic cards, ID cards, membership cards', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Custom Plastic Cards, Australia offer manufacturing of plastic cards, ID cards, membership cards and more. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, identification cards, access cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 27, 0, 0, 1, '2019-06-20 12:22:15', '2019-06-20 04:32:23');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_submission`
--

CREATE TABLE `quotation_submission` (
  `qt_id` int(11) NOT NULL,
  `qt_card_type` varchar(200) DEFAULT NULL,
  `qt_no_of_cards` int(11) DEFAULT NULL,
  `qt_features` varchar(1000) DEFAULT NULL,
  `qt_cardthickness` varchar(50) NOT NULL,
  `qt_no_of_colors_front` varchar(20) DEFAULT NULL,
  `qt_no_of_colors_back` varchar(20) DEFAULT NULL,
  `qt_magnetic_stripe` varchar(20) DEFAULT NULL,
  `qt_date_card` varchar(50) DEFAULT NULL,
  `qt_fullfillment` varchar(5) DEFAULT NULL,
  `qt_how_findus` varchar(500) DEFAULT NULL,
  `qt_firstname` varchar(200) DEFAULT NULL,
  `qt_lastname` varchar(200) DEFAULT NULL,
  `qt_email` varchar(50) DEFAULT NULL,
  `qt_phone` varchar(50) DEFAULT NULL,
  `qt_comments` varchar(5000) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quotation_submission`
--

INSERT INTO `quotation_submission` (`qt_id`, `qt_card_type`, `qt_no_of_cards`, `qt_features`, `qt_cardthickness`, `qt_no_of_colors_front`, `qt_no_of_colors_back`, `qt_magnetic_stripe`, `qt_date_card`, `qt_fullfillment`, `qt_how_findus`, `qt_firstname`, `qt_lastname`, `qt_email`, `qt_phone`, `qt_comments`, `created_at`) VALUES
(1, 'Security card', 1, 'Personalization', '0.76 mm thick', '1 Color', '2 Colors', 'Low Coercivity', '30-06-2019', NULL, 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999887766', 'Test', '2019-06-28 06:09:04'),
(2, 'Advertising card', 100, '', '0.76 mm thick', '1 Color', '3 Colors', 'Hi Coercivity', NULL, NULL, NULL, 'Hesham', 'Mansour', 'info@member-cards.com', '8668452083', NULL, '2019-06-30 17:55:18'),
(3, 'Environmentally friendly - Teslin', 120, 'Embossing (raised print)', '0.76 mm thick', '4 Colors', '4 Colors', 'Hi Coercivity', '12-07-2019', NULL, 'google', 'stephanie', 'raphael', 'stephmelanie@gmail.com', '1165882200', 'test', '2019-06-30 17:59:55'),
(4, 'Loyalty card', 33, 'Personalization', '0.28 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '26-07-2019', NULL, 'google', 'stephanie', 'raphael', 'stephmelanie@gmail.com', '4665582', 'test', '2019-06-30 18:03:40'),
(5, 'Identification card', 1, '', '0.76 mm thick', '1 Color', '3 Colors', 'Hi Coercivity', '31-07-2019', NULL, 'Google', 'Alex Test', '0999888777', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-06-30 18:04:15'),
(6, 'Fundraising card', 30, 'Barcode', '0.58 mm thick', '4 Colors', '4 Colors', 'Hi Coercivity', '26-07-2019', NULL, 'google', 'christine', 'manning', 'christinemanning83@gmail.com', '491570157', 'test', '2019-06-30 18:10:28'),
(7, 'Environmentally friendly - Recycled PVC', 50, 'Numbering', '0.28 mm thick', '3 Colors', '3 Colors', 'Low Coercivity', '01-08-2019', NULL, 'google', 'stephanie', 'raphael', 'stephmelanie@gmail.com', '644955876', 'test', '2019-06-30 18:11:44'),
(8, 'Novelty card', 80, 'Personalization', '0.58 mm thick', '4 Colors', 'Black Only', 'Low Coercivity', '09-08-2019', NULL, 'facebook', 'stephanie', 'raphael', 'stephmelanie@gmail.com', '697448203', 'test', '2019-06-30 18:14:15'),
(9, 'Student card', 75, 'Signature Panel', '0.76 mm thick', 'Black Only', '4 Colors', 'Hi Coercivity', '15-08-2019', NULL, 'facebook', 'stephanie', 'raphael', 'steph_melanie@yahoo.com', '2667593008', 'test', '2019-06-30 18:15:40'),
(10, 'Other card type', 1, 'Barcode', '0.76 mm thick', '4 Colors', '4 Colors', 'Low Coercivity', '31-07-2019', NULL, 'google', 'stephanie', 'raphael', 'stephmelanie@gmail.com', '223885449', 'test', '2019-06-30 18:20:01'),
(11, 'Business card', 1000, 'Numbering', '0.58 mm thick', '4 Colors', '2 Colors', 'Low Coercivity', '16-08-2019', NULL, 'google', 'stephanie', 'raphael', 'steph_melanie@yahoo.com', '44966388772', 'test', '2019-06-30 18:21:24'),
(12, 'Identification card', 170, 'Scratch Off', '0.38 mm thick', '2 Colors', '3 Colors', 'Hi Coercivity', '31-07-2019', NULL, 'facebook', 'stephanie', 'raphael', 'stephmelanie@gmail.com', '335574523', 'test', '2019-06-30 18:26:54'),
(13, 'Discount card', 750, 'Barcode', '0.76 mm thick', '4 Colors', '4 Colors', 'Hi Coercivity', '23-08-2019', NULL, 'google', 'stephanie', 'raphael', 'stephmelanie@gmail.com', '996334587', 'TEST', '2019-06-30 18:44:02'),
(14, 'Security card', 120, 'Numbering', '0.58 mm thick', '3 Colors', '3 Colors', 'Hi Coercivity', '26-07-2019', NULL, 'facebook', 'stephanie', 'raphael', 'stephmelanie@gmail.com', '4658896325', 'test', '2019-06-30 18:51:16'),
(15, 'Novelty card', 50, 'Personalization', '0.38 mm thick', '2 Colors', '2 Colors', 'Low Coercivity', '26-07-2019', NULL, 'facebook', 'christine', 'manning', 'christinemanning83@gmail.com', '465879225', 'test', '2019-06-30 18:52:44'),
(16, 'Advertising card', 100, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '01-07-2019', NULL, 'Google', 'Clixpert', 'Test', 'qacheck17@gmail.com', '000000000', 'Testing, kindly ignore this.', '2019-06-30 23:10:43'),
(17, 'Business card', 1, 'Barcode', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '31-07-2019', NULL, 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-06-30 23:45:30'),
(18, 'Business card', 344, '', '0.76 mm thick', 'Black Only', '1 Color', 'Hi Coercivity', NULL, NULL, NULL, 'Hesham', 'Mansour', 'info@member-cards.com', '8668452083', NULL, '2019-07-01 04:31:52'),
(19, 'Advertising card', 2500, 'Scratch Off', '0.76 mm thick', '2 Colors', 'None', 'None', '01-08-2019', NULL, 'Google', 'Renee', 'Kushnir', 'renee@australianopaldirect.com', '3102280271', NULL, '2019-07-01 18:10:37'),
(20, 'Loyalty card', 50, 'Numbering', '0.76 mm thick', '3 Colors', '2 Colors', 'None', NULL, NULL, 'Google', 'Jami', 'Stein', 'Jamidianee@hotmail.com', '0449123739', NULL, '2019-07-01 18:42:25'),
(21, 'Identification card', 10, 'Barcode', '0.76 mm thick', '1 Color', 'Black Only', 'None', NULL, NULL, NULL, 'Maddison', 'Breen', 'Accounts@mrwednesday.com.au', '0435930422', 'Need cards to access impos system for staff', '2019-07-01 19:42:54'),
(22, 'Membership Card', 100, 'Numbering', '0.76 mm thick', '4 Colors', '4 Colors', 'Low Coercivity', NULL, NULL, 'google', 'stephanie', 'test', 'stephmelanie@gmail.com', '4196835920', 'test', '2019-07-02 23:35:39'),
(23, 'Membership Card', 100, 'Barcode', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '31-07-2019', NULL, 'Google', 'Ranjan', 'Ratnam', 'rranjanratnam@gmail.com', '0481332224', 'Please do not respond just checking', '2019-07-02 23:40:50'),
(24, 'Advertising card', 1, '', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '31-07-2019', NULL, 'Google', 'Alex Test', 'Maniago', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-02 23:59:46'),
(25, 'Membership Card', 300, 'Barcode, Numbering, Signature Panel', '0.76 mm thick', '3 Colors', '3 Colors', 'Hi Coercivity', '22-07-2019', NULL, 'Web', 'Greg', 'Ollerhead', 'greg@oneworldcollection.com', '0405336727', NULL, '2019-07-03 16:56:08'),
(26, 'Gift card', 1, '', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '31-07-2019', NULL, 'Google', 'Alex', 'test', 'amaniago@clixpert.com.au', '0999888777', 'test', '2019-07-03 23:08:31'),
(27, 'Identification card', 3445, '', '0.76 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', NULL, NULL, NULL, 'Hesham', 'Mansour', 'mans446@hotmail.com', '8668452083', NULL, '2019-07-04 16:41:53'),
(28, 'Membership Card', 1, 'Barcode', '0.58 mm thick', '1 Color', '2 Colors', 'None', '30-07-2019', NULL, 'Google', 'Alex Test', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-05 16:08:07'),
(29, 'Membership Card', 100, '', '0.76 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', '24-07-2019', NULL, 'Google', 'Fariz', 'Bey', 'fbey@clixpert.com.au', '+4410415', 'This is a test, please ignore', '2019-07-07 16:28:12'),
(30, 'Membership Card', 100, 'Barcode, Scratch Off, Embossing (raised print)', '0.76 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', '27-07-2019', NULL, 'Google', 'Fariz', 'Bey', 'fbey@clixpert.com.au', '0415', 'This is test please ignore', '2019-07-07 16:31:59'),
(31, 'Membership Card', 1, 'Numbering', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '31-07-2019', NULL, 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-07 18:02:14'),
(32, 'Membership Card', 1, 'Barcode', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '31-07-2019', NULL, 'Google', 'Alex', 'Test 2', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-07 18:32:05'),
(33, 'Key tag', 500, 'Barcode', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, NULL, 'Kimberley', 'Martin', 'kimberley.martin@phorest.com', NULL, NULL, '2019-07-07 21:24:29'),
(34, 'Membership Card', 200, 'Barcode, Signature Panel', '0.76 mm thick', '1 Color', 'Black Only', 'None', NULL, NULL, NULL, 'Bernadette', 'Torresan', 'editorial@golocalmagazine.com.au', '0432277236', 'delivery to PO in Albury NSW', '2019-07-08 18:42:13'),
(35, 'Loyalty card', 500, 'Numbering', '0.76 mm thick', '1 Color', '1 Color', 'None', '20-07-2019', NULL, 'google', 'Jeannette', 'Biordi', 'jen@biordigroup.com', '0432882975', 'Please let me know which email to send design to', '2019-07-08 21:37:19'),
(36, 'Discount card', 2, 'Barcode, Numbering', '0.58 mm thick', '1 Color', '2 Colors', 'Hi Coercivity', '24-07-2019', NULL, 'Test', 'Tester', 'test', 'qacheck17@gmail.com', '000000000', 'Test mail, please ignore', '2019-07-08 22:25:59'),
(37, 'Business card', 1000, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '15-07-2019', NULL, NULL, 'Mohammad', 'Hussain', 'h.brothers2000@gmail.com', '+61402141728', NULL, '2019-07-08 23:58:09'),
(38, 'Gift card', 50, 'Numbering, Personalization', '0.76 mm thick', '3 Colors', 'Black Only', 'None', '31-07-2019', NULL, 'Google', 'Nicolle', 'Simpson', 'Nesthair@hotmail.con', '0411569075', NULL, '2019-07-09 01:56:47'),
(39, 'Membership Card', 1, 'Barcode', '0.58 mm thick', 'Black Only', 'Black Only', 'None', '31-07-2019', NULL, 'Google', 'Alex Test', 'Tea 1', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-09 05:34:10'),
(40, 'Security card', 1000, '', '0.58 mm thick', '2 Colors', 'None', 'None', '01-09-2019', NULL, 'Google', 'Morgan', 'Schwertfeger', 'morgan.schwertfeger@outlook.com', '+61438166111', 'We would require 10 different types of design printed, some only 10 - 50, and others might be in batches of 100 - 200', '2019-07-09 18:08:52'),
(41, 'Membership Card', 1, 'Numbering', '0.76 mm thick', '1 Color', '2 Colors', 'Hi Coercivity', '31-07-2019', NULL, 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-09 18:34:35'),
(42, 'Gift card', 500, '', '0.76 mm thick', '4 Colors', '2 Colors', 'None', '15-07-2019', NULL, 'google', 'Ruby', 'Choi', 'rubieyh@gmail.com', NULL, 'can we please get a quotation for all the number of colours (we haven\'t decided on the design yet so it may vary)', '2019-07-10 00:31:00'),
(43, 'Key tag', 250, '', '0.76 mm thick', '3 Colors', 'None', 'None', NULL, NULL, 'Google', 'Jo', 'Hammond', 'jo-anne@jazzhomes.com.au', NULL, 'Will require 2 lots of 250 key tags (2 designs).', '2019-07-10 20:24:35'),
(44, 'Gift card', 50, 'Barcode', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '31-07-2019', NULL, 'google', 'Natalie', 'kerst', 'nataliekondy@y7mail.com', '0403013457', NULL, '2019-07-10 22:52:47'),
(45, 'Identification card', 200, 'Personalization', '0.76 mm thick', '2 Colors', 'None', 'None', NULL, NULL, 'Google', 'Kaya', NULL, 'sponsorship@saskinsociety.com', NULL, 'Looking for key card plastic cards to put a logo on + have a punched hole to attach to keys', '2019-07-11 18:07:31'),
(46, 'Gift card', 100, '', '0.76 mm thick', '1 Color', '2 Colors', 'None', '05-08-2019', NULL, 'Google', 'Evan', 'Vicary', 'ecvicary@gmail.com', '0407531239', 'Can I get a unique barcode & security code printed on the back of each card?', '2019-07-11 18:17:44'),
(47, 'Gift card', 100, 'Barcode', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '29-07-2019', NULL, 'google', 'Travis', 'Flens', 'theleggingslady59@gmail.com', NULL, NULL, '2019-07-11 21:29:38'),
(48, 'Membership Card', 250, '', '0.76 mm thick', '3 Colors', '2 Colors', 'None', '31-07-2019', NULL, 'Google', 'Kim', 'Wall', 'kim.wall@zapfitness.com.au', '0432942329', NULL, '2019-07-14 18:19:13'),
(49, 'Other card type', 1000, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Victor', 'Hubanic', 'victor@lightningpress.com.au', '87687505', 'Size 95mm x 70mm\r\nRound Corner\r\nLanyard Slot', '2019-07-15 16:13:28'),
(50, 'Discount card', 1000, '', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, 'google', 'Rachael', 'Ziccone', 'rachael@corporatechallenge.com.au', '0422265633', NULL, '2019-07-15 18:43:56'),
(51, 'Key tag', 100, 'Barcode', '0.58 mm thick', 'None', 'None', 'None', NULL, NULL, NULL, 'Jason', 'Ho', 'jason.ho@pb.com', '94573353', 'Hi I am after some barcoded keytags with our company logo printed on the other side.\r\n\r\nCan you please provide a quote with options?', '2019-07-16 20:23:43'),
(52, 'Membership Card', 200, 'Barcode', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '31-07-2019', NULL, 'google', 'Beauty', 'brunskills', 'beautyatbrunskills@gmail.com', '99684119', NULL, '2019-07-16 20:53:10'),
(53, 'Membership Card', 50, 'Barcode, Numbering', '0.76 mm thick', '4 Colors', '3 Colors', 'None', NULL, NULL, NULL, 'Samantha', 'Collins', 'croweatersnc@hotmail.com', NULL, NULL, '2019-07-17 01:44:57'),
(54, 'Combo Card', 250, 'Numbering', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, 'google', 'Megan', 'Niven', 'goverealestate@bigpond.com', '0408735841', NULL, '2019-07-17 22:40:00'),
(55, 'Membership Card', 500, '', '0.76 mm thick', 'None', 'None', 'None', NULL, NULL, NULL, 'peter', 'smith', 'petersmith@bigpond.com', NULL, 'Just blank cards. Thank you', '2019-07-17 23:33:17'),
(56, 'Gift card', 10, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'HADIL', 'MECHREF', 'hadiil.97@hotmail.com', '0404950485', 'the colours are black and white', '2019-07-18 16:41:57'),
(57, 'Membership Card', 100, 'Numbering', '0.76 mm thick', '3 Colors', 'Black Only', 'None', '29-08-2019', NULL, 'Google', 'Rhiannan', 'James', 'rhiannanj08@gmail.com', '+61438393963', 'Hi I would like a quote please what I need is the following.\r\n\r\n100 plastic membership cards (total)\r\n\r\nI will provide the artwork but I need 50 x of one artwork and 50x of a second art work ( season pass and pit pass) \r\nI need each of these cards numbered 1 to 50. Can you do this without me providing 50 different copies of the artwork?\r\n\r\nWould like standard size ( credit card size?) \r\nA quote for both one sided print and both sides printed.\r\n\r\nI will also need 100 lanyards and 100 clear plastic holders that they will fit in if you sell these aswell?\r\n\r\nLook forward to hearing from you.\r\nRegards Rhiannan James', '2019-07-20 16:17:39'),
(58, 'Business card', 3000, '', '0.58 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Matt', NULL, 'mattw@businessmilestones.com.au', NULL, '6 kinds x 500 cards (3000 cards total). Ongoing work due to company we were dealing with going into admin. Also option for 0.76mm thickness.', '2019-07-21 21:57:21'),
(59, 'Membership Card', 1000, 'Barcode', '0.76 mm thick', '2 Colors', '3 Colors', 'None', '01-08-2019', NULL, 'Google', 'Celia', 'Falato', 'c.falato@mosman.nsw.gov.au', '0419784013', '‘Membership Card’\r\n-	Standard PVC\r\n-	X1000 cards\r\n-	Standard credit card size\r\n-	Barcode on back \r\n\r\nHave a pre-prepared design for both sides.', '2019-07-23 17:52:38'),
(60, 'Key tag', 40, '', '0.76 mm thick', '3 Colors', '1 Color', 'None', '30-09-2019', NULL, NULL, 'Jordan', 'Kearney', 'jordan@kearneymedia.com.au', NULL, 'Just a simple membership key tag for a cricket club. The order will be split as we have two different membership offerings. All that will change is the card colour from red to blue.', '2019-07-24 17:57:51'),
(61, 'Novelty card', 500, 'Personalization', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '31-07-2019', NULL, 'Google', 'Peter', 'May', 'pm@jmr.com.au', '0418177704', 'These are business cards that a client of ours has requested be printed on plastic.', '2019-07-24 18:03:57'),
(62, 'Membership Card', 200, 'Numbering', '0.76 mm thick', '3 Colors', '2 Colors', 'Hi Coercivity', '12-08-2019', NULL, 'Google', 'Carmel', 'Smithers', 'carmel@jrmhospitality.com.au', NULL, NULL, '2019-07-24 23:48:39'),
(63, 'Discount card', 100, 'Numbering', '0.76 mm thick', '3 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Mel', NULL, 'crashadmin@rosenthals.com.au', NULL, NULL, '2019-07-25 18:25:54'),
(64, 'Gift card', 200, 'Barcode', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '15-08-2019', NULL, 'Google', 'Krizy', 'See', 'krizysee@gmail.com', '0426267187', 'Really simple design\r\n\r\nFront: Black Card with white text logo\r\nBack: Text: Slogan + Location & Website with the barcode on it.', '2019-07-25 23:39:21'),
(65, 'Identification card', 100, 'Personalization', '0.76 mm thick', '4 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Betul', 'Kuyruk', 'betul.kuyruk@rmit.edu.au', NULL, 'Hi,\r\nI\'m hoping to print plastic cards in:\r\n- Full Colour\r\n- Standard credit card size (85.60mm × 53.98mm)\r\n- Quantity: 100\r\n\r\nThey will be double-sided. The back side needs to be able to be written on with regular pen, is this possible?\r\n\r\nThanks,\r\nBetul Kuyruk\r\nRMIT property Services', '2019-07-28 16:39:23'),
(66, 'Membership Card', 2000, 'Barcode', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'David', 'Hammond', 'davehammond@me.com', '61437270363', NULL, '2019-07-29 21:54:58'),
(67, 'Gift card', 250, 'Scratch Off', '0.38 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Kitty', 'Tsang', 'kitty@ilsau.com.au', '0299361123', NULL, '2019-07-29 22:43:01'),
(68, 'Loyalty card', 400, 'Barcode', '0.76 mm thick', '3 Colors', '3 Colors', 'None', NULL, NULL, 'Google Ad', 'Jesse', 'Hare', 'jesse@harvesthotels.com.au', '0400255326', NULL, '2019-07-29 23:49:16'),
(69, 'Key tag', 5000, '', '0.76 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', '01-09-2019', NULL, 'Google', 'Travers', 'O\'Rafferty', 'torafferty@mandoonestate.com.au', '0862790500', 'just wanted individual barcodes on the back - no mag stripe.', '2019-07-30 20:10:59'),
(70, 'Gift card', 250, '', '0.76 mm thick', '2 Colors', 'Black Only', 'Hi Coercivity', '09-08-2019', NULL, 'google', 'kathy', 'bertolami', 'bertolami@iinet.net.au', '+61400510120', 'We are having our Grand Opening on the 11th Aug so ideally would be great to have cards by then\r\nI need them to integrate with Square', '2019-07-31 01:10:39'),
(71, 'Membership Card', 1000, '', '0.76 mm thick', '3 Colors', '3 Colors', 'Low Coercivity', NULL, NULL, 'Google', 'Brendan', 'Norman', 'brendan@themix.com.au', '0417071436', NULL, '2019-07-31 20:33:38'),
(72, 'Key tag', 250, 'Numbering', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Giovanni', 'Mauro', 'jw.mauro@yahoo.it', '0478953002', 'I have my own design.', '2019-08-01 16:08:39'),
(73, 'Gift card', 50, '', '0.58 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Kelly', 'Hamilton', 'hello@babyandme3d.com.au', '0417847965', NULL, '2019-08-01 17:17:44'),
(74, 'Loyalty card', 8000, 'Barcode, Numbering', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '31-08-2019', NULL, 'search engine', 'Kate', 'Wilson', 'kate@wilsonsfruit.com.au', '0438846989', NULL, '2019-08-01 20:49:51'),
(75, 'Discount card', 50, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, 'google', 'James', 'Boyne', 'jamesstaylucky@gmail.com', '0427029561', NULL, '2019-08-02 01:53:56'),
(76, 'Identification card', 50, '', '0.76 mm thick', '4 Colors', 'None', 'None', '03-09-2019', NULL, 'Google', 'Ellen', 'Kazias', 'ellen@transitgraphics.com.au', '+61262491655', NULL, '2019-08-04 16:21:57'),
(77, 'Key tag', 1000, 'Numbering', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, NULL, 'Scott', 'McCardel', 'membership.tslsc@gmail.com', '0439305688', NULL, '2019-08-04 17:46:47'),
(78, 'Discount card', 200, 'Barcode', '0.76 mm thick', 'Black Only', '3 Colors', 'None', NULL, NULL, NULL, 'Ben', 'Chiu', 'ben@aptmnt.com', '0401411020', NULL, '2019-08-04 18:23:21'),
(79, 'Loyalty card', 200, 'Numbering, Signature Panel', '0.76 mm thick', '1 Color', 'None', 'None', '20-08-2019', NULL, 'google', 'Vicki', 'Morgan', 'vicki@nationalcollegeofdance.com', '+61249529294', NULL, '2019-08-04 21:26:34'),
(80, 'Identification card', 10, '', '0.76 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Sid', 'Baker', 'sayedbaker@yahoo.com', NULL, NULL, '2019-08-04 21:54:34'),
(81, 'Membership Card', 10000, 'Barcode, Numbering, Personalization', '0.76 mm thick', '3 Colors', '1 Color', 'None', '25-08-2019', NULL, 'Google', 'Megan', 'Hannon', 'Megan.Hannon@darebin.vic.gov.au', '1300655355', NULL, '2019-08-04 22:15:26'),
(82, 'Key tag', 250, 'Personalization', '0.76 mm thick', '3 Colors', '1 Color', 'None', NULL, NULL, 'Google', 'Emma', 'Tubb', 'Emmatu14@gmail.com', '0410944826', 'Membership key tags for a cricket club', '2019-08-05 18:56:22'),
(83, 'Business card', 500, '', '0.28 mm thick', '2 Colors', 'Black Only', 'None', '12-08-2019', NULL, 'google', 'Lerina', 'Lam', 'hello@supercarsydney.com.au', '0406520524', NULL, '2019-08-05 19:41:47'),
(84, 'Gift card', 10000, '', '0.58 mm thick', '2 Colors', 'Black Only', 'None', '10-09-2019', NULL, 'google', 'Penny', 'Kee', 'penny@mobility.com.au', '0466152561', 'Can you pl quote for a cardboard backing page for the card to be affixed to as well.\r\n\r\nThanks\r\nPenny', '2019-08-05 22:00:59'),
(85, 'Key tag', 250, '', '0.76 mm thick', 'Black Only', 'None', 'None', NULL, NULL, 'google', 'Peter', 'Rudge', 'prudge@hindmarsh.vic.gov.au', '03 5391 4444', NULL, '2019-08-05 22:49:26'),
(86, 'Gift card', 1000, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Ashleigh', 'O\'Connor', 'ashleigh.oconnor@cbre.com.au', NULL, 'Can you please provide quote for 500, 1000 & 1500 cards as number of cards is not yet finalised.', '2019-08-06 17:15:02'),
(87, 'Key tag', 200, 'Personalization', '0.58 mm thick', '1 Color', 'None', 'None', '19-08-2019', NULL, 'bing', 'Jena', 'White', 'jenawhite11@icloud.com', '0419999620', 'I am just after a simple tag with our Logo on the front.', '2019-08-07 20:21:49'),
(88, 'Gift card', 250, 'Barcode', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Nick', 'McNair', 'nickmcnair98@gmail.com', NULL, NULL, '2019-08-08 00:01:37'),
(89, 'Gift card', 200, 'Barcode', '0.76 mm thick', '3 Colors', '1 Color', 'None', NULL, NULL, NULL, 'Grant', 'Muller', 'mullergoose@hotmail.com', '0431309388', NULL, '2019-08-08 23:53:27'),
(90, 'Business card', 1000, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, 'google', 'leo', 'katsidis', 'katsidis@live.com.au', NULL, NULL, '2019-08-11 19:43:45'),
(91, 'Gift card', 500, 'Barcode', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, 'Google', 'Jamie', NULL, 'jamie@portdayspa.com.au', NULL, NULL, '2019-08-11 22:07:59'),
(92, 'Membership Card', 100, 'Numbering', '0.76 mm thick', 'Black Only', 'None', 'None', '31-08-2019', NULL, 'Internet', 'Pamela', 'Dart', 'Treaslhcc@gmail.co', NULL, NULL, '2019-08-11 22:59:48'),
(93, 'Business card', 100, '', '0.76 mm thick', '3 Colors', '3 Colors', 'None', NULL, NULL, NULL, 'Hamish', 'Thomson', 'hamish.thomson@locatrix.com', NULL, NULL, '2019-08-11 23:09:14'),
(94, 'Key tag', 1000, '', '0.76 mm thick', '3 Colors', 'Black Only', 'None', '01-12-2019', NULL, 'SEARCH ENGINE', 'BROOKE', 'TSEKERIS', 'Brooke.tsekeris@gmail.com', '0433668084', NULL, '2019-08-12 16:24:11'),
(95, 'Membership Card', 500, 'Barcode', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Alec', 'Rimmer', 'cellardoor@myattsfield.com.au', NULL, NULL, '2019-08-15 19:03:15'),
(96, 'Key tag', 600, 'Numbering, Personalization', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, 'google', 'Rental', 'Domain', 'samantha.chiarelli@rentaldomain.com.au', '0732808500', NULL, '2019-08-15 21:04:47'),
(97, 'Gift card', 200, 'Numbering, Personalization', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '30-09-2019', NULL, 'Google', 'Jodie', 'Hill', 'jodie@bluebungalow.com.au', '0412350272', NULL, '2019-08-19 18:17:53'),
(98, 'Loyalty card', 2000, 'Barcode, Numbering, Signature Panel', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Justin', 'Di Caprio', 'justin_dicaprio@yahoo.com', '0487847072', NULL, '2019-08-20 16:15:33'),
(99, 'Membership Card', 500, 'Numbering', '0.76 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Jo', 'Salemi', 'inflations.balloons@gmail.com', '+61422374111', NULL, '2019-08-20 20:12:27'),
(100, 'Identification card', 50, 'Personalization', '0.76 mm thick', '3 Colors', 'Black Only', 'None', '10-09-2019', NULL, NULL, 'Jesse', NULL, 'jesse_pattinson@hotmail.com', NULL, 'Will ideally feature a photo and some graphics', '2019-08-20 21:02:57'),
(101, 'Membership Card', 4000, 'Barcode, Personalization', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '18-09-2019', NULL, 'Google', 'David', 'Tyson', 'Davidtyson@live.com.au', '0415630087', NULL, '2019-08-20 22:03:35'),
(102, 'Membership Card', 1000, 'Barcode', '0.76 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, 'google', 'Louise', 'Nahi', 'Lnahi@hotmail.com', '0412072028', NULL, '2019-08-21 00:41:39'),
(103, 'Membership Card', 150, 'Personalization', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, 'google', 'Karyn', 'Hamer', 'karyn.hamer@gmail.com', '0403537116', 'Hi, we are a carc lub looking at doing fresh membership cards for 2020.\r\n1. The cards would be printed with only the member name and member number to be individual (the artwork will say 2019 membership)\r\n2. We would need a couple of \"runs\" done throughout the year for when new members join the club (we could give them one using a pen or clear label in the interim)- this would be maybe twice a year. Can a small run be done also (likely to be maybe a dozen or so)?\r\n3. The artwork would be supplied- front and back- our run would only be say 150. Does that then come under the promo card rather than indent? How would i go about getting pricing please?', '2019-08-21 20:17:01'),
(104, 'Gift card', 200, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'None', '30-08-2019', NULL, 'google', 'Georgia', 'Bradley', 'info@peninsulabeverageco.com', '0438465638', NULL, '2019-08-21 21:59:03'),
(105, 'Gift card', 250, 'Barcode', '0.58 mm thick', '2 Colors', '2 Colors', 'None', '30-08-2019', NULL, 'Google', 'Anthony', NULL, 'info@sportsontherun.com.au', '86784993', NULL, '2019-08-21 22:52:44'),
(106, 'Membership Card', 250, 'Numbering, Signature Panel', '0.76 mm thick', '1 Color', 'None', 'None', NULL, NULL, NULL, 'Lauren', 'Whitton', 'Xposedteam@yahoo.com.au', '0432242092', NULL, '2019-08-22 20:56:20'),
(107, 'Identification card', 1, 'Barcode', '0.76 mm thick', '1 Color', 'None', 'None', NULL, NULL, NULL, 'Jaydan', NULL, 'jharvey@jttyt.onmicrosoft.com', NULL, NULL, '2019-08-24 21:46:26'),
(108, 'Gift card', 250, 'Barcode', '0.58 mm thick', '3 Colors', 'Black Only', 'None', '14-09-2019', NULL, 'google', 'Ashleigh', 'Rimmington', 'ashleigh@gympiecitygym.com.au', '0754836444', NULL, '2019-08-26 16:36:48'),
(109, 'Gift card', 250, 'Barcode', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, NULL, 'Marina', 'Tokarski', 'marina@von-routte.com', NULL, NULL, '2019-08-26 18:57:07'),
(110, 'Gift card', 100, '', '0.76 mm thick', '3 Colors', '2 Colors', 'None', NULL, NULL, 'Google', 'Eric', 'Vigo', 'eric@rebootr.io', '0431749550', 'Also quote for 250', '2019-08-26 20:35:52'),
(111, 'Advertising card', 2500, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '09-09-2019', NULL, 'google', 'Susan', 'Wilde', 'isistyreservice@outlook.com', '0458180180', NULL, '2019-08-26 21:10:43'),
(112, 'Gift card', 200, '', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', NULL, NULL, NULL, 'connie', 'anello', 'hampers@newfarmdeli.com.au', NULL, NULL, '2019-08-26 21:32:55'),
(113, 'Gift card', 100, 'Personalization', '0.76 mm thick', 'Black Only', '1 Color', 'Hi Coercivity', '30-08-2019', NULL, NULL, 'PRAWIN', 'DANIEL', 'prawin.daniel@myob.com', '+61468574323', 'Thank you for your Bigger Business lead.', '2019-08-26 23:02:31'),
(114, 'Discount card', 1000, '', '0.76 mm thick', '1 Color', 'Black Only', 'None', NULL, NULL, 'Search engine', 'Fatimah', 'Desai', 'fatimah@talesofindia.com.au', '+61425457867', NULL, '2019-08-27 21:56:07'),
(115, 'Discount card', 300, 'Numbering', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '06-09-2019', NULL, 'Google', 'Brad', 'Perks', 'brad@matrixcompanies.com.au', '0409692418', NULL, '2019-08-27 23:20:36'),
(116, 'Loyalty card', 250, 'Barcode', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, 'Google', 'Catherine', 'Disher', 'catherinejayne777@gmail.com', '0497815126', NULL, '2019-08-28 19:29:15'),
(117, 'Key tag', 100, '', '0.76 mm thick', '2 Colors', 'None', 'None', '30-09-2019', NULL, 'Google', 'Wesley', 'Suradnya', 'wesley.suradnya@gmail.com', '+61468637987', NULL, '2019-08-28 20:27:01'),
(118, 'Membership Card', 1000, 'Barcode', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '18-09-2019', NULL, 'Google', 'Michal', 'M', 'events@vgl.net.au', '0403 065 064', NULL, '2019-08-28 20:51:02'),
(119, 'Key tag', 100, '', '0.76 mm thick', '2 Colors', 'None', 'None', NULL, NULL, 'google', 'Jenni', 'Wearing-Smith', 'jenniws@reallyclever.com.au', '+61398707978', NULL, '2019-08-28 21:13:50'),
(120, 'Discount card', 200, '', '0.76 mm thick', '4 Colors', 'None', 'None', '15-08-2019', NULL, 'goog', 'Mal', 'Campbell', 'mal@iandmindustries.com.au', '0411253423', 'Can i pls have a quote for 200 plastic cards printed one side only', '2019-08-29 20:12:51'),
(121, 'Discount card', 100, 'Barcode, Numbering, Personalization', '0.76 mm thick', '3 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Yogesh', 'Dhawan', 'yogi143@outook.com', '0448 640 900', 'please call me', '2019-08-29 22:43:16'),
(122, 'Loyalty card', 500, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '22-09-2019', NULL, 'google', 'gina', 'ianosevici', 'gina@newagecaravanssydney.com.au', NULL, NULL, '2019-09-01 19:09:51'),
(123, 'Membership Card', 5000, '', '0.76 mm thick', '3 Colors', '3 Colors', 'None', '01-10-2019', NULL, 'google', 'Doreen', 'Farrugia', 'doreen@boylesgolfshed.com', NULL, NULL, '2019-09-02 00:24:02'),
(124, 'Business card', 1000, 'Numbering, Personalization, Embossing (raised print)', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '25-09-2019', NULL, 'google', 'Michael', 'Cohen', 'michael@bluepowder.com.au', '02 9907 1027', NULL, '2019-09-02 21:47:13'),
(125, 'Identification card', 10, '', '0.76 mm thick', '3 Colors', 'None', 'None', NULL, NULL, NULL, 'Efosa', 'Amegor', 'e.amegor@amondos.com.au', '0414017924', 'Could I also get a quote as to how much it would cost to only have 2 photo ID cards produced.\r\n\r\nThanks', '2019-09-02 22:51:41'),
(126, 'Discount card', 80, '', '0.76 mm thick', '4 Colors', 'None', 'None', '13-09-2019', NULL, 'google', 'Janet', 'Lin', 'janet_lin_@hotmail.com', '0402061982', NULL, '2019-09-03 09:17:15'),
(127, 'Combo Card', 150, 'Numbering', '0.76 mm thick', 'Black Only', 'None', 'None', '09-09-2019', NULL, NULL, 'Nick', 'Byrne', 'nick@bayleafcatering.com.au', '0396905831', 'Looking to get cloakroom tags made. Can we make the hole in the small card larger? What would the turn around time be? Do you have a generic style that is cheaper with no branding?', '2019-09-03 17:40:51'),
(128, 'Membership Card', 600, 'Barcode', '0.76 mm thick', '3 Colors', 'Black Only', 'None', '30-09-2019', NULL, 'Google', 'Colin', 'Frick', 'frickcolin@gmail.com', '0410790187', 'For a tennis club with logo to be included and sequential numbering.', '2019-09-03 18:51:48'),
(129, 'Novelty card', 30, '', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '18-09-2019', NULL, NULL, 'Jake', 'Depetro', 'jakedepetro01@gmail.com', '0434506539', 'Hey Guys i would like to just add a logo on front and one on the back', '2019-09-03 23:59:58'),
(130, 'Gift card', 1000, 'Numbering', '0.76 mm thick', '3 Colors', 'Black Only', 'Hi Coercivity', NULL, NULL, 'Google', 'Chris', 'Wilson', 'bdm@bowlsclayton.com.au', '0467418430', NULL, '2019-09-04 20:14:32'),
(131, 'Loyalty card', 600, '', '0.76 mm thick', '3 Colors', '3 Colors', 'None', NULL, NULL, 'google', 'deo', 'fenech', 'deo.fenech@au.harveynorman.com', '0412613730', NULL, '2019-09-04 20:38:12'),
(132, 'Key tag', 300, 'Barcode', '0.38 mm thick', '3 Colors', '1 Color', 'None', NULL, NULL, 'Google', 'Melinda', 'MacBeth', 'macbethmelinda@gmail.com', '+61411334710', 'I was after quote for keytags with barcode on the back and logo on the front for gym membership. Could you please quote on 300 & 500 please.', '2019-09-05 16:48:49'),
(133, 'Gift card', 250, 'Signature Panel', '0.76 mm thick', '3 Colors', '3 Colors', 'None', '29-09-2019', NULL, 'google', 'Sandra', 'Findlay', 's.findlay@morningsidemedispa.com.au', '0731033804', NULL, '2019-09-08 16:03:45'),
(134, 'Key tag', 1000, '', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '23-09-2019', NULL, 'google', 'Craig', 'McKim', 'craig@boxr.com.au', '0414391558', NULL, '2019-09-08 19:48:09'),
(135, 'Gift card', 250, '', '0.76 mm thick', '3 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Anthony', 'Ebery', 'anthony@perfit.com.au', '0408350086', NULL, '2019-09-08 20:43:36'),
(136, 'Gift card', 100, 'Barcode', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Andrew', 'Johnston', 'sales@cyclecentre.net.au', '0424209785', NULL, '2019-09-09 19:51:01'),
(137, 'Identification card', 50, '', '0.76 mm thick', '4 Colors', 'None', 'None', NULL, NULL, 'google', 'Jewel', 'Walls', 'jewelsplace@hotmail.com', NULL, NULL, '2019-09-09 20:32:12'),
(138, 'Membership Card', 2000, 'Signature Panel', '0.76 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', NULL, NULL, 'google', 'Cahill', 'Poulter', 'cahill@barracksonbarrack.com.au', '0414076784', NULL, '2019-09-09 21:09:06'),
(139, 'Membership Card', 500, 'Numbering', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '20-09-2019', NULL, NULL, 'Kerrie', 'Mann', 'info@bloomfieldfarm.com.au', '+61424862604', NULL, '2019-09-10 18:45:00'),
(140, 'Membership Card', 1500, '', '0.38 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Rosanna', 'Mazzucco', 'rosanna@melacreative.com.au', '0295576699', 'please quote 1500/ 3000/ 4000.\r\n\r\nwe are a design agency in Sydney. Coudl you send samples for us to show clients?', '2019-09-10 19:33:27'),
(141, 'Gift card', 50, '', '0.76 mm thick', '3 Colors', '3 Colors', 'Low Coercivity', '16-09-2019', NULL, 'google', 'Luke', 'Herbst', 'nathanherbst14@gmail.com', '0415213831', 'Please email me', '2019-09-10 19:49:37'),
(142, 'Gift card', 40, 'Numbering, Signature Panel, Scratch Off', '0.76 mm thick', '3 Colors', '3 Colors', 'Hi Coercivity', '18-09-2019', NULL, 'google', 'Luke', 'Herbst', 'nathanherbst14@gmail.com', '0415213831', 'Please email me', '2019-09-10 19:50:46'),
(143, 'Gift card', 2000, 'Barcode, Signature Panel', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Katie', 'Nguyen', 'admin@rabbitholeplaycentre.com.au', '0393178178', 'Hi Can i please get a quote, details are below\r\n\r\nQTY 2000\r\nNumber of sides Printed: PET Full Colour 2 sides	\r\nCard Thickness Plastic Card Thickness: .76mm	\r\nBarcodes Sequential: QR CODE\r\nWriting Panel Signature Strip: YES	\r\nDesign: Different Design for front and back	\r\nArtwork Options Upload - Print Ready	\r\nManufacture Time Turnaround Time on Plastic Cards (Standard)	\r\n\r\nThanks\r\nKatie\r\nRabbit Hole Playcentre', '2019-09-10 21:55:39'),
(144, 'Membership Card', 250, 'Barcode, Numbering, Personalization, Embossing (raised print)', '0.76 mm thick', '1 Color', 'Black Only', 'None', '30-09-2019', NULL, 'Customer', 'Alex', 'Dodovski', 'Offthemap4x4@outlook.com', '0477283301', NULL, '2019-09-16 02:47:33'),
(145, 'Gift card', 500, '', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'SHARON', 'SAWYER', 'sharon.sawyer@lifeline.org.au', '+61405214834', 'WE are a vend user and will be using code VEND11', '2019-09-17 16:31:52'),
(146, 'Gift card', 500, 'Barcode, Numbering', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '30-09-2019', NULL, 'google', 'Daniel', 'Jordon', 'dan@roaringstories.com.au', '0474156152', NULL, '2019-09-17 20:12:46'),
(147, 'Gift card', 1000, '', '0.76 mm thick', '4 Colors', '3 Colors', 'None', '30-09-2019', NULL, 'Google', 'Jessica', 'Anderson', 'jess.anderson@citycave.com.au', '0402831596', 'I have the design just need to print', '2019-09-17 21:11:46'),
(148, 'Key tag', 41000, '', '0.76 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Dion', 'Colefax', 'dion@loadedvisuals.com.au', '+61402646297', NULL, '2019-09-17 22:57:45'),
(149, 'Membership Card', 400, '', '0.76 mm thick', '4 Colors', 'None', 'None', NULL, NULL, NULL, 'Lawrence', 'Mago', 'lawrencemago@qantas.com.au', NULL, 'Key tags with hole punch\r\nGraphic on one side of card', '2019-09-18 20:59:59'),
(150, 'Gift card', 1000, 'Barcode, Numbering, Personalization', '0.76 mm thick', '3 Colors', '4 Colors', 'None', NULL, NULL, 'google', 'Toni-Jade', 'Lennox', 'info@beautifulbodysalon.com.au', '32840476', NULL, '2019-09-19 16:59:10'),
(151, 'Discount card', 2500, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Robert', 'Mellor', 'robert@cycloneprint.com.au', '+61732009693', 'Aso require lanyard plastic inserts for these if you are able to provide?', '2019-09-22 21:36:01'),
(152, 'Membership Card', 3000, 'Barcode, Personalization', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, 'Google', 'Joy', 'Cheng', 'joyyyy.cheng@gmail.com', '96679276', NULL, '2019-09-22 23:31:19'),
(153, 'Business card', 1000, '', '0.58 mm thick', '4 Colors', '4 Colors', 'None', '30-09-2019', NULL, 'Google', 'Maree', 'Miller', 'info@signblast.com.au', '+61753536011', 'We can supply artwork and please quote for Delivery to Caboolture 4510. Thank you Kind Regards Maree', '2019-09-23 16:41:20'),
(154, 'Loyalty card', 500, '', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, 'google', 'Caroline', 'Jardine', 'caroline@nimmitabelbakery.com.au', '+61413538837', 'Can you cost in the option to have the cards numbered. With a name panel on the back, and a barcode.', '2019-09-23 17:33:29'),
(155, 'Gift card', 100, 'Barcode', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '30-10-2019', NULL, 'Google', 'Trephina', 'Lathwell', 'contact@wickedearth.com.au', '0432680281', NULL, '2019-09-23 17:51:29'),
(156, 'Membership Card', 201, 'Numbering', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Paul', 'Blain', 'quote@presshere.com.au', '0418304651', '201 cards - 2 x 001', '2019-09-23 18:09:25'),
(157, 'Gift card', 250, '', '0.76 mm thick', '1 Color', '1 Color', 'Low Coercivity', '31-10-2019', NULL, 'google', 'marie', 'ganino', 'hairnskinboutique@gmail.com', '0398536970', 'Hi just looking for gift cards please - \r\nwe need them to be able to add the amount or service they client has purchased. this is for a hairdressing salon.', '2019-09-23 18:41:09'),
(158, 'Gift card', 10, '', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '04-10-2019', NULL, 'Google', 'Sarah', 'Speirs', 'sarah.jane.speirs@gmail.com', '0431054338', NULL, '2019-09-23 21:56:19'),
(159, 'Membership Card', 1000, '', '0.28 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Paul', 'Blain', 'quote@presshere.com.au', '0418304651', 'please quote 2000 as well\r\nwith round corners', '2019-09-25 17:26:06'),
(160, 'Key tag', 600, '', '0.76 mm thick', 'Black Only', '1 Color', 'None', '13-10-2019', NULL, 'Google', 'Trent', 'Gardner', 'trent.gardner@raywhite.com', '0404492227', NULL, '2019-09-25 20:43:45'),
(161, 'Membership Card', 1000, 'Barcode', '0.76 mm thick', '3 Colors', 'Black Only', 'None', '01-03-2020', NULL, 'web', 'Samantha', 'Arthur', 'sarthur@sacredheartmission.org', '+61432513414', NULL, '2019-09-25 20:50:06'),
(162, 'Advertising card', 5000, '', '0.76 mm thick', '4 Colors', '2 Colors', 'None', NULL, NULL, 'google ads', 'Kevin', 'Sheo', 'thechaoticfactory@gmail.com', '+61423053976', 'size 88mm * 62mm\r\nrequired 5000 qty, 1000 qty for each art work\r\neach art work has multiple colours', '2019-09-25 21:36:58'),
(163, 'Other card type', 9, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '04-10-2019', NULL, NULL, 'Andrew', 'Nguyen', 'bluepowerranger2007@gmail.com', NULL, NULL, '2019-09-25 22:00:54'),
(164, 'Membership Card', 500, 'Personalization', '0.76 mm thick', '3 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Tim', 'Beattie', 'timbeattie1@live.com', NULL, NULL, '2019-09-25 23:55:10'),
(165, 'Gift card', 50, '', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '08-11-2019', NULL, 'Google', 'Nicholas J', 'Tremaine', 'nictremaine@adam.com.au', '0407401858', NULL, '2019-09-26 19:50:44'),
(166, 'Gift card', 250, 'Numbering, Signature Panel', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, NULL, 'Ildi', 'Campbell', 'Info@performpilates.com.au', '0412996121', NULL, '2019-09-30 21:13:29'),
(167, 'Membership Card', 500, 'Numbering', '0.76 mm thick', '2 Colors', '1 Color', 'None', '27-10-2019', NULL, 'search engine', 'Francesca', 'Jorio', 'francesca.jorio@scu.edu.au', '0419375467', NULL, '2019-10-01 21:04:10'),
(168, 'Identification card', 200, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Ryan', 'Morgan', 'ryan.morgan@endemolshine.com.au', NULL, 'Double sided Graphic and personal names on each one,  with hole on all of them.', '2019-10-02 01:14:32'),
(169, 'Membership Card', 75, '', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '14-10-2019', NULL, 'web', 'Kent', 'Bird', 'kmbird@bigpond.net.au', '+61430218418', 'Looking for a small run of numbered VIP cards', '2019-10-02 16:09:24'),
(170, 'Business card', 500, 'Embossing (raised print)', '0.58 mm thick', '2 Colors', '2 Colors', 'None', '15-10-2019', NULL, 'Facebook', 'Kara', 'Grant', 'Kara@grantfinancesolutions.com.au', '0438792579', NULL, '2019-10-03 03:25:50'),
(171, 'Membership Card', 300, 'Numbering, Personalization', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '25-10-2019', NULL, 'google', 'Mary', 'Melo', 'mary@marymelographicdesign.com.au', '0404497341', 'My client is looking for a pearl type of finish on the card - is this something you\'re able to achieve?', '2019-10-03 22:58:08'),
(172, 'Loyalty card', 500, 'Barcode, Numbering', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '18-10-2019', NULL, 'Google', 'Ray', 'Long', 'ray@thebirchhouse.com.au', '0408836627', NULL, '2019-10-06 22:21:15'),
(173, 'Other card type', 100, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '31-10-2019', NULL, 'google', 'David', 'Margetson', 'admin@printintegrity.com.au', '0434830145', NULL, '2019-10-07 01:03:29'),
(174, 'Key tag', 500, 'Personalization, Embossing (raised print)', '0.76 mm thick', '4 Colors', '1 Color', 'None', NULL, NULL, 'google', 'Adie', 'Kriesl', 'office@dwmechanical.com.au', '0491 273 434', NULL, '2019-10-07 01:48:40'),
(175, 'Gift card', 250, '', '0.76 mm thick', '4 Colors', '1 Color', 'Hi Coercivity', '04-11-2019', NULL, NULL, 'Craig', 'Maria', 'craig@australianpianowarehouse.com.au', '0402145361', NULL, '2019-10-07 22:42:15'),
(176, 'Membership Card', 201, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '17-10-2019', NULL, NULL, 'Paul', 'Blain', 'quote@presshere.com.au', '0418304651', '2 x 001 cards', '2019-10-08 15:15:18'),
(177, 'Gift card', 500, 'Barcode, Personalization', '0.76 mm thick', '1 Color', 'Black Only', 'None', '28-10-2019', NULL, 'google', 'Johnny', 'Zuccarelli', 'johnnyzuccarelli@outlook.com', '0457980007', 'Hi\r\nI need a quote on 500 cards\r\nGift cards with logo and bar-code on back \r\nPrinted on credit card type and size card\r\nMany thanks\r\nJohnny', '2019-10-08 17:55:17'),
(178, 'Identification card', 100, 'Personalization', '0.76 mm thick', '3 Colors', 'None', 'None', NULL, NULL, NULL, 'Narelle', 'Munari', 'narelle.munari@rail.bombardier.com', '0882032200', 'These cards are for office use only.  To be customised with staff members photo on.', '2019-10-08 19:57:17'),
(179, 'Membership Card', 300, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '03-02-2020', NULL, 'google', 'Perry', 'Kleppe', 'perry@gr8corp.com.au', '0411 117086', 'Willetton Football Club', '2019-10-08 21:06:42'),
(180, 'Membership Card', 100, '', '0.76 mm thick', '3 Colors', '1 Color', 'None', '02-12-2019', NULL, 'Google!', 'EIRINI', 'Tzortzis', 'eirinitzortzis@hotmail.com', '0409238099', NULL, '2019-10-09 00:24:53'),
(181, 'Membership Card', 2000, 'Numbering, Signature Panel, Embossing (raised print)', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '22-11-2019', NULL, 'google', 'ELIZABETH', 'BRENNAN', 'liz@glasshouse.org.au', '0265818521', NULL, '2019-10-09 15:14:49'),
(182, 'Combo Card', 500, 'Signature Panel', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '01-11-2019', NULL, 'Google', 'Daniel', 'Wyld', 'daniel.wyld@globalmicro.com.au', '0283389063', 'Hi,\r\n\r\nI need the primary card to have a back face that can be written on.\r\nFront Face and both faces of the KeyTag can be glossy.\r\n\r\nKeyTag Front - 2 Colours\r\nKeyTag Rear - 2 Colours\r\n\r\nCard Front - 4 Colours\r\nCard Rear - 4 Colours', '2019-10-10 20:39:59'),
(183, 'Gift card', 1000, 'Barcode', '0.76 mm thick', '1 Color', 'None', 'None', NULL, NULL, NULL, 'Jose', 'Caceres', 'jose.c-r@hotmail.com', '0421794070', 'Hello,\r\nDesign will be provided in PDF format.\r\nColours will be White, Black & Blue (#159dd9).', '2019-10-10 21:43:55'),
(184, 'Gift card', 250, 'Barcode, Numbering, Signature Panel, Personalization', '0.76 mm thick', '2 Colors', '1 Color', 'None', '22-10-2019', NULL, NULL, 'Amanda', 'Giacc', 'therapie@optusnet.com.au', '0432674109', NULL, '2019-10-10 23:46:41'),
(185, 'Membership Card', 2000, 'Numbering, Signature Panel, Embossing (raised print)', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '25-11-2019', NULL, 'google', 'ELIZABETH', 'BRENNAN', 'liz@glasshouse.org.au', NULL, NULL, '2019-10-13 20:43:01'),
(186, 'Gift card', 50, 'Numbering', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, NULL, 'Stephanie', 'Wishart', 'design@cleanseandco.com.au', NULL, 'matte stock please and quote for 100 if more for money.', '2019-10-14 18:02:26'),
(187, 'Membership Card', 250, 'Barcode', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '05-11-2019', NULL, 'Google', 'Mark', 'Paohler', 'mark@completecolour.com.au', NULL, 'Please send a price list if you have one .\r\n\r\nWe are A1 Paper printers and get heaps of requests for these cards.', '2019-10-14 19:50:35'),
(188, 'Membership Card', 5000, 'Embossing (raised print)', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '31-10-2019', NULL, 'google', 'Cathy', 'Stacey', 'cathystacey@terrace.qld.edu.au', '07 3214 5439', NULL, '2019-10-14 21:55:12'),
(189, 'Business card', 50, 'Personalization', '0.76 mm thick', '3 Colors', '3 Colors', 'None', '29-10-2019', NULL, 'Google', 'Chantelle', 'Adamson', 'chantelle.adamson@rby.net.au', '0732456933', 'Afternoon, \r\nI\'m Chantelle from RBY Projects.\r\nI\'m trying to get some motivational work cards done for our employees but standard business card material would not last in the conditions they work in.\r\nCould you please quote me on some plastic business cards that would last construction environment.\r\n\r\nKind Regards,\r\nChantelle', '2019-10-14 23:47:53'),
(190, 'Identification card', 20, '', '0.76 mm thick', '2 Colors', 'None', 'None', NULL, NULL, NULL, 'Stacey', NULL, 'admin@coderedmarketing.com.au', NULL, NULL, '2019-10-15 16:48:27'),
(191, 'Membership Card', 100, 'Signature Panel', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, NULL, 'Thomas', 'Powell', 'tom@exmouthsurfcentre.com.au', '0467906091', 'Hi Guys,\r\n\r\nI\'m after a loyalty card with two areas for writing a customer name and another for a club association', '2019-10-15 17:34:02'),
(192, 'Gift card', 20, 'Barcode', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Carolina', 'Minton', 'carolina.minton@gmail.com', '0431802915', 'Hi, we are looking to get some gift cards made up for our car detailing business.', '2019-10-15 21:41:13'),
(193, 'Membership Card', 100, 'Barcode', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '01-11-2019', NULL, 'search engine', 'Rick', 'Jarosinski', 'themeredithhotel@gmail.com', '0447606556', 'For POS System', '2019-10-16 01:47:48'),
(194, 'Identification card', 40, 'Signature Panel, Personalization', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '06-11-2019', NULL, 'Bing', 'Cassi', 'Day', 'cassianna.day@mq.edu.au', '0481303509', 'Ideally would like 12.5cmx12.5cm square plastic cards to go on lanyards. Need an image, date, name of conference and space for names and pronouns.', '2019-10-16 15:20:12'),
(195, 'Membership Card', 1000, 'Barcode, Numbering', '0.76 mm thick', '1 Color', 'Black Only', 'None', NULL, NULL, 'google', 'Allan', 'Murphy', 'info@renascencehealth.com.au', '0419600655', 'Good Morning, could I please have a price for std and PLA cards, also is there any difference in price for key tags?\r\n\r\nRegards,\r\n\r\nAllan', '2019-10-17 16:02:49'),
(196, 'Novelty card', 250, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '31-10-2019', NULL, 'google', 'Mick', 'Clohesy', 'mick@academygraphics.com.au', '+61414686607', NULL, '2019-10-17 17:16:06'),
(197, 'Discount card', 100, '', '0.58 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, 'google', 'Tracy', 'Archer', 'tracydesigns@iinet.net.au', '0434061612', NULL, '2019-10-17 19:57:01'),
(198, 'Gift card', 1000, '', '0.76 mm thick', 'Black Only', 'Black Only', 'Hi Coercivity', '11-11-2019', NULL, 'google', 'Jack', NULL, 'jack.yao@brazioniseyecare.com.au', NULL, NULL, '2019-10-17 22:41:58'),
(199, 'Business card', 250, 'Embossing (raised print)', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, 'Internet', 'James', NULL, 'jameskearns82@gmail.com', NULL, NULL, '2019-10-18 02:00:05'),
(200, 'Fundraising card', 500, 'Numbering', '0.58 mm thick', '3 Colors', '1 Color', 'None', NULL, NULL, 'google', 'Echuca Printers', 'Lee', 'info@echucaprinters.com.au', '0354801307', 'We are looking for a trade supplier, can you help?\r\nRegards\r\nChris Lee', '2019-10-20 16:19:22'),
(201, 'Business card', 10000, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '28-10-2019', NULL, NULL, 'Helen', 'Trickey', 'ipsales@onthenet.com.au', '0755649950', 'we are looking for a supplier of our PVC cards for our client. We are a printing company - Integrity press', '2019-10-21 00:09:02'),
(202, 'Gift card', 200, 'Barcode, Numbering', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, NULL, 'sam', 'mn', 'm.moharaminia@gmail.com', NULL, 'need gift vouchers for massage bussiness', '2019-10-21 18:09:47');
INSERT INTO `quotation_submission` (`qt_id`, `qt_card_type`, `qt_no_of_cards`, `qt_features`, `qt_cardthickness`, `qt_no_of_colors_front`, `qt_no_of_colors_back`, `qt_magnetic_stripe`, `qt_date_card`, `qt_fullfillment`, `qt_how_findus`, `qt_firstname`, `qt_lastname`, `qt_email`, `qt_phone`, `qt_comments`, `created_at`) VALUES
(203, 'Gift card', 500, '', '0.76 mm thick', '2 Colors', '2 Colors', 'Low Coercivity', '05-11-2019', NULL, 'Google', 'Emily', 'Genova', 'paddingtonqld@f45training.com.au', '0428407575', NULL, '2019-10-22 00:36:23'),
(204, 'Gift card', 200, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Cameron', 'Black', 'cblack@tranzmile.com', '0419167179', NULL, '2019-10-22 22:21:27'),
(205, 'Membership Card', 5, 'Numbering, Personalization', '0.76 mm thick', '3 Colors', 'None', 'None', NULL, NULL, 'google', 'Chris', 'Wilson', 'mirriulah@yahoo.com.au', '0499476343', NULL, '2019-10-23 16:56:00'),
(206, 'Membership Card', 100, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, 'Google', 'Julie', 'Sampson', 'julie.sampson@telstramaroubra.com.au', '0400006343', 'Please advise on what you need to be provided in terms of design.', '2019-10-23 19:26:02'),
(207, 'Key tag', 1000, '', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, 'Google', 'Kate', 'Aynsley-Smith', 'mail.kaynsley@gmail.com', '0411708001', 'Delivery is to 4800 Qld', '2019-10-23 22:21:25'),
(208, 'Other card type', 50, 'Numbering', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '07-11-2019', NULL, 'Google', 'Amy', 'Ellery', 'amy.ellery@fultonhogan.com.au', '0431137813', 'these are reference cards which will have information our customers can keep.Mainly text.', '2019-10-24 19:20:44'),
(209, 'Loyalty card', 500, '', '0.76 mm thick', '3 Colors', '3 Colors', 'None', '01-12-2019', NULL, 'google', 'Bianca', 'Skene', 'biancas@pellicano.com.au', '0738505800', NULL, '2019-10-24 22:21:43'),
(210, 'Membership Card', 250, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Connor', NULL, 'connor@youpla.com.au', NULL, 'Full colour required on front and back (not 4 colours)', '2019-10-27 14:41:28'),
(211, 'Identification card', 6, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Dani', 'Martyn', 'dani@desireav.com.au', '0249818626', 'Hi, \r\n\r\nLooking at purchasing Staff ID cards but we would like CR100 size, is this something you can help with? \r\n\r\nThank you', '2019-10-27 20:03:07'),
(212, 'Gift card', 40, 'Personalization', '0.28 mm thick', '1 Color', '1 Color', 'None', '25-12-2019', NULL, 'Google', 'priyanca', 'Chauhan', 'priyanca.princii@gmail.com', '424223087', 'Gift cards for cafe for christmas', '2019-10-27 20:10:28'),
(213, 'Membership Card', 100, 'Numbering', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Mick', NULL, 'micklaw@gmail.com', NULL, NULL, '2019-10-27 22:48:31'),
(214, 'Gift card', 500, 'Barcode, Numbering, Personalization', '0.76 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Brad', 'Morfett', 'bmorfett@tranzmile.com', '+61733444156', 'With A6 backing cards please', '2019-10-28 15:57:25'),
(215, 'Gift card', 200, 'Barcode, Numbering', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '06-11-2019', NULL, 'Internet search', 'Jean-Daniel', 'Coulon', 'jcoulon@pgamember.org.au', '0451653178', 'Hi,\r\n\r\nWe have just opened a new business and would like to organise plastic gift cards (credit card size).\r\n\r\nHow much are they with our logo ? \r\n\r\nRegards,', '2019-10-29 15:24:24'),
(216, 'Loyalty card', 150, 'Barcode, Personalization', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, NULL, 'donatello', 'pietrantuono', 'dona147@hotmail.it', NULL, NULL, '2019-10-29 21:00:27'),
(217, 'Business card', 1000, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '08-11-2019', NULL, 'google', 'Mike', 'Ebsworth', 'mike@maritimeprojects.com.au', '0400333223', NULL, '2019-10-30 17:44:52'),
(218, 'Gift card', 20, 'Embossing (raised print)', '0.76 mm thick', '1 Color', '1 Color', 'None', '08-11-2019', NULL, 'google', 'Mia', 'Stone', 'i.am.mia.stone@icloud.com', '0353673341', 'hello, needing a quote for 20x plastic gift cards. There is no special requirements for them (ie, they don\'t need barcode or anything like that) but it does need to be in gold foil. please let me know if this is possible. Kind Regards, Mia', '2019-10-31 18:21:52'),
(219, 'Membership Card', 2000, 'Personalization', '0.76 mm thick', '3 Colors', 'Black Only', 'None', '29-11-2019', NULL, 'Google', 'Maddie', 'Robson', 'Maddie@mxdome.com.au', '+61435356471', 'Hi there, \r\nJust after a quote for cards that will have a custom QR code printed onto the back. We are also after gift cards separate to the membership cards, about 500 of those if I could also get that quote, they are the same requirements!\r\n\r\nThanks', '2019-11-03 16:11:46'),
(220, 'Gift card', 1000, 'Barcode, Numbering, Signature Panel', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '15-11-2019', NULL, 'google', 'charmainW', 'welgemoed', 'info@mcloughlinbutchers.com.au', '0432236460', 'hi we have a butchers in malaga and are wanting a quote with our logo printed in the front - can send jpeg if required.', '2019-11-04 01:38:40'),
(221, 'Discount card', 30, 'Numbering', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, NULL, 'Amanda', 'Denis', 'A.k.beauty@mail.com', NULL, NULL, '2019-11-04 16:31:52'),
(222, 'Key tag', 500, 'Personalization', '0.76 mm thick', '3 Colors', '3 Colors', 'None', NULL, NULL, 'Google', 'Ferre', 'Bynoe', 'kingscross@peterpans.com', '0405689018', NULL, '2019-11-04 17:57:07'),
(223, 'Gift card', 25, 'Barcode', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, 'Google', 'Tim', 'Moran', 'timbo.t16@gmail.com', '0447848774', NULL, '2019-11-04 19:23:42'),
(224, 'Other card type', 100, '', '0.76 mm thick', '2 Colors', '1 Color', 'None', '06-12-2019', NULL, NULL, 'Anastasia', 'Ellerby', 'anastasiaellerby@gmail.com', '0439393200', 'We are after Bag Tags credit card size for our swim club - with Swim club logo on front and swimmer initials on Back and a hole to attach card to a bag \r\n\r\nThanks', '2019-11-04 21:57:57'),
(225, 'Loyalty card', 1000, '', '0.76 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Brianna', 'Smith', 'brianna@pohlmans.com.au', '0417773752', NULL, '2019-11-04 23:07:20'),
(226, 'Identification card', 4444, '', '0.76 mm thick', 'Black Only', '2 Colors', 'None', NULL, NULL, NULL, 'Hesham', 'Mansour', 'info@member-cards.com', '8668452083', NULL, '2019-11-05 15:40:07'),
(227, 'Membership Card', 1, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '25-11-2019', NULL, 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-11-05 15:49:57'),
(228, 'Gift card', 5000, 'Barcode, Numbering, Personalization', '0.76 mm thick', '3 Colors', '2 Colors', 'None', '18-11-2019', NULL, NULL, 'Emilia', NULL, 'emilia.mircevska@signalbrands.com.au', NULL, NULL, '2019-11-05 16:15:34'),
(229, 'Key tag', 250, 'Personalization', '0.76 mm thick', '1 Color', '1 Color', 'None', NULL, NULL, NULL, 'Tayla', 'Johannesen', 'tayla@redcatadventures.com.au', NULL, NULL, '2019-11-05 19:43:03'),
(230, 'Gift card', 1, 'Barcode', '0.58 mm thick', '2 Colors', '1 Color', 'Hi Coercivity', '29-11-2019', NULL, NULL, 'Alex', 'Test', 'test@gmail.com', '0877666333', 'Test', '2019-11-05 22:26:21'),
(231, 'Business card', 445, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Hesham', 'Mansour', 'info@member-cards.com', '8668452083', NULL, '2019-11-05 22:36:19'),
(232, 'Identification card', 1, '', '0.28 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '27-11-2019', NULL, NULL, 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-11-05 23:04:27'),
(233, 'Novelty card', 1, '', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '26-11-2019', NULL, NULL, 'Alex Test', 'Test', 'test@gmail.com', '0999888777', 'Test', '2019-11-06 15:06:03'),
(234, 'Business card', 1, '', '0.58 mm thick', '1 Color', '2 Colors', 'Hi Coercivity', NULL, NULL, NULL, 'Alex', 'Test', 'test@gmail.com', '0999888333', 'Test', '2019-11-06 15:11:47'),
(235, 'Discount card', 1, '', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '28-11-2019', NULL, 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-11-06 15:16:59'),
(236, 'Business card', 555, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Hesham', 'Mansour', 'info@member-cards.com', '8668452083', NULL, '2019-11-06 17:23:06'),
(237, 'Gift card', 1, 'Barcode', '0.76 mm thick', '4 Colors', '4 Colors', 'Hi Coercivity', '30-11-2019', NULL, 'google', 'stephanie', 'test', 'stephmelanie@gmail.com', '4196835920', 'test', '2019-11-06 17:23:27'),
(238, 'Business card', 1000, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '29-11-2019', NULL, 'google', 'Sevkie', 'Del Rosso', 'operations@adzmania.com.au', '0488745832', 'I am an advertising agency\r\ni would like a quote for 1000 plastic cards for one of my clients', '2019-11-07 18:28:39'),
(239, 'Membership Card', 300, '', '0.38 mm thick', '3 Colors', 'None', 'None', '06-03-2020', NULL, NULL, 'Matthew', 'Gatehouse', 'mattgatehouse@hotmail.com', '0413283684', NULL, '2019-11-10 22:44:45'),
(240, 'Discount card', 100, 'Numbering', '0.76 mm thick', '1 Color', 'Black Only', 'None', '28-11-2019', NULL, 'Google', 'Leon', 'Goldbaum', 'leongoldbaum@gmail.com', '0425754828', NULL, '2019-11-11 15:25:14'),
(241, 'Gift card', 500, 'Barcode', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Adrian', NULL, 'amanikas@mac.com', NULL, NULL, '2019-11-11 15:50:52'),
(242, 'Membership Card', 1000, '', '0.76 mm thick', '1 Color', 'None', 'None', NULL, NULL, NULL, 'Susan', 'Hull', 'headoffice@buildpro.com.au', '03 5482 6962', 'UV Protected', '2019-11-11 18:11:27'),
(243, 'Membership Card', 1000, 'Barcode, Numbering', '0.76 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Ron', 'Hill', 'ron@lighthousecare.org.au', NULL, NULL, '2019-11-11 19:15:00'),
(244, 'Identification card', 1, 'Barcode', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '03-12-2019', NULL, 'Google', 'Sarah', 'Hynes', 'Sarah.hynes27@gmail.com', NULL, NULL, '2019-11-12 01:30:17'),
(245, 'Gift card', 500, 'Barcode', '0.76 mm thick', '3 Colors', '2 Colors', 'None', NULL, NULL, 'Google', 'Kate', 'Moore', 'Unwindcraftcafe.kate@gmail.com', '0405001224', 'Are you cards vend POS compatible', '2019-11-12 19:32:12'),
(246, 'Environmentally friendly - Corn Cards', 500, 'Barcode', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Andrew', 'Crutch', 'andrewcrutch@activefeetog.com.au', NULL, 'Do you offer gift cards adhered to cardboard backing?', '2019-11-12 21:06:33'),
(247, 'Gift card', 100, 'Barcode, Signature Panel', '0.76 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Sarah', 'Holder', 'accounts@calibrecountry.com.au', '0414291486', 'We need a writing panel, not specifically for signature', '2019-11-12 23:15:50'),
(248, 'Identification card', 1, '', '0.58 mm thick', '1 Color', '2 Colors', 'Hi Coercivity', '29-11-2019', NULL, 'Google', 'Alex', 'Maniago', 'test@gmail.com', '0999888777', 'Test', '2019-11-13 20:44:10'),
(249, 'Gift card', 250, 'Numbering, Personalization', '0.28 mm thick', '4 Colors', 'Black Only', 'None', '22-11-2019', NULL, 'Goggle', 'Hadassah', 'Jordan', 'info@frankiesstory.com.au', '0423517332', NULL, '2019-11-13 20:58:31'),
(250, 'Gift card', 100, 'Signature Panel', '0.76 mm thick', '4 Colors', '3 Colors', 'None', NULL, NULL, 'Referal', 'Katarina', 'Mihaljek', 'kmihaljek@hotmail.com', NULL, NULL, '2019-11-14 00:21:39'),
(251, 'Gift card', 500, 'Barcode, Numbering, Personalization', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, 'google', 'Amelia', NULL, 'ameliajane28@hotmail.com', '0402552397', NULL, '2019-11-14 16:16:08'),
(252, 'Membership Card', 250, '', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Phil', 'Smith', 'phil@totalphysiogroup.com.au', '0448413208', 'Looking for 100-250 (smallest amount possible) small membership cards for a discount group.', '2019-11-14 18:02:37'),
(253, 'Discount card', 200, 'Barcode, Numbering, Signature Panel', '0.76 mm thick', '3 Colors', 'Black Only', 'None', '15-12-2019', NULL, 'chrome', 'Chris', 'Gallop', 'cgallop@waldecks.com.au', '92546700', NULL, '2019-11-14 20:50:34'),
(254, 'Gift card', 200, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'None', NULL, NULL, NULL, 'Jason', NULL, 'jasonwang0608@gmail.com', NULL, NULL, '2019-11-14 22:08:05'),
(255, 'Combo Card', 150, 'Numbering, Signature Panel', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '02-12-2019', NULL, 'Google', 'Danielle', 'Berry', 'admin@nswsupraclub.org', '0430912692', NULL, '2019-11-17 03:52:56'),
(256, 'Discount card', 1000, 'Personalization', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '01-01-2020', NULL, 'Google', 'Corina', 'Smith', 'corina.smith@spartners.com.au', NULL, NULL, '2019-11-17 20:37:37'),
(257, 'Gift card', 1000, 'Barcode, Embossing (raised print)', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '01-12-2019', NULL, 'Google', 'Danielle', 'Green', 'danielle@purehomeliving.com.au', NULL, 'Are these cards able to be scanned via Barcode into Retail Express POS System?', '2019-11-18 15:35:27'),
(258, 'Gift card', 70, 'Barcode, Numbering, Personalization', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '30-11-2019', NULL, 'Google', 'Coralie', 'Loftes', 'coralieloftes@gmail.com.au', '0439658720', NULL, '2019-11-18 19:39:14'),
(259, 'Key tag', 200, 'Barcode', '0.76 mm thick', '3 Colors', '3 Colors', 'None', NULL, NULL, NULL, 'Heidi', NULL, 'design@chm.com.au', NULL, NULL, '2019-11-18 20:25:42'),
(260, 'Discount card', 50, 'Scratch Off', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, 'google', 'Rupert', 'Hackett', 'rupert@calebandbrown.com', '0413566216', NULL, '2019-11-18 21:00:57'),
(261, 'Gift card', 5, 'Personalization', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, 'Google', 'Jess', 'Jaswal', 'info@buyfruit.com.au', '07 3054 5935', NULL, '2019-11-18 21:52:25'),
(262, 'Membership Card', 250, 'Numbering', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '09-12-2019', NULL, NULL, 'Jarrod', NULL, 'president@staqld.com.au', NULL, NULL, '2019-11-19 19:18:49'),
(263, 'Membership Card', 1000, 'Personalization', '0.76 mm thick', 'None', 'None', 'None', '31-12-2019', NULL, 'Google', 'Nicole', 'Tregenza', 'nicole.tregenza@civilexvic.com.au', '0488028568', 'We want to provide an image to be printed on the front & back of cards for Soccer Club membership card', '2019-11-19 21:45:23'),
(264, 'Gift card', 1000, 'Personalization', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '02-12-2019', NULL, 'Google', 'Joshua', 'Di Girolamo', 'test@test.com', '0391931206', 'Email is joshua@brandhaus.agency, would not allow us to enter our email', '2019-11-19 23:37:30'),
(265, 'Discount card', 800, 'Signature Panel', '0.58 mm thick', '4 Colors', 'Black Only', 'None', '18-12-2019', NULL, 'Google', 'Chris', 'Reed', 'chris.reed@workfront.org.au', '0437032315', 'I am after transparent cards', '2019-11-20 17:36:42'),
(266, 'Membership Card', 500, 'Barcode, Personalization', '0.76 mm thick', '4 Colors', '1 Color', 'None', NULL, NULL, NULL, 'Sam', 'Vinci', 'sam.vinci@iinet.net.au', '0412527380', 'Hi, we require round plastic key tags with logo and barcode, can you please quote?', '2019-11-20 20:46:40'),
(267, 'Gift card', 100, 'Barcode, Numbering', '0.76 mm thick', '1 Color', 'Black Only', 'None', NULL, NULL, 'google', 'sarah', 'Birch', 'hello@willowandcohomewares.com.au', '0405630881', NULL, '2019-11-21 22:45:09'),
(268, 'Gift card', 100, 'Barcode', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, NULL, 'vishnu', 'Bhanderi', 'on16vishnu@hotmail.com', NULL, NULL, '2019-11-24 18:39:48'),
(269, 'Student card', 1, 'Personalization', '0.76 mm thick', '1 Color', '1 Color', 'None', '04-12-2019', NULL, 'google', 'Ailish', 'Sheehan', '22asheehan@mlmc.vic.edu.au', '0498400502', NULL, '2019-11-24 23:21:28'),
(270, 'Gift card', 5, 'Personalization', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '16-12-2019', NULL, NULL, 'Peta', 'Chapman', 'info@farrugiabros.com.au', '0754823315', 'Hi, were looking at getting some custom gift cards made up before christmas. Can someone please contact me about this. thank you', '2019-11-26 18:46:11'),
(271, 'Gift card', 100, 'Numbering', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '15-12-2019', NULL, 'Google', 'Tiana', 'Walker', 'sales@id-living.com.au', '0740512432', NULL, '2019-11-26 18:56:53'),
(272, 'Membership Card', 250, '', '0.28 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Sapphira', 'Nix', 'u6350856@anu.edu.au', '+61491159790', 'Hi I\'m interested in printing between 250-300 Membership cards which will have our clubs logo on the front left half, and some other smaller logos taking up the front right with text underneath each. We would like the back of the card to have text detailing phone numbers, and names. I am interested in knowing how much this would cost and process through which I will need to design the card (i.e. will I need to send in logos seperate from the design etc). Looking forward to hearing from you – preferably over email.\r\nCheers,\r\nSapphira Nix', '2019-11-26 19:29:15'),
(273, 'Other card type', 2, '', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, 'Google', 'Bradley', 'Burns', 'bradley.burns05@gmail.com', NULL, NULL, '2019-11-27 00:18:15'),
(274, 'Gift card', 50, '', '0.76 mm thick', '2 Colors', 'Black Only', 'Low Coercivity', NULL, NULL, 'Google', 'Tara', 'Burns', 'barat83@outlook.com', '+61401638166', NULL, '2019-11-27 18:55:24'),
(275, 'Membership Card', 250, 'Numbering', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '31-01-2020', NULL, 'Google search', 'Steve', 'Hugler', 'steve@giraffe.com.au', '0262429669', NULL, '2019-11-28 21:19:24'),
(276, 'Gift card', 1000, 'Numbering, Signature Panel, Embossing (raised print)', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '09-12-2019', NULL, 'Google', 'Craig', 'Langford', 'craigjohnlangford@me.com', '+61411309219', 'Hi, need some cards ASAP - what\'s the quickest turnaround? Numbers can be printed on if embissing takes longer. Many thanks, Craig.', '2019-12-01 19:17:34'),
(277, 'Key tag', 50, '', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, 'Google', 'Claudia', 'Lee', 'claudia.lee@angelajewelleryaustralia.com.au', NULL, 'Only a logo need to be printed both side (black logo)', '2019-12-01 21:51:24'),
(278, 'Key tag', 250, 'Signature Panel', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '03-02-2020', NULL, 'Google', 'Thor', 'Lund', 'tlund@bigpond.net.au', '0409591359', NULL, '2019-12-02 06:03:06'),
(279, 'Identification card', 500, 'Barcode', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '13-12-2019', NULL, 'Google', 'Shammara', 'Markotis', 'shammara.markotis@manjimup.wa.gov.au', '0897717777', NULL, '2019-12-02 20:11:09'),
(280, 'Membership Card', 500, 'Numbering', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '02-01-2020', NULL, 'Google', 'Kym', 'Farquhar', 'Secretary@coomeracrushers.com.au', '0432898321', NULL, '2019-12-02 21:18:11'),
(281, 'Membership Card', 500, 'Barcode, Numbering', '0.76 mm thick', '4 Colors', '4 Colors', 'Hi Coercivity', '02-01-2020', NULL, 'Google', 'Kym', 'Farquhar', 'Secretary@coomeracrushers.com.au', '0432898321', 'Do you also do cards that have the key tags attached?', '2019-12-02 23:22:15'),
(282, 'Gift card', 500, 'Barcode, Signature Panel, Personalization, Scratch Off', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '01-02-2020', NULL, 'google', 'Alison', NULL, 'alison.ninyett@gmail.com', NULL, NULL, '2019-12-03 00:22:26'),
(283, 'Membership Card', 250, '', '0.76 mm thick', '3 Colors', '3 Colors', 'Hi Coercivity', NULL, NULL, 'internet', 'Bindi', 'Butters', 'manager@nollamararsl.asn.au', '0404156781', NULL, '2019-12-03 01:33:30'),
(284, 'Membership Card', 250, '', '0.76 mm thick', '3 Colors', '3 Colors', 'None', NULL, NULL, NULL, 'Aaron', 'McDonald', 'aaron.nunawading@gmail.com', NULL, NULL, '2019-12-03 17:05:10'),
(285, 'Other card type', 300, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, 'Google', 'Bec', 'Bennett', 'studio@firefish.com.au', '0406 765 773', 'I am after a card that is 170mm x 100mm and printed on both sides.', '2019-12-03 19:52:41'),
(286, 'Loyalty card', 500, 'Barcode, Numbering, Personalization', '0.58 mm thick', '2 Colors', 'Black Only', 'Hi Coercivity', NULL, NULL, 'online', 'Lucy', 'Luo', 'lucy_joy_@hotmail.com', '0433332256', NULL, '2019-12-03 21:13:56'),
(287, 'Gift card', 1000, '', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '06-01-2020', NULL, 'Google', 'Vanessa', 'Gilbert', 'vanessa@valoursport.com.au', '0418436382', 'Hi there,\r\n\r\nI need cards in 4 denominations\r\n700 x $25\r\n600 x$50\r\n300 x $100\r\n300 x $250', '2019-12-04 16:45:27'),
(288, 'Membership Card', 250, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '01-01-2020', NULL, 'google', 'Cassandra', 'Hirning', 'cellardoor@mantonscreekestate.com.au', '03 5989 6264', NULL, '2019-12-04 20:03:54'),
(289, 'Membership Card', 50, 'Embossing (raised print)', '0.58 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, 'Google', 'Brad', NULL, 'brad@firstshot.com.au', NULL, NULL, '2019-12-04 22:55:44'),
(290, 'Membership Card', 1000, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'None', NULL, NULL, 'Google', 'Scott', 'Martins', 'scottmartins92@outlook.com', '0429112023', NULL, '2019-12-04 23:48:16'),
(291, 'Membership Card', 1000, 'Barcode', '0.76 mm thick', '4 Colors', '3 Colors', 'None', '15-02-2020', NULL, 'web', 'Samantha', 'Arthur', 'sarthur@sacredheartmission.org', '0432513414', NULL, '2019-12-08 19:48:18'),
(292, 'Gift card', 20, 'Numbering', '0.76 mm thick', '1 Color', '1 Color', 'None', '14-12-2019', NULL, 'Online', 'Elena', 'Shami', 'elenashami@hotmail.com', '0424295250', 'Getting a quote only.', '2019-12-08 20:56:29'),
(293, 'Gift card', 200, 'Numbering', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, 'Google', 'Jenna', 'Clarke', 'info@thehenchman.com.au', NULL, NULL, '2019-12-09 17:00:46'),
(294, 'Business card', 200, '', '0.38 mm thick', '1 Color', '1 Color', 'None', '17-01-2020', NULL, 'Facebook', 'Fabian', 'Then', 'Fabian@cee3.com.au', '0423034889', NULL, '2019-12-09 23:46:19'),
(295, 'Membership Card', 500, 'Numbering', '0.76 mm thick', '3 Colors', '1 Color', 'None', NULL, NULL, NULL, 'Leah', 'Cushion', 'design@rjbatt.com.au', '0353359888', NULL, '2019-12-11 16:42:50'),
(296, 'Membership Card', 350, 'Numbering', '0.76 mm thick', '4 Colors', '2 Colors', 'None', '16-01-2020', NULL, 'Google', 'Alicia', 'Gorman', 'aliciagorman.ag@gmail.com', '0434345423', 'Looking for 350 membership cards.\r\n2 different designs. \r\nLooking for best price possible.\r\nThanks', '2019-12-11 17:55:50'),
(297, 'Key tag', 500, 'Barcode', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Richard', 'Williams', 'richard.william.a@gmail.com', '0437104884', 'Gym Membership, two logos on the front.', '2019-12-11 18:28:32'),
(298, 'Membership Card', 100, 'Numbering', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '20-12-2019', NULL, 'Google', 'Paul', 'Kirkham', 'paulk@aminoactive.com.au', '0402235289', NULL, '2019-12-11 22:02:49'),
(299, 'Membership Card', 100, 'Numbering, Personalization', '0.76 mm thick', '3 Colors', '3 Colors', 'None', '29-02-2020', NULL, 'Google', 'Ryan', 'McCaig', 'mccaig.rd@gmail.com', '0439261974', NULL, '2019-12-12 02:33:59'),
(300, 'Gift card', 500, '', '0.76 mm thick', '1 Color', '1 Color', 'None', NULL, NULL, NULL, 'Vicky', 'Mezzapesa', 'popgamingamusements@gmail.com', '0402541221', NULL, '2019-12-12 17:50:02'),
(301, 'Student card', 200, 'Barcode, Personalization', '0.76 mm thick', '3 Colors', 'None', 'Hi Coercivity', NULL, NULL, NULL, 'Jesper', 'Nielsen', 'jesper@jespernielsenphotography.com', NULL, 'Student id cards with photo and name on it', '2019-12-12 19:40:35'),
(302, 'Key tag', 1000, 'Barcode', '0.76 mm thick', '1 Color', 'Black Only', 'None', '22-12-2019', NULL, 'Known of you from Herne Hill', 'Kathy', 'Landos', 'landos@bigpond.com', '0428843895', NULL, '2019-12-15 17:45:21'),
(303, 'Novelty card', 100, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Richard', 'Maddever', 'richard.b.maddever@gmail.com', '415873702', 'Hi, I have a four-colour design comprised mainly of typed text, double-sided, that I was hoping to have printed onto plastic cards as a gift for event attendees. The cards would ideally have a slot cut out of a short edge so that they could be affixed behind an ID badge or lanyard, but this batch does not need any personalisation. I would be seeking a minimum quantity (100?) for a once-off purchase just for this one event, but I may seek additional consignments in the future. Can you please quote a cost (incl GST) and a minimum order quantity and we\'ll go from there?\r\nThanks, Richard', '2019-12-15 17:51:09'),
(304, 'Membership Card', 180, 'Signature Panel, Personalization', '0.76 mm thick', '3 Colors', '1 Color', 'None', '31-01-2020', NULL, 'Internet', 'Z', 'T', 'zeetee66@live.com', NULL, NULL, '2019-12-15 22:43:27'),
(305, 'Identification card', 1000, '', '0.76 mm thick', '2 Colors', 'None', 'None', NULL, NULL, 'Google', 'Adam', 'Maurice', 'adam@hazsure.com.au', NULL, NULL, '2019-12-15 23:41:16'),
(306, 'Gift card', 300, 'Barcode, Numbering', '0.76 mm thick', '4 Colors', 'None', 'None', NULL, NULL, NULL, 'Anthony', 'Cramer', 'cecilwalkermelbourne@ozemail.co.au', '0396009000', NULL, '2019-12-16 19:34:56'),
(307, 'Identification card', 50, '', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '30-01-2020', NULL, NULL, 'Amber', 'Bellamy', 'amber.bellamy@rsrg.com', '0418244761', NULL, '2019-12-16 20:29:25'),
(308, 'Gift card', 100, '', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Jess', 'Kaisner', 'jessica-kaisner@hotmail.com', NULL, 'Hi,\r\nI\'m looking at ordering plastic gift cards for my lash extension business for clients to purchase.', '2019-12-17 16:27:33'),
(309, 'Gift card', 250, 'Numbering', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'HYEREE', NULL, 'hairday.official@gmail.com', NULL, NULL, '2019-12-17 20:19:00'),
(310, 'Membership Card', 100, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Saroj', NULL, 'saroj@trade-up.com.au', NULL, NULL, '2019-12-17 20:28:30'),
(311, 'Membership Card', 1000, 'Numbering', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '01-02-2020', NULL, 'google', 'Amelia', 'Braithwaite', 'ameliabraithwaite19@gmail.com', NULL, NULL, '2019-12-18 01:14:42'),
(312, 'Membership Card', 200, '', '0.76 mm thick', '3 Colors', '3 Colors', 'None', '31-01-2020', NULL, 'Google', 'Nick', 'ter-Kuile', 'Tickfordenhancedcc@gmail.com', '0468878772', 'We would like to hand engrave the name and membership number after the card is printed', '2019-12-19 00:50:24'),
(313, 'Membership Card', 1000, 'Barcode, Scratch Off', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '06-03-2020', NULL, 'Google', 'Zoe', 'Smith', 'zoe.smith@ansarada.com', '0468341567', 'I may have submitted this form twice by accident, I only require one quote.', '2019-12-19 20:38:39'),
(314, 'Loyalty card', 100, '', '0.76 mm thick', '3 Colors', 'None', 'None', '22-01-2020', NULL, 'google', 'Ashleigh', 'Freer', 'ashleighf@allpet.com.au', NULL, NULL, '2020-01-01 22:50:47'),
(315, 'Gift card', 500, 'Personalization', '0.58 mm thick', '4 Colors', '4 Colors', 'None', '07-02-2020', NULL, 'google', 'Sarah', NULL, 'sarah.mansell@delwp.vic.gov.au', NULL, 'full colour double sided 85mmx55mm recycled plastic cards', '2020-01-06 15:15:48'),
(316, 'Gift card', 250, 'Numbering', '0.76 mm thick', '3 Colors', '1 Color', 'None', NULL, NULL, NULL, 'Blake', 'Jones', 'blake.jones@moethennessy.com', '0468600399', NULL, '2020-01-06 16:29:05'),
(317, 'Membership Card', 500, 'Numbering, Personalization', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '13-01-2020', NULL, 'google', 'adrianne', 'arizabal', 'functions@beachcomberhotel.com.au', '0459428806', NULL, '2020-01-06 17:16:46'),
(318, 'Combo Card', 250, 'Barcode, Numbering, Signature Panel', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '31-01-2020', NULL, 'google', 'Jason', 'O\'Connor', 'JAson@snapeastwood.com.au', '0407742211', NULL, '2020-01-06 19:54:11'),
(319, 'Membership Card', 250, 'Signature Panel', '0.76 mm thick', '3 Colors', '1 Color', 'None', '20-01-2020', NULL, 'Internet', 'Tracey', 'Brown', 'traceybrown77@gmail.com', '0402433441', NULL, '2020-01-07 15:25:44'),
(320, 'Gift card', 200, 'Numbering', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, NULL, 'Nick', 'Luck', 'Nick@grumpystqvern.com.au', NULL, NULL, '2020-01-08 00:25:42'),
(321, 'Gift card', 200, 'Numbering', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, NULL, 'Nick', 'Luck', 'nick@grumpystavern.com.au', NULL, 'Please ignore last request. The email address is incorrect.', '2020-01-08 00:26:44'),
(322, 'Discount card', 500, 'Numbering', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Chris', 'Griswood', 'Christopher.griswood@au.harveynorman.com', '0402855487', NULL, '2020-01-13 18:56:45'),
(323, 'Student card', 25, 'Signature Panel', '0.76 mm thick', '1 Color', 'Black Only', 'None', '31-01-2020', NULL, 'google', 'Eilannin', 'Harris-Black', 'info@theatreblacks.com', '0476904495', NULL, '2020-01-14 17:32:03'),
(324, 'Key tag', 500, '', '0.58 mm thick', '2 Colors', 'Black Only', 'None', '01-01-2020', NULL, 'Google', 'Errin', 'Hellingrath', 'errin.hellingrath@raywhite.com', '49721876', NULL, '2020-01-14 18:00:48'),
(325, 'Gift card', 500, 'Barcode', '0.76 mm thick', '4 Colors', '1 Color', 'None', NULL, NULL, 'goggle ad', 'Wendy', NULL, 'wendy@okanui.com', NULL, NULL, '2020-01-15 17:20:13'),
(326, 'Loyalty card', 500, '', '0.76 mm thick', '4 Colors', '2 Colors', 'None', '31-01-2020', NULL, NULL, 'Lincoln', 'Mooney', 'lincoln.mooney@lh.com.au', NULL, NULL, '2020-01-16 20:14:26'),
(327, 'Membership Card', 250, '', '0.76 mm thick', '3 Colors', 'Black Only', 'None', '31-01-2020', NULL, 'Google', 'Alys', 'Goodwin', 'alysbfnc@gmail.com', '0451716036', NULL, '2020-01-19 16:25:49'),
(328, 'Gift card', 10, 'Barcode', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, 'Google', 'Max', 'Moore', 'Mooremax07@gmail.com', '0458916152', NULL, '2020-01-19 20:50:57'),
(329, 'Membership Card', 5, 'Numbering', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '29-01-2020', NULL, 'Google', 'Clixpert Regular', 'Maintenance Check', 'amaniago@clixpert.com.au', '0000 000 000', 'Please ignore', '2020-01-20 12:51:33'),
(330, 'Identification card', 7, 'Personalization', '0.38 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '29-01-2020', NULL, 'Googlr', 'Clixpert Regular', 'Maintenance Check', 'amaniago@clixpert.com.au', '0999888877', 'Please ignore', '2020-01-20 12:54:18'),
(331, 'Membership Card', 5, 'Signature Panel', '0.76 mm thick', '1 Color', '2 Colors', 'Hi Coercivity', '29-01-2020', NULL, 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '09998888777', 'Maintenance Check', '2020-01-20 16:32:46'),
(332, 'Identification card', 6, 'Scratch Off', '0.76 mm thick', 'Black Only', '1 Color', 'Hi Coercivity', '29-01-2020', NULL, 'Google', 'Alex', 'Test 2', 'amaniago@clixpert.com.au', '0000', 'Maintenance Check', '2020-01-20 16:35:22'),
(333, 'Key tag', 500, 'Barcode', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Phil', NULL, 'phil@burnleybrewing.com.au', NULL, NULL, '2020-01-20 16:39:59'),
(334, 'Membership Card', 2, 'Barcode, Numbering, Personalization', '0.76 mm thick', '2 Colors', '3 Colors', 'Hi Coercivity', '23-01-2020', NULL, 'Test clicpert', 'Clixpert', 'Test', 'clixpertmaintanance@gmail.com', '011022033', 'Test form', '2020-01-20 22:12:27'),
(335, 'Gift card', 23, 'Barcode, Numbering, Signature Panel', '0.76 mm thick', '2 Colors', '3 Colors', 'Low Coercivity', '08-02-2020', NULL, 'Test', 'Clixpert', 'Test', 'clixpertmaintanance@gmail.com', '011022033', 'Test', '2020-01-20 23:04:25'),
(336, 'Business card', 3, 'Barcode, Signature Panel, Personalization', '0.76 mm thick', '2 Colors', '3 Colors', 'Low Coercivity', '07-02-2020', NULL, 'Test clicpert', 'Test', 'Test', 'clixpertmaintanance@gmail.com', '011099088', NULL, '2020-01-20 23:06:10'),
(337, 'Fundraising card', 23, 'Barcode, Signature Panel, Personalization', '0.28 mm thick', '4 Colors', '2 Colors', 'None', '08-02-2020', NULL, 'Test', 'Clixpert', 'Clixpert', 'clixpertmaintanance@gmail.com', '011033044', 'Form test', '2020-01-20 23:44:21'),
(338, 'Membership Card', 500, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '15-01-2020', NULL, 'Google', 'Chris', 'Trembath', 'christrembath@gmail.com', '0411062354', 'Hey there I have a client looking for a bit of a price break down for plastic membership cards 88x54mm need delivery in around 7-10 days if possible.\r\n\r\nMany thanks', '2020-01-21 19:36:04'),
(339, 'Membership Card', 500, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '12-02-2020', NULL, 'Google', 'Lachlan', 'Henry', 'lachlanhenry@outlook.com', '0419822483', NULL, '2020-01-21 23:05:32'),
(340, 'Gift card', 1000, 'Numbering', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, NULL, NULL, 'Susannah', 'Malyon', 'susi@brickfields.com.au', '0431951014', NULL, '2020-01-22 00:57:16'),
(341, 'Membership Card', 1915, 'Numbering, Personalization', '0.76 mm thick', '4 Colors', '2 Colors', 'None', '30-01-2020', NULL, 'Google', 'Suzi', 'Ignatidis', 'admin@rtbuvicloco.com.au', '0396821122', NULL, '2020-01-22 14:39:20'),
(342, 'Gift card', 15, 'Barcode', '0.76 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', '31-01-2020', NULL, 'Google', 'Fariz', 'Bey', 'fbey@clixpert.com.au', '0415', 'Maintenance check. Clixpert', '2020-01-22 16:09:59'),
(343, 'Membership Card', 250, 'Numbering', '0.76 mm thick', '4 Colors', '1 Color', 'None', '31-01-2020', NULL, 'We search', 'STEVE', 'Janiec', 'steve@wagravityenduro.org', '0407446273', NULL, '2020-01-23 00:16:45'),
(344, 'Gift card', 1500, 'Barcode, Signature Panel, Personalization, Scratch Off', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '29-02-2020', NULL, 'Google', 'Sarah', 'OSullivan', 'sarah0@ozdesignfurniture.com.au', NULL, NULL, '2020-01-23 19:43:35'),
(345, 'Key tag', 100, 'Barcode', '0.76 mm thick', '3 Colors', 'None', 'None', '01-03-2020', NULL, 'Google', 'Shane', 'Bell', 'Shane.bell@fultonhogan.com.au', '0447933347', NULL, '2020-01-23 20:27:32'),
(346, 'Gift card', 15, 'Barcode', '0.76 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', '31-01-2020', NULL, 'Google', 'Fariz', 'Bey', 'fbey@clixpert.com.au', '0415', 'Maintenance Check. Clixpert', '2020-01-23 21:53:45'),
(347, 'Key tag', 700, 'Numbering, Personalization', '0.58 mm thick', '2 Colors', '2 Colors', 'None', NULL, NULL, NULL, 'Lawson Real Estate W', NULL, 'admin@lawsonrew.com.au', '9741 9666', NULL, '2020-01-23 23:29:39'),
(348, 'Membership Card', 250, 'Barcode, Personalization', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'kristie', 'birkbeck', 'kristie_birkbeck@hotmail.com', '0400214807', 'Hi we were wanting to get some loyalty barcode cards printed as soon as possible and shipped to us in Western Australia. Could you please provide me a quote and also estimated printing/shipping timeframe. That would be amazing! Thank you!', '2020-01-24 23:19:21'),
(349, 'Loyalty card', 5, 'Personalization', '0.76 mm thick', '3 Colors', '2 Colors', 'None', '27-02-2021', NULL, 'Google', 'Frank', 'Sgro', 'info@dishandspooncafe.com.au', '0400911463', NULL, '2020-01-26 15:13:05'),
(350, 'Membership Card', 100, '', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Cassandra', NULL, 'info@jondaryanwoolshed.com.au', NULL, NULL, '2020-01-27 17:31:47'),
(351, 'Identification card', 50, 'Signature Panel, Personalization', '0.76 mm thick', '2 Colors', 'None', 'None', NULL, NULL, NULL, 'Larissa', 'Thomas', 'larissa@papertopixels.com.au', NULL, NULL, '2020-01-27 20:10:40'),
(352, 'Membership Card', 200, '', '0.76 mm thick', 'Black Only', 'None', 'None', NULL, NULL, NULL, 'mengya', 'shen', 'sammie.shen@hotmail.com', NULL, NULL, '2020-01-27 20:15:47'),
(353, 'Other card type', 40, '', '0.28 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Bel', 'Schneider', 'belinda.schneider@hotmail.com', '0414771718', 'Can I please get a quote for;\r\n40 plastic cards with party invitation details on it.\r\n\r\nPostage is to Southport QLD 4215.\r\n\r\nHow long would delivery be?\r\n\r\nRegards,\r\nBel', '2020-01-28 07:15:11'),
(354, 'Membership Card', 200, '', '0.76 mm thick', '2 Colors', '1 Color', 'Hi Coercivity', NULL, NULL, NULL, 'Kitty', 'Mitton', 'office@sportsmensclubhotel.com.au', '0498172287', NULL, '2020-01-28 15:57:36'),
(355, 'Membership Card', 250, 'Personalization', '0.76 mm thick', '3 Colors', 'None', 'None', NULL, NULL, 'Google', 'Ajit', 'Sanghvi', 'ajit.sanghvi456@gmail.com', '0466593144', NULL, '2020-01-28 17:46:19'),
(356, 'Membership Card', 200, 'Numbering', '0.58 mm thick', '4 Colors', 'Black Only', 'None', '29-02-2020', NULL, 'Google', 'Ben', 'Coure', 'westbeachminigolf@gmail.com', '0468552359', NULL, '2020-01-28 19:40:01'),
(357, 'Loyalty card', 500, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'None', '03-02-2020', NULL, NULL, 'Bridget', 'Pern', 'bridget@hamiltonhampers.com.au', '0419599053', NULL, '2020-01-28 20:35:13'),
(358, 'Key tag', 500, '', '0.76 mm thick', '3 Colors', '3 Colors', 'None', NULL, NULL, 'google search', 'Melissa', 'Matthews', 'melissa@signstyle.net.au', '0249917338', 'Hi Guys, Just wondering if you can quote this up for me. If you have any questions please give me a call on 02 4991 7338', '2020-01-30 17:47:21'),
(359, 'Loyalty card', 2000, 'Barcode, Numbering', '0.76 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, 'google chrome', 'NARELLE', 'PEARCE', 'accounts@allthingsbrewing.com.au', '0242 574851', 'I would like a quote for 2000 plastic cards with artwork on one side, and barcode and card number on the other. Thankyou.', '2020-01-30 19:58:23'),
(360, 'Membership Card', 500, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '01-03-2020', NULL, NULL, 'andrew', 'dedes', 'dedes1@bigpond.com', '0412009721', NULL, '2020-02-02 15:54:59'),
(361, 'Membership Card', 700, 'Numbering, Signature Panel', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'Simon', 'Tran', 'exec.ukc@gmail.com', '0435640340', NULL, '2020-02-02 22:24:30'),
(362, 'Loyalty card', 500, 'Barcode', '0.76 mm thick', '3 Colors', '3 Colors', 'None', '01-01-2020', NULL, 'google', 'Brooke', 'Wuersching', 'sheabeauty@outlook.com', '0455021105', NULL, '2020-02-02 22:44:04'),
(363, 'Key tag', 250, 'Personalization', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Vanessa', 'Sullivan', 'vanessa.s@ipswich.rh.com.au', '0417167865', NULL, '2020-02-03 01:56:03'),
(364, 'Key tag', 250, 'Personalization', '0.76 mm thick', '3 Colors', 'Black Only', 'None', NULL, NULL, NULL, 'Vanessa', 'Sullivan', 'vanessa.s@ipswich.rh.com.au', '0417167865', NULL, '2020-02-03 01:56:21'),
(365, 'Membership Card', 500, 'Numbering', '0.76 mm thick', '3 Colors', '1 Color', 'None', '13-02-2020', NULL, 'Google', 'Anne-Maree', 'Ferguson', 'amfergie@outlook.com', '0439820177', NULL, '2020-02-03 15:43:46'),
(366, 'Membership Card', 500, '', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '30-03-2020', NULL, 'Google', 'David', 'Roberts', 'david@bigelephant.com.au', '0418362489', '500 cards required', '2020-02-03 19:51:57'),
(367, 'Identification card', 4, '', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, NULL, 'Benjamin', 'MacLeod', 'bmacleod@c3lanecove.com.au', '0423770296', NULL, '2020-02-04 16:15:49'),
(368, 'Business card', 50, '', '0.76 mm thick', 'None', 'None', 'None', NULL, NULL, 'google', 'Courtney', 'Tanner', 'courtney@melbournefunctionalmedicine.com.au', NULL, NULL, '2020-02-04 17:39:24'),
(369, 'Key tag', 250, '', '0.76 mm thick', '1 Color', '1 Color', 'None', NULL, NULL, NULL, 'Andrew', 'Moore', 'andrewmoore@dillmac.com', '0427349912', NULL, '2020-02-04 20:03:05'),
(370, 'Membership Card', 1500, 'Numbering', '0.76 mm thick', '4 Colors', '4 Colors', 'None', '17-02-2020', NULL, 'Google', 'Shania', NULL, 'shania.sheehan.ss@gmail.com', '0421135274', NULL, '2020-02-05 06:37:34'),
(371, 'Key tag', 250, 'Barcode', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '14-02-2020', NULL, 'google', 'james', 'fewings', 'powersupps_chinchilla@outlook.com', '0402344974', 'company logo on front barcode on back', '2020-02-05 18:14:30'),
(372, 'Key tag', 250, 'Barcode', '0.76 mm thick', '4 Colors', 'Black Only', 'None', NULL, NULL, 'Google', 'Lyn', NULL, 'admin@whitsundaygold.com', NULL, NULL, '2020-02-06 19:10:40'),
(373, 'Gift card', 250, 'Numbering', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '01-03-2020', NULL, 'google search', 'Kate', 'Arton', 'kate@alpinetradegroup.com.au', '1300252225', 'please quote on 250 gift cards. with our logo and design. \r\nThanks Kate', '2020-02-06 19:13:33'),
(374, 'Identification card', 50, '', '0.76 mm thick', 'Black Only', 'None', 'None', '20-02-2020', NULL, 'Google', 'Harrison', 'Gerhardy', 'info@wright-archive.com', '0437408550', NULL, '2020-02-06 20:31:34'),
(375, 'Business card', 250, '', '0.38 mm thick', '1 Color', 'None', 'None', NULL, NULL, NULL, 'Caitlan', 'Sim', 'caitlan.sim@gmail.com', NULL, NULL, '2020-02-06 22:26:41'),
(376, 'Membership Card', 1000, 'Personalization', '0.76 mm thick', '3 Colors', '3 Colors', 'None', NULL, NULL, NULL, 'Joe', 'Corrigan', 'joecorrigan2005@gmail.com', '0426663243', NULL, '2020-02-07 15:57:19'),
(377, 'Membership Card', 250, '', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '28-02-2020', NULL, 'google', 'Marc', 'Middleton', 'unitedtweedheadsmall@yahoo.com', NULL, NULL, '2020-02-09 17:00:08'),
(378, 'Key tag', 275, 'Personalization', '0.76 mm thick', '4 Colors', 'Black Only', 'None', '28-02-2020', NULL, 'Search', 'Colin', 'Wilson', 'info@trenthamgolf.com.au', NULL, NULL, '2020-02-09 19:52:46'),
(379, 'Membership Card', 100, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '29-02-2020', NULL, NULL, 'stephanie', 'raphael', 'stephmelanie@gmail.com', '06666878', 'test', '2020-02-12 15:54:55'),
(380, 'Loyalty card', 500, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, NULL, NULL, 'TEST', NULL, 'karen@plastic-cards.com.au', NULL, NULL, '2020-02-12 16:02:30'),
(381, 'Business card', 1000, '', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, 'Test', 'Imelda', 'Thadeus', 'imelda@plastic-cards.com.au', '0128033454', 'Test', '2020-02-12 16:04:34'),
(382, 'Membership Card', 1, '', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '24-02-2020', NULL, 'Google', 'Maintenance Check', 'Please ignore', 'amaniago@clixpert.com.au', '0000000000', 'Please ignore', '2020-02-12 17:07:41'),
(383, 'Membership Card', 100, '', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '29-02-2020', NULL, 'google', 'stephanie', 'raphael', 'stephmelanie@gmail.com', '1300651544', 'test', '2020-02-12 17:24:34'),
(384, 'Discount card', 1000, '', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, NULL, 'Test', 'Imelda', 'Thadeus', 'imelda@plastic-cards.com.au', '0128033454', 'Test', '2020-02-12 17:25:04'),
(385, 'Business card', 100, 'Barcode', '0.76 mm thick', 'Black Only', 'Black Only', 'Low Coercivity', '28-02-2020', NULL, 'facebook', 'christine', 'manning', 'christinemanning83@gmail.com', '4196835920', 'test', '2020-02-12 17:27:07'),
(386, 'Business card', 100, 'Barcode', '0.76 mm thick', '4 Colors', '4 Colors', 'Hi Coercivity', '26-02-2020', NULL, 'google', 'stephanie', 'raphael', 'steph_melanie@yahoo.com', '1300651544', 'test', '2020-02-12 17:39:10'),
(387, 'Membership Card', 12, 'Barcode, Numbering, Signature Panel', '0.58 mm thick', 'Black Only', '2 Colors', 'Low Coercivity', '07-02-2020', NULL, 'test only', 'test only', 'test only', 'clixpertmaintanance@gmail.com', '0110220330', 'test only', '2020-02-12 21:18:05');

-- --------------------------------------------------------

--
-- Table structure for table `slug`
--

CREATE TABLE `slug` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slug`
--

INSERT INTO `slug` (`id`, `slug_url`, `template`, `page_id`, `created_at`, `updated_at`) VALUES
(1, 'about-us.html', 'pages', 1, NULL, '2019-05-22 06:00:18'),
(2, 'contactus.html', 'contactus', 2, NULL, NULL),
(3, 'testimonials.html', 'testimonial', 3, NULL, NULL),
(4, 'warranty.html', 'pages', 4, NULL, NULL),
(5, 'services.html', 'pages', 5, NULL, NULL),
(6, 'quote.htm', 'quote', 6, NULL, NULL),
(7, 'gallery.html', 'gallery', 7, NULL, NULL),
(8, 'ccardsize.html', 'gallery_detail', 8, NULL, '2019-05-23 00:57:01'),
(9, 'keytags.html', 'gallery_detail', 9, NULL, NULL),
(10, 'combos.html', 'gallery_detail', 10, '2019-05-25 02:58:02', '2019-05-25 02:58:02'),
(11, 'idcards.html', 'gallery_detail', 11, '2019-05-25 03:00:30', '2019-05-25 03:00:30'),
(12, 'discountcards.html', 'gallery_detail', 12, '2019-05-25 03:00:30', '2019-05-25 03:00:30'),
(13, 'membershipcards.html', 'gallery_detail', 13, '2019-05-25 03:06:43', '2019-05-25 03:06:43'),
(14, 'customcards.html', 'gallery_detail', 14, '2019-05-25 03:06:43', '2019-05-25 03:06:43'),
(15, 'mailers.html', 'gallery_detail', 15, '2019-05-25 03:06:43', '2019-05-25 03:06:43'),
(16, 'ordersamples.html', 'ordersamples', 16, '2019-05-25 03:06:43', '2019-05-25 03:06:43'),
(17, 'specs.htm', 'pages', 17, '2019-05-25 03:06:43', '2019-05-25 03:06:43'),
(18, 'templates.html', 'pages', 18, '2019-05-25 03:11:43', '2019-05-25 03:11:43'),
(19, 'designservices.html', 'pages', 19, '2019-05-25 03:11:43', '2019-05-25 03:11:43'),
(20, 'submission.asp', 'submitorder', 20, '2019-05-25 03:11:43', '2019-05-25 03:11:43'),
(21, 'faqs.html', 'pages', 21, '2019-05-25 03:11:43', '2019-05-25 03:11:43'),
(22, 'thank-you.html', 'pages', 22, '2019-05-27 05:40:56', '2019-05-27 05:40:56'),
(23, 'businesscards.html', 'gallery_detail', 24, '2019-05-29 10:20:31', '2019-05-29 10:20:31'),
(24, 'envirocards.html', 'pages', 25, '2019-05-30 04:57:54', '2019-05-30 04:57:54'),
(25, 'sitemap.html', 'pages', 26, '2019-06-20 11:51:34', '2019-06-20 11:51:34'),
(26, 'privacy.html', 'pages', 27, '2019-06-20 12:25:02', '2019-06-20 12:25:02');

-- --------------------------------------------------------

--
-- Table structure for table `submit_order_submissions`
--

CREATE TABLE `submit_order_submissions` (
  `sbo_id` int(11) NOT NULL,
  `sbo_firstname` varchar(100) DEFAULT NULL,
  `sbo_lastname` varchar(100) DEFAULT NULL,
  `sbo_email` varchar(500) DEFAULT NULL,
  `sbo_phone` varchar(100) DEFAULT NULL,
  `sbo_upload` varchar(200) DEFAULT NULL,
  `sbo_comments` text,
  `con_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submit_order_submissions`
--

INSERT INTO `submit_order_submissions` (`sbo_id`, `sbo_firstname`, `sbo_lastname`, `sbo_email`, `sbo_phone`, `sbo_upload`, `sbo_comments`, `con_date`) VALUES
(1, 'stephanie', 'raphael', 'stephmelanie@gmail.com', '123684597', '', 'test', '2019-06-30 18:32:05'),
(2, 'christine', 'manning', 'christinemanning83@gmail.com', '4196835920', '', 'test', '2019-06-30 18:53:01'),
(3, 'Alex Test', 'Test', 'amaniago@clixpert.com.au', '0999888777', '', 'test', '2019-06-30 22:59:38'),
(4, 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', '', 'test please ignore', '2019-11-05 15:50:46'),
(5, 'Majid', 'sadeghi', 'sales@ariadcard.com.au', '+61424458067', '5dc57564dbc771573221732.jpg', 'Dear Sir/Madam\r\nHope this letter finds you well,  I would like to introduce  ARIADCARD company.\r\nARIADCARD is the leading plastic card producer in Australia. If you are thinking of purchasing your necessary plastic card including Gift cards , VIP cards , Customer club cards, Loyalty cards , Membership cards ,visit cards, ....  please visit our website (www.ariadcard.com.au)\r\nand send us your inquiry via Email (sales@ariadcard.com.au)\r\nWe produce 10 pcs untill over 10,000 PVC cards (RFID /smart/Magnetic/,... cards) every business day of the year to support\r\na broad range of private, public and government entities.\r\nOur customer service team is here to answer all your questions regarding your plastic card needs.                                                      We will be glad you  following   and give new experience with Ariadcard.                                                                                                                   Even 10 pcs -1000 pcs delivery  one day****Best quality & Best Price *****                                                                                                                   Thanks                                                                                                                             Best regards', '2019-11-08 07:02:12');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `tes_id` int(11) NOT NULL,
  `tes_title` varchar(100) DEFAULT NULL,
  `tes_description` text,
  `tes_order` int(11) DEFAULT '0',
  `tes_created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `tes_updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`tes_id`, `tes_title`, `tes_description`, `tes_order`, `tes_created_at`, `tes_updated_at`) VALUES
(1, 'Rick King, Rick\'s Place', 'Absolute top notch products and 5 star service. We highly recommend <strong>Custom Plastic Cards.</strong>', 1, '2019-05-25 21:12:22', '2019-05-25 21:12:22'),
(2, 'Jim Harding, CA', 'My experience with <strong>Custom Plastic Cards</strong> was outstanding! My order required numerical sequencing on the front and back of the card. I submitted my design via fax, clarified a few points over the phone, and within a few days I had the finished product delivered to me. These high quality cards were exactly what I was expecting. Along with the great service and low cost I would not hesitate to recommend <strong>Custom Plastic Cards.</strong>', 2, '2019-05-25 21:12:53', '2019-05-25 21:12:53'),
(3, 'Brandon Trentler, Operations Manager, Park1', '<strong>Custom Plastic Cards</strong> did a fantastic job on our order! They worked with us to ensure that the cards we received were exactly what we wanted. Their quality showed in the work they did. I was also impressed at the speed in which we received our cards. I will certainly use <strong>Custom Plastic Cards</strong> for access cards again.', 3, '2019-05-25 21:15:20', '2019-05-25 21:15:20'),
(4, 'Lexington Golf Club, Lexington', 'Our golf club implemented a swipe card accounting system. Finding magnetic plastic cards to interact with this system fell on my shoulders. I searched extensively for a company to fulfill my needs but I had a difficult time just finding a company that responded. I did a Google search and located <strong>Custom Plastic Cards</strong>. From the first phone conversation to the last, <strong>Custom Plastic Cards</strong> were great to do business with. The response time was unbelievable and the end product was exactly what I needed at a great price. I promptly received my cards from the time of my initial conversation, the printed and encoded cards were perfectly done. I can say that I highly recommend the <strong>Custom Plastic Cards</strong> Company.', 4, '2019-05-25 21:15:42', '2019-05-25 21:15:42'),
(5, 'Brian D. Schram, President, Hennessy & Associates', 'As a repeat customer of <strong>Custom Plastic Cards,</strong> I am extremely happy with the quality of my orders. Each order has been completed on time and accurately. Perhaps the greatest satisfaction comes from the friendly, hands-on service provided by each employee.', 5, '2019-05-25 21:16:06', '2019-05-25 21:16:06'),
(6, 'Kristin Hugo, Owner, Dream Haven Interiors', 'We are proud to issue the cards we received from <strong>Custom Plastic Cards</strong> to our customers. The staff worked diligently with us to get the look we wanted and promptly responded to all emails and telephone calls. We received the cards within days. We are extremely happy with the product and the service of <strong>Custom Plastic Cards</strong> and would recommend them to anybody in the market for custom cards.', 6, '2019-05-25 21:16:26', '2019-05-25 21:16:26'),
(7, 'Michael Fisher, NSW', '(Actual Email)\r\nWe received the cards today and are extremely pleased with the quality and fast turnaround of your services. We will continue to use your services for our future needs. I anticipate placing another order within the next 2 weeks. Thank you!', 7, '2019-05-25 21:16:48', '2019-05-25 21:16:48'),
(8, 'Tracey S', 'We have been a customer since 2011 and continue returning due to the continued professional customer service we receive from Hesham! Always a delight to deal with, quick and efficient dealings via email. Great product leaving our customers happy. Definitely will continue ordering here & highly recommend this company.', 8, '2019-06-21 13:30:18', '2019-06-21 13:30:18'),
(9, 'Crownalee Van', 'Karen was very efficient and professional in the way she handled our orders. The products are of high quality and impressive. Definitely will use them again! Highly recommended!', 9, '2019-06-21 13:31:09', '2019-06-21 13:31:09'),
(10, 'David Allan', 'I have been in the industry for 20 years and have used these guys for 8 years now, as they have the best quality cards I have seen. They provide great advice and a reliable service all round, and at a reasonable price to boot.', 10, '2019-06-21 13:31:53', '2019-06-21 13:31:53'),
(11, 'Cindy Marron', 'I use this company since 2011 for all my custom plastic cards. Great quality and delivery is always on time.', 11, '2019-06-21 13:32:42', '2019-06-21 13:32:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'CustomPlastics', 'admin@plasticcard', NULL, '$2y$10$rH7kWkKOC68mNUM.RPbv9OhQzVS0pM38nClLoU8iXy8Dy8mtqS/De', 'iNzROCsHWISawuhIdSAzEM3l5Smnr4xl65ZoasR7EpBYcAHpduuul8vmwH7z', '2019-04-22 03:58:12', '2019-04-22 03:58:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_submissions`
--
ALTER TABLE `contact_submissions`
  ADD PRIMARY KEY (`con_id`);

--
-- Indexes for table `cpau_cmsv_delete`
--
ALTER TABLE `cpau_cmsv_delete`
  ADD PRIMARY KEY (`cms_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `homepage`
--
ALTER TABLE `homepage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepage_boxes`
--
ALTER TABLE `homepage_boxes`
  ADD PRIMARY KEY (`hbox_id`);

--
-- Indexes for table `homepage_clients`
--
ALTER TABLE `homepage_clients`
  ADD PRIMARY KEY (`clients_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordersamples`
--
ALTER TABLE `ordersamples`
  ADD PRIMARY KEY (`ord_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `quotation_submission`
--
ALTER TABLE `quotation_submission`
  ADD PRIMARY KEY (`qt_id`);

--
-- Indexes for table `slug`
--
ALTER TABLE `slug`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submit_order_submissions`
--
ALTER TABLE `submit_order_submissions`
  ADD PRIMARY KEY (`sbo_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`tes_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_submissions`
--
ALTER TABLE `contact_submissions`
  MODIFY `con_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `cpau_cmsv_delete`
--
ALTER TABLE `cpau_cmsv_delete`
  MODIFY `cms_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;

--
-- AUTO_INCREMENT for table `homepage`
--
ALTER TABLE `homepage`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `homepage_boxes`
--
ALTER TABLE `homepage_boxes`
  MODIFY `hbox_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `homepage_clients`
--
ALTER TABLE `homepage_clients`
  MODIFY `clients_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ordersamples`
--
ALTER TABLE `ordersamples`
  MODIFY `ord_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `quotation_submission`
--
ALTER TABLE `quotation_submission`
  MODIFY `qt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=388;

--
-- AUTO_INCREMENT for table `slug`
--
ALTER TABLE `slug`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `submit_order_submissions`
--
ALTER TABLE `submit_order_submissions`
  MODIFY `sbo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `tes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
