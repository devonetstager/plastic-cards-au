@extends('frontend.Layouts.app')
@section('content')
<div class="innerpage page_{{$data->id}}">
	<div class="innerbanner">
		<div class="wid">
			<h1><?php echo $data->page_heading;?></h1>
		</div>
	</div>
	<div class="wid">
		<div class="contLft">
			<div class="innerCvr">
                <?php echo $data->page_description;?> 
				
				<div class="gallerDiv">
					<div class="demo-gallery">
						<ul id="lightgallery" class="list-unstyled row">
							
							<?php
							foreach($galleryImages as $images)
							{
							?>
							<li class="galdivBx" data-responsive="{{asset('public/galleryassets/'.$images->gallery_file)}}" data-src="{{asset('public/galleryassets/'.$images->gallery_file)}}">
								<a href="" title="<?php echo $data->page_heading;?>"><div class="hocEff"><img src="{{asset('public/galleryassets/'.$images->gallery_file)}}" alt="<?php echo $data->page_heading;?>" title="<?php echo $data->page_heading;?>"></div></a>
							</li>
										
							
							<?php
							}
							?>
						</ul>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>
<?php
function excerpt($content,$limit)
{
	$excerpt = explode(' ', $content , $limit);
	if (count($excerpt) >= $limit)
	{
		array_pop($excerpt);
		$excerpt = implode(" ", $excerpt) . '...';
	}
	else
	{
		$excerpt = implode(" ", $excerpt);
	}

	$excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
	return $excerpt;
} 
?>
@endsection