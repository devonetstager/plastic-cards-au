@extends('frontend.Layouts.app')
@section('content')
<div class="innerpage page_{{$data->id}}">
	<div class="innerbanner">
		<div class="wid">
			<h1><?php echo $data->page_heading;?></h1>
		</div>
	</div>
	<div class="wid">
		<div class="contLft">
			<div class="innerCvr">
                <?php echo $data->page_description;?> 
			</div>
		</div>
	</div>
</div>
@endsection