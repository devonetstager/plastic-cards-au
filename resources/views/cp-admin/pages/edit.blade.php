@extends('adminlte::page')

@section('title', 'Edit Page')

@section('content_header')
    <h1>Edit Page</h1>
@stop




@section('content')

    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.4/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.4/css/froala_style.min.css" rel="stylesheet" type="text/css" />
	
	  <!-- Include Editor JS files. -->
	<div class="col-md-12">
        @include('layouts.alert')
        <div class="panel panel-default">
			<div class="panel-body">
				<form method="POST" id="addShops" action="{{ url('/cp-admin/pages/update/') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post">
				
				
				
					{{ csrf_field() }}
					<div class="box-body">
						<div class="form-group">
							<label for="inputText1" class="col-sm-2 control-label">Heading</label>

							<div class="col-sm-10">
								<input type="text" class="form-control" id="page_heading" name="page_heading" placeholder="Heading" value="{{$data->page_heading}}">
								{!! $errors->first('page_heading', '<p class="help-block">:message</p>') !!}
							
							</div>
						</div>
						<div class="form-group">
							<label for="inputText1" class="col-sm-2 control-label ">Description</label>

							<div class="col-sm-10">
								
								<textarea  class="form-control froalaeditor" name="page_description" id="page_description">{{$data->page_description}}</textarea>
								
							
							</div>
						</div>
						<?php
						if($data->page_type==1) { //ie Gallery Page
						?>
						<div class="form-group">
							<label for="inputText1" class="col-sm-2 control-label">Page Image</label>

							<div class="col-sm-10">
								<input type="file" class="form-control" id="page_image" name="page_image">
								{!! $errors->first('page_image', '<p class="help-block">:message</p>') !!}
								
								
							
							</div>
						</div>
						
						
						<?php
						}
						?>
						<input type="hidden" name="old_page_image" id="old_banner_image" value="{{$data->page_image}}" />
						
						
						<div class="form-group">
							<label for="inputText1" class="col-sm-2 control-label">Meta Title</label>

							<div class="col-sm-10">
								<input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="Meta Title" value="{{$data->meta_title}}">
							
							</div>
						</div>
						<div class="form-group">
							<label for="inputText1" class="col-sm-2 control-label">Meta Keywords</label>

							<div class="col-sm-10">
								<input type="text" class="form-control" id="meta_keywords" name="meta_keywords" placeholder="Meta Keywords" value="{{$data->meta_keywords}}">
							
							</div>
						</div>
						<div class="form-group">
							<label for="inputText1" class="col-sm-2 control-label">Meta Description</label>

							<div class="col-sm-10">
								
								<textarea placeholder="Meta Description" class="form-control" name="meta_description" id="meta_description">{{$data->meta_description}}</textarea>
							
							</div>
						</div>
						
						
						
						
						
						
						<input type="hidden" id="page_id" name="page_id" value="{{$data->id}}">		
						
						
						
					</div>
					  <!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-default">Cancel</button>
						<button type="submit" class="btn btn-info pull-right">Update</button>
					</div>
					  <!-- /.box-footer -->
				
				</form>
				
				
				
						
						
				
				
				<?php
						if($data->page_type==1) { //ie Gallery Page
						?>
						<div class="box-body">
							<div class="form-group">
								<div class="col-sm-12">
									<?php
									foreach($details as $images)
									{
										?>

									<div class="col-sm-3 galCvr" id="img_{{$images->gallery_id}}">
										<img src="{{asset('public/galleryassets/'.$images->gallery_file)}}" width="150" />
										
										<div ><a class="galLink"  onclick="del_image({{$images->gallery_id}});"><i class="fa fa-fw fa-close"></i></a></div>
										
									</div>
									<?php
									}
									?>
								</div>
							</div>
						</div>
						
						<form action="" onsubmit="return validateEnquiry()" method="post" enctype="multipart/form-data">
											  <input type="hidden" name="_token" value="{{ csrf_token() }}">		

								<div class="well">
								  <h3>Add Media</h3>

								  <div class="form-group">
									 
									   <div class="file-loading">
											<input id="image-file" type="file" name="file[]" accept="image/*" data-min-file-count="1" multiple>
										</div>
								  </div>
								 
								  


								</div>
					  
							 
							</form>
						
						
						
						<?php
						}
						?>
			</div>
          <!-- /.box -->
        
		</div>
       
      
	</div>
      <!-- /.row -->
	  <!-- Include Editor style. -->
	  


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.4/js/froala_editor.pkgd.min.js"></script>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.9.5/js/froala_editor.min.js"></script>  -->  
 
<!-- Initialize the editor. -->

    <script> $(function() { $('.froalaeditor').froalaEditor({
		fileUploadMethod: 'PUT',
		imageUploadURL: "{{route('froala_upload')}}",
		fileUploadParam: 'file_name'		
		}) 
	}); 
	/*new FroalaEditor('.froalaeditor', {
      // Set the image upload URL.
      fileUploadMethod: 'PUT',
      imageUploadURL: 'upload.php',
      fileUploadParam: 'file_name'

     
    })*/
	
	

	
	</script>
	
	  <script>
    /*$(function () {
      const editorInstance = new froalaEditor('.froalaeditor', {
        events: {
          'image.beforeUpload': function (files) {
            const editor = this
            if (files.length) {
              var reader = new FileReader()
              reader.onload = function (e) {
                var result = e.target.result
                editor.image.insert(result, null, null, editor.image.get())
              }
              reader.readAsDataURL(files[0])
            }
            return false
          }
        }
      })
    })()*/
  </script>
  
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $("#image-file").fileinput({
            theme: 'fa',
            uploadUrl: "{{route('upload_image')}}",
            uploadExtraData: function() {
                return {
                    _token: "{{ csrf_token() }}",
                    gallery_page_id: "{{$data->id}}",
                    page_heading: "{{$data->page_heading}}",
					
                };
            },
            allowedFileExtensions: ['jpg', 'png', 'gif','jpeg','webp'],
            overwriteInitial: false,
            maxFileSize:2048,
            maxFilesNum: 10
        });
    </script>
	
	<script>
	
	function del_image(gal_id)
	{
		a= confirm("Are you sure to delete image?");
		
		if(a)
		{
			$.ajax({
				type: "GET",
				url: "{{route('delete_image')}}",
				data: {gal_id:gal_id},
				success: function( msg ) {
					if(msg=='yes')
					{
						$("#img_"+gal_id).hide();
						
					}
				}
			});
		}
		return false;
		
	}
	</script>
    
	
@endsection


