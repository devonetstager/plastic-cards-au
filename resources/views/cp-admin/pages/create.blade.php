@extends('adminlte::page')

@section('title', 'Options')

@section('content_header')
    <h1>Page</h1>
@stop

@section('content')

    <div class="col-md-12">
        @include('layouts.alert')
        <div class="panel panel-default">
		  <div class="panel-body">
		 
            <form method="POST" id="addShops" action="{{ url('/cp-admin/option/update/') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post">
			  
			  {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputText1" class="col-sm-2 control-label">Heading</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="heading" name="heading" placeholder="Heading" value="">
					
                  </div>
                </div>
				
				
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        
        </div>
       
      
	</div>
      <!-- /.row -->
  

</section>
@endsection
