@extends('adminlte::page')

@section('title', 'Options')

@section('content_header')
    <h1>Options</h1>
@stop

@section('content')

    <div class="col-md-12">
        @include('layouts.alert')
        <div class="panel panel-default">
		  <div class="panel-body">
		 
            <form method="POST" id="addShops" action="{{ url('/cp-admin/option/update/') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post">
			  
			  {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputText1" class="col-sm-2 control-label">YouTube</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="youtube" name="youtube" placeholder="YouTube" value="{{$options->youtube}}">
					
                  </div>
                </div>
				<div class="form-group">
                  <label for="inputText2" class="col-sm-2 control-label">LinkedIn</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$options->linkedin}}" id="linkedin" name="linkedin" placeholder="LinkedIn">
					
                  </div>
                </div>
				<div class="form-group">
                  <label for="inputText3" class="col-sm-2 control-label">Google Plus</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$options->googleplus}}" id="googleplus" name="googleplus" placeholder="Google Plus">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputText4" class="col-sm-2 control-label">Facebook</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$options->facebook}}" id="facebook" name="facebook" placeholder="Facebook">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="inputText4" class="col-sm-2 control-label">Instagram</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$options->instagram}}" id="instagram" name="instagram" placeholder="Instagram">
                  </div>
                </div>
				<div class="form-group">
                  <label for="inputText4" class="col-sm-2 control-label">Twitter</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$options->twitter}}" id="twitter" name="twitter" placeholder="Twitter">
                  </div>
                </div>
				
				<div class="form-group  {!! $errors->first('email', 'has-error') !!}">
                  <label for="inputText4" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$options->email}}" id="email" name="email" placeholder="Email">
					{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                  </div>
                </div>
				<div class="form-group {!! $errors->first('phonenumber', 'has-error') !!}">
                  <label for="inputText4" class="col-sm-2 control-label">Phone Number</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$options->phonenumber}}" id="phonenumber" name="phonenumber" placeholder="Phone Number">
					{!! $errors->first('phonenumber', '<span class="help-block">:message</span>') !!}
                  </div>
                </div>
				<div class="form-group {!! $errors->first('mobile', 'has-error') !!}">
                  <label for="inputText4" class="col-sm-2 control-label">Mobile</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$options->mobile}}" id="mobile" name="mobile" placeholder="Mobile">
					{!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
                  </div>
                </div>
				<div class="form-group">
                  <label for="inputText4" class="col-sm-2 control-label">Address</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" id="address" name="address" placeholder="Address">{{$options->address}}</textarea>
                  </div>
                </div>
				
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        
        </div>
       
      
	</div>
      <!-- /.row -->
  

</section>
@endsection
