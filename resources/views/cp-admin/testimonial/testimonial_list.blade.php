@extends('adminlte::page')

@section('title', 'Testimonials')

@section('content_header')
    <h1> Testimonials</h1>
@stop

@section('content')

    <div class="col-md-12">
        @include('layouts.alert')
		
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-sm-10"><h3 class="box-title">Testimonials</h3></div>
					<div class="col-sm-2"><a href="{{ url('/cp-admin/testimonial_add/') }}" class="btn btn-block btn-primary">Add New</a></div>
				</div>
				
				
				
			</div>
			
			<!-- /.box-header -->
			<div class="box-body">
				<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6"></div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
								<thead>
									<tr role="row" class="odd">
										<td class="sorting_1" width="5%">No</td>
										<td width="25%">Title</td>
										<td width="60%">Testimonial</td>
										
										<td width="10%">&nbsp;</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$page_no = 0;
									foreach($data as $details)
									{
										$page_no++;
									?>
									<tr role="row" class="odd">
										<td><?php echo $page_no;?></td>
										<td>
										<?php 
										
										echo $details->tes_title;
										?>
										</td>	
										<td>
										<?php 
										
										echo $details->tes_description;
										?>
										</td>	
											
										<td>
										<a href="{{URL::route('testimonial_edit',Crypt::encrypt($details->tes_id))}}"><i class="fa fa-pencil-square-o"  title="Edit"></i></a>
										
										<a href="{{URL::route('testimonial_delete',Crypt::encrypt($details->tes_id))}}"><i class="fa fa-fw fa-close"  title="Delete"></i></a>
										
										</td>
									</tr>
									<?php
									}
									?>
							
									
								</tbody>
								
							</table>
						</div>
					</div>
				   
				</div>
			</div>
			<!-- /.box-body -->
		</div>


		
		  
          <!-- /.box -->
        
	</div>

@endsection
