@extends('adminlte::page')

@section('title', 'Contact Submission')

@section('content_header')
    <h1> Contact Submission</h1>
@stop

@section('content')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('public/vendor/adminlte/dist/css/bootstrap-datepicker.min.css') }}">
    <!-- bootstrap datepicker -->
    <script src="{{ asset('public/vendor/adminlte/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <div class="col-md-12">
        @include('layouts.alert')
		
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-sm-10"><h3 class="box-title">Contact Submission</h3></div>
					
				</div>
				
				
				
			</div>
			
			<!-- /.box-header -->
			<div class="box-body">
			    
              
              
				<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
				    <form action="{{ route('contact_submissions') }}" method="GET">
					<div class="row">
    					    <div class="col-sm-10">
    					         <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="datepickerFrom" name="dateFrom"
                                  @if(Request::get('dateFrom')) value="{{ Request::get('dateFrom') }}"  @endif
                                  >
                                </div>
        					    <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="datepickerTo" name="dateTo"
                                  @if(Request::get('dateTo')) value="{{ Request::get('dateTo') }}"  @endif
                                  >
                                </div>
                                <button class="btn btn-primary ">Search</button>
   
                            <a href="{{route('contact_submissions')}}" class="btn btn-link ">Clear</a>
						</div>
					    </div>
					 </form>
					 <hr>
					 <form action="{{ route('export_contact_submissions') }}" method="GET">
					 <div class="row">
    					<div class="col-sm-10">
    					    <label>Export Filters</label>
    					    <input type="hidden" name="dateFrom" value="{{ Request::get('dateFrom') }}">
    					    <input type="hidden" name="dateTo" value="{{ Request::get('dateTo') }}">
    					    
    					    <table class="">
    					        <tr>
    					        <td>First Name</td>
    					        <td><input type="checkbox" name="filters[First Name]" value="1" > </td>
    					        </tr>
    					        <tr>
    					        <td>Last Name</td>
    					        <td><input type="checkbox" name="filters[Last Name]" value="2" > </td>
    					        </tr>
    					        <tr>
    					        <td>Email</td>
    					        <td><input type="checkbox" name="filters[Email]" value="3" > </td>
    					        </tr>
    					        <tr>
    					        <td>Inquiry Type</td>
    					        <td><input type="checkbox" name="filters[Inquiry Type]" value="4" > </td>
    					        </tr>
    					        <tr>
    					        <td>Enquiries/Quantity</td>
    					        <td><input type="checkbox" name="filters[Inquiry]" value="5" > </td>
    					        </tr>
    					        <tr>
    					        <td>Date</td>
    					        <td><input type="checkbox" name="filters[Enquired On]" value="4" > </td>
    					        </tr>
    					    </table>
    					    
    					    <button class="btn btn-default ">Export</button>
    					      
    					      
    					     <!--<a href="{{route('export_contact_submissions')}}" class="btn btn-default ">Export</a>-->
                            	
    					</div>
    				</div>  
    				 </form>
					    
					<hr>
						<!-- <div class="col-sm-6"></div> -->
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
								<thead>
									<tr role="row" class="odd">
										<td class="sorting_1" width="5%">No</td>
										<td width="15%">Name</td>
										<td width="20%">Email</td>
										<td width="15%">Inquiry Type</td>
										<td width="30%">Inquiry</td>
										<td width="10%">Enquired On</td>
										
										<td width="5%">&nbsp;</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$page_no = 0;
									foreach($data as $details)
									{
										$page_no++;
									?>
									<tr role="row" class="odd">
										<td><?php echo $page_no;?></td>
										<td><?php echo $details->con_firstname." ".$details->con_lastname; ?></td>	
										<td><?php echo $details->con_email;?></td>
										<td><?php echo $details->con_inquiry_type;?></td>
										<td><?php echo $details->con_inquiry; ?></td>
										<td><?php echo date("d-m-Y h:i:s a",strtotime($details->con_date)); ?></td>											
											
										<td><a href="{{URL::route('contact_submissions_delete',Crypt::encrypt($details->con_id))}}"><i class="fa fa-fw fa-close"  title="Delete"></i></a></td>
									</tr>
									<?php
									}
									?>
							
									
								</tbody>
								
							</table>
							<?php echo $data->render(); ?>
						</div>
					</div>
				   
				</div>
			</div>
			<!-- /.box-body -->
		</div>


		
		  
          <!-- /.box -->
        
	</div>
    <script>
     //Date picker
    $('#datepickerFrom').datepicker({
      autoclose: true
    })
     $('#datepickerTo').datepicker({
      autoclose: true
    })
    </script>
@endsection
