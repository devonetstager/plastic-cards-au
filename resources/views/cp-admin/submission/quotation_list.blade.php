@extends('adminlte::page')

@section('title', 'Quotation Submission')

@section('content_header')
    <h1> Quotation Submission</h1>
@stop

@section('content')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('public/vendor/adminlte/dist/css/bootstrap-datepicker.min.css') }}">
    <!-- bootstrap datepicker -->
    <script src="{{ asset('public/vendor/adminlte/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <div class="col-md-12">
        @include('layouts.alert')
		
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-sm-10"><h3 class="box-title">Quotation Submission</h3></div>
					
				</div>
				
				
				
			</div>
			
			<!-- /.box-header -->
			<div class="box-body">
				<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
				    
				    
				    	    
				       <form action="{{ route('quote_submissions') }}" method="GET">
					<div class="row">
    					    <div class="col-sm-10">
    					         <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="datepickerFrom" name="dateFrom"
                                  @if(Request::get('dateFrom')) value="{{ Request::get('dateFrom') }}"  @endif
                                  >
                                </div>
        					    <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="datepickerTo" name="dateTo"
                                  @if(Request::get('dateTo')) value="{{ Request::get('dateTo') }}"  @endif
                                  >
                                </div>
                                <button class="btn btn-primary ">Search</button>
   
                            <a href="{{route('quote_submissions')}}" class="btn btn-link ">Clear</a>
						</div>
					    </div>
					 </form>
					 <hr>
					 	 	 <form action="{{ route('export_quote_submissions') }}" method="GET">
					 <div class="row">
    					<div class="col-sm-10">
    					    <label>Export Filters</label>
    					    <input type="hidden" name="dateFrom" value="{{ Request::get('dateFrom') }}">
    					    <input type="hidden" name="dateTo" value="{{ Request::get('dateTo') }}">
    					    
    					    <table class="">
    					        <tr>
    					        <td>First Name</td>
    					        <td><input type="checkbox" name="filters[First Name]" value="1" > </td>
    					        </tr>
    					        <tr>
    					        <td>Last Name</td>
    					        <td><input type="checkbox" name="filters[Last Name]" value="2" > </td>
    					        </tr>
    					        <tr>
    					        <td>Email</td>
    					        <td><input type="checkbox" name="filters[Email]" value="3" > </td>
    					        </tr>
    					        <tr>
    					        <td>Phone</td>
    					        <td><input type="checkbox" name="filters[Phone]" value="4" > </td>
    					        </tr>
    					        <tr>
    					        <td>Comment</td>
    					        <td><input type="checkbox" name="filters[Comment]" value="5" > </td>
    					        </tr>
    					          <tr>
    					        <td>Details</td>
    					        <td><input type="checkbox" name="filters[Details]" value="4" > </td>
    					        </tr>
    					        <tr>
    					        <td>Date</td>
    					        <td><input type="checkbox" name="filters[Created Date]" value="4" > </td>
    					        </tr>
    					        
    					    </table>
    					    
    					    <button class="btn btn-default ">Export</button>
    					    
                            	
    					</div>
    				</div>  
    				 </form>
					<!--<div class="row">-->
					<!--		<div class="col-sm-12">-->
					<!--	<a href="{{route('export_quote_submissions')}}" class="btn btn-primary pull-right">Export</a>-->
					<!--	</div>-->
					<!--</div>-->
					<hr>
						
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="" class="table table-bordered " role="grid" aria-describedby="example2_info">
								<thead>
									<tr role="row" class="odd">
										<td class="sorting_1" width="5%">No</td>
										<td width="20%">Name</td>
										<td width="20%">Email</td>
										<td width="15%">Phone</td>
										<td width="30%">Comment</td>
										
										<td width="10%">&nbsp;</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$page_no = 0;
									foreach($data as $details)
									{
										$page_no++;
									?>
									<tr role="row" class="odd">
										<td rowspan="2"><?php echo $page_no;?></td>
										<td><?php echo $details->qt_firstname." ".$details->qt_lastname; ?></td>	
										<td><?php echo $details->qt_email;?></td>
										<td><?php echo $details->qt_phone;?></td>
										<td><?php echo $details->qt_comments; ?></td>										
											
										<td rowspan="2" class="odd"><a href="{{URL::route('quote_submissions_delete',Crypt::encrypt($details->qt_id))}}"><i class="fa fa-fw fa-close"  title="Delete"></i></a></td>
									</tr>
									<tr role="row">
										
										<td colspan="4">
											<?php 
		echo "<strong>Card Type : </strong> ".$details->qt_card_type."<br />".
			 "<strong>No Cards : </strong> ".$details->qt_no_of_cards."<br />".
			 "<strong>Features : </strong> ".$details->qt_features."<br />".
			 "<strong>Card Thickness : </strong> ".$details->qt_cardthickness."<br />".
			 "<strong>Number of colors front : </strong> ".$details->qt_no_of_colors_front."<br />".
			 "<strong>Number of colors back : </strong> ".$details->qt_no_of_colors_back."<br />".
			 "<strong>Magnetic Stripe : </strong> ".$details->qt_magnetic_stripe."<br />".
			 "<strong>Date Card : </strong> ".$details->qt_date_card."<br />".
			 "<strong>Enquired On : </strong> ".date("d-m-Y h:i:s a",strtotime($details->created_at))."<br />"
			 ;
			 
			 
												
											?>
										</td>										
										
									</tr>
									<?php
									}
									?>
							
									
								</tbody>
								
								
							</table>
							<?php echo $data->render(); ?>
						</div>
					</div>
				   
				</div>
			</div>
			<!-- /.box-body -->
		</div>


		
		  
          <!-- /.box -->
        
	</div>
		    <script>
     //Date picker
    $('#datepickerFrom').datepicker({
      autoclose: true
    })
     $('#datepickerTo').datepicker({
      autoclose: true
    })
    </script>
@endsection
