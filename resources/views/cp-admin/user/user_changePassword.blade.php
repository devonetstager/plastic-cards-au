@extends('adminlte::page')

@section('title', 'Change Password')

@section('content_header')
    <h1>Change Password</h1>
@stop

@section('content')

    <div class="col-md-12">
        @include('layouts.alert')
        <div class="panel panel-default">
		  <div class="panel-body">
                      
                  <form method="post" action="{{route('updating_password')}}" id="biform" onsubmit="return validation_check();">
                    
                    {{csrf_field()}}
                           
                           <input type="hidden" name="usr_id" value="1">
                        <!-- right column -->
                        <div class="col-md-12">
                            <!-- general form elements disabled -->
                            <div class="box box-warning">
                               
                                <div class="box-body">
                                    <form role="form">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Type New Password</label>
                                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter ..."/>
                                             <span class="hide" id="password_error" onmouseover="hide_this(this.id);"></span>
                                        
                                            </div>

                                            <div class="form-group">
                                            <label>Confirm Password</label>
                                             <input type="password" class="form-control" name="confirm_password" id="confirm_password"/> 
                                             <span id="indicator"></span><br>  
                                        
                                            </div>
                                        
                                        <input class="sb-st" type="submit" value="Submit" class="pure-button pure-button-primary">
                                        
            

                                    </form>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!--/.col (right) -->
                   </form>
          </div>
          <!-- /.box -->
        
        </div>
       
      
	</div>
      <!-- /.row -->
  

</section>



        <script>


        //Password check
                    var password = document.getElementById("password"), 
                    confirm_password = document.getElementById("confirm_password");

                    function validatePassword(){
                      if(password.value != confirm_password.value) {
                        confirm_password.setCustomValidity("Passwords Don't Match");
                      } else {
                        confirm_password.setCustomValidity('');
                      }
                    }

                    password.onchange = validatePassword;
                    confirm_password.onkeyup = validatePassword;

            
            function validation_check(){
                            var flag=0;
                            
                           if ($("#password").val() == ""){
                                $("#password_error").removeClass("hide");
                                $("#password_error").addClass("error");
                                $("#password_error").html("Please enter Password");
                                goToByScroll("password");
                                $("#password").focus();
                                flag=1;
                            }

                            if ($("#confirm_password").val() == ""){
                                $("#confirm_password_error").removeClass("hide");
                                $("#confirm_password_error").addClass("error");
                                $("#confirm_password_error").html("Please confirm Password");
                                goToByScroll("confirm_password");
                                $("#confirm_password").focus();
                                flag=1;
                            }


                            if(flag==1)
                                return false;
                            else 
                                return true;
                        }

            function goToByScroll(id) {
                            // Remove "link" from the ID
                            id = id.replace("link", "");
                            // Scroll
                            $('html,body').animate({
                                scrollTop: $("#" + id).offset().top - 100},
                            'slow');
                        }
                        
                        function hide_this(span_id){
                             //document.getElementById(span_id).className="hide";
                             $("#" + span_id).removeClass("error");
                            $("#" + span_id).addClass("hide");
                        }
        </script>
        </div>
 

@endsection