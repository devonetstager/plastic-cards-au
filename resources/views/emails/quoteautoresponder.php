<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Custom Plastic Cards - AU</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <style>

    </style>
</head>

<body>
    <div style="max-width:640px; margin:0px auto; border: 1px solid #878786; text-align:center; background:#FFF; position:relative; padding-bottom: 0px;">
        <table width="100%" border="0" cellspacing="10" cellpadding="10">
            <tr>
                
                <td style="background:#000;"><img src="<?php echo url('/').'/public/frontend/images/customcardlogo1.png';?>"></td>
             
            </tr>
            <tr>
                <td align="center">
                    <p style="color:#2b2a29; font-family: 'Open Sans', sans-serif; font-size:12.5px; padding:0px; margin-bottom:5px;  margin-top:0px;  line-height:22px;"><font face="'Open Sans', sans-serif">
		
						<table>
							
							
							<tr>
								<td>Hello <?php echo $content['firstname'];?>,<br><br>Thank you very much for your interest in our plastic cards. We will respond promptly to your enquiry. We are recognized for our low pricing, friendly service and fast turnaround <a href="https://plastic-cards.com.au/gallery.html">(https://plastic-cards.com.au/gallery.html)</a>.Please also feel free to contact us at 1 300 651 544 (local toll), thank you again for your interest in our plastic cards.<br><br>Kind Regards<br>Hesham<br><br>Custom Plastic Cards<br>www.plastic-cards.com.au<br>info@plastic-cards.com.au<br>Level 2 Riverside Quay<br>1 Southbank Boulevard<br> Southbank, Victoria 3006<br>Phone 1 300 651 544<br>Fax (03) 5278 8025</td>
												
							</tr>
					   
						</table>
</font>
                    </p>
                </td>
            </tr>
        </table>
    </div>
</body>


</html>